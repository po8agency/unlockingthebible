<?php
get_header();
  /* Template name: Station Listing */
  get_template_part('partials/layout/tpart-start-page');
  get_template_part('partials/page/tpart-subnav');
  ?>
  <div class="utb--stations">
    <div class="container">
      <div class="utb--page-title-area utb--align-center page-title-special">
        <h1 class="utb--page-title ">Station Listing</h1>
      </div>
      <div class="search-form" id="station-container">
        <div class="locality-switcher">
          <select id="listing-switcher">
            <option value="united-states">United States</option>
            <option value="international">International</option>
          </select>
        </div>
        <form class="controls">
          <?php
          if( have_rows('stations', 'option') ): ?>
          <fieldset data-filter-group="state" class="select-wrapper us-select">
            <select>
              <option value="">Select State</option>
              <?php
                $stations_forward = get_field('stations', 'option');
                $stations = array_reverse($stations_forward);
                foreach($stations as $station) {
                  $states[] = $station['station_state']; // Grabing their state from their profile page
                }
                $states = array_unique($states);
                foreach($states as $state) {
                  if(!empty($state)) :
                    echo '<option value=".'.str_replace(' ', '-', strtolower($state)).'">'.$state.'</option>';
                  endif;
                }
            ?>
            </select>
          </fieldset>
          <fieldset data-filter-group="city" class="select-wrapper us-select">
            <select>
              <option value="">Select City</option>
              <?php
                $stations_forward = get_field('stations', 'option');
                $stations = array_reverse($stations_forward);
                foreach($stations as $station) {
                  $cities[] = $station['station_city']; // Grabing their state from their profile page
                }
                $cities = array_unique($cities);
                foreach($cities as $city) {
                  if(!empty($city)) :
                    echo '<option value=".'.str_replace(' ', '-', strtolower($city)).'">'.$city.'</option>';
                  endif;
                }
            ?>
            </select>
          </fieldset>
          <?php endif; ?>
          <!-- <fieldset data-filter-group class="text-input-wrapper us-select">
		        <input type="text" data-search-attribute="data-search" placeholder="Search by Zip Code">
          </fieldset> -->
  			</form>
      </div>
      <div class="stations--grid">
      <?php
        if( have_rows('stations', 'option') ):
        ?>
        <div id="stations-head-us" class="station--grid-head grid grid-head-us active-list">
          <div class="row-col col--city col--us">City</div>
          <div class="row-col col--state col--us">State</div>
          <div class="row-col col--zip col--us">Zip</div>
          <div class="row-col col--station col--int col--us">Station</div>
          <div class="row-col col--frequency col--int col--us">Frequency</div>
          <div class="row-col col--days col--int col--us">Days</div>
          <div class="row-col col--times col--int col--us">Times</div>
        </div>

        <div id="stations-head-int" class="station--grid-head grid grid-head-int">
          <div class="row-col col--country col--int">Country</div>
          <div class="row-col col--region col--int">City/Region</div>
          <div class="row-col col--station col--int col--us">Station</div>
          <div class="row-col col--frequency col--int col--us">Frequency</div>
          <div class="row-col col--days col--int col--us">Days</div>
          <div class="row-col col--times col--int col--us">Times</div>
        </div>

        <div class="station--grid-body" id="stations-int">
        <?php
          while ( have_rows('stations', 'option') ) : the_row();
          $locality     = get_sub_field('station_locality');
          $country      = get_sub_field('station_country');
          $region       = get_sub_field('station_region');
          $name         = get_sub_field('station_name');
          $frequency    = get_sub_field('station_frequency');
          $days         = get_sub_field('station_days');
          $times        = get_sub_field('station_times');
          $locality     = str_replace(' ', '-', strtolower($locality['label']));
          $overrides    = $locality;
          ?>
          <?php if($locality == 'international'): ?>
          <div class="mix station--row grid <?php echo $overrides; ?>">
            <!-- <div class="row-col col--locality"><?php //echo $locality; ?></div> -->
            <div class="row-col col--country col--int"><?php echo $country; ?></div>
            <div class="row-col col--region col--int"><?php echo $region; ?></div>
            <div class="row-col col--station col--int"><?php echo $name; ?></div>
            <div class="row-col col--frequency col--int"><?php echo $frequency; ?></div>
            <div class="row-col col--days col--int"><?php echo $days; ?></div>
            <div class="row-col col--times col--int"><?php echo $times; ?></div>
          </div>
          <?php endif; ?>
          <?php

          endwhile;
        ?>
        </div>
        <div class="station--grid-body active-list" id="stations-us">
        <div class="station-loader"></div>
        <div class="station-fail">No stations could be found matching the requested filters</div>
        <?php
          while ( have_rows('stations', 'option') ) : the_row();
          $locality     = get_sub_field('station_locality');
          $city         = get_sub_field('station_city');
          $state        = get_sub_field('station_state');
          $state_label  = get_sub_field('station_state');
          $zip          = get_sub_field('station_zip');
          $name         = get_sub_field('station_name');
          $frequency    = get_sub_field('station_frequency');
          $days         = get_sub_field('station_days');
          $times        = get_sub_field('station_times');
          $locality     = str_replace(' ', '-', strtolower($locality['label']));
          $state        = strtolower($state);
          $city_l       = str_replace(' ', '-', strtolower($city));
          $overrides      = $state . ' ' . $city_l . ' ' . $locality;
          ?>
          <?php if($locality == 'united-states'): ?>
          <div class="mix station--row grid <?php echo $overrides; ?>" <?php if(!empty($zip)) { ?>data-search="<?php echo $zip; ?>"<?php } ?>data-city="<?php echo $city_l; ?>">
            <!-- <div class="row-col col--locality"><?php //echo $locality; ?></div> -->
            <div class="row-col col--city col--us"><?php echo $city; ?></div>
            <div class="row-col col--state col--us"><?php echo $state_label; ?></div>
            <div class="row-col col--zip col--us"><?php echo $zip; ?></div>
            <div class="row-col col--station col--int col--us"><?php echo $name; ?></div>
            <div class="row-col col--frequency col--us"><?php echo $frequency; ?></div>
            <div class="row-col col--days col--us"><?php echo $days; ?></div>
            <div class="row-col col--times col--us"><?php echo $times; ?></div>
          </div>
          <?php endif; ?>
          <?php
          endwhile;
        ?>
        </div>
        <?php
        endif;
      ?>
      </div>
    </div>
  </div>
  <?php
  get_template_part('partials/layout/tpart-end-page');
get_footer();
