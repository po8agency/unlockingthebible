<?php
  get_header();
  /* Template name: Archive Template */
  get_template_part('partials/layout/tpart-start-page');
  ?>
  <section class="utb--archive utb--archive-all">
    <div class="utb--page-title-area utb--align-center page-title-special">
      <h1 class="utb--page-title ">Archive</h1>
    </div>
    <div class="utb--wrap has-sidebar">
    <?php
      //<div class="utb--archive-search">
      //  <form class="controls">
      //    <fieldset data-filter-group class="text-input-wrapper">
      //        <input class="form-control" type="text" data-search-attribute="data-title" placeholder="Search the Archive"/>
      //    </fieldset>
      // </form>
      //</div>
      ?>
      <aside class="utb--sidebar sb--type-bordered">
        <div class="utb--widget">
          <h3 class="widget--title">Filters</h3>
          <div class="">
            <form class="controls">

              <fieldset data-filter-group="type" class="checkbox-group">
                <label class="checkbox-group-label">Type</label>
                <div class="checkbox">
                  <input type="checkbox" id="series" value=".series" />
                  <label for="series" class="checkbox-label">Teaching Series</label>
                </div>
                <div class="checkbox">
                  <input type="checkbox" id="broadcasts" value=".broadcast" />
                  <label for="broadcasts" class="checkbox-label">Broadcasts</label>
                </div>
                <div class="checkbox">
                  <input type="checkbox" id="sermons" value=".sermon" />
                  <label for="sermons" class="checkbox-label">Sermons</label>
                </div>
                <div class="checkbox">
                  <input type="checkbox" id="articles" value=".post" />
                  <label for="articles" class="checkbox-label">Articles</label>
                </div>
                <div class="checkbox">
                  <input type="checkbox" id="lifekeys" value=".lifekey" />
                  <label for="lifekeys" class="checkbox-label">Life KEYS</label>
                </div>
                <div class="checkbox">
                  <input type="checkbox" id="stories" value=".story" />
                  <label for="stories" class="checkbox-label">Stories of Impact</label>
                </div>
              </fieldset>

              <fieldset data-filter-group class="checkbox-group">
                <label class="checkbox-group-label">Topic</label>
                <?php
                $picked = get_field('topics_picked', 'option');
                if($picked) :
                foreach($picked as $pick) :
                $term_id    = $pick->term_id;
                $term_name  = $pick->name;
                $term_slug  = $pick->slug;
                ?>
                <div class="checkbox">
                  <input type="checkbox" id="type-<?php echo $term_slug; ?>" value=".<?php echo $term_slug; ?>" />
                  <label for="type-<?php echo $term_slug; ?>" class="checkbox-label"><?php echo $term_name; ?></label>
                </div>
                <?php
                endforeach;
                else :
                $tops = get_terms( array(
                  'taxonomy' => 'topic',
                  'parent'   => 0,
                  'number'   => '12',
                  'hide_empty' => false,
                ));
                foreach($tops as $top) :
                $term_id    = $top->term_id;
                $term_name  = $top->name;
                $term_slug  = $top->slug;
                ?>
                <div class="checkbox">
                  <input type="checkbox" id="type-<?php echo $term_slug; ?>" value=".<?php echo $term_slug; ?>" />
                  <label for="type-<?php echo $term_slug; ?>" class="checkbox-label"><?php echo $term_name; ?></label>
                </div>
                <?php
                endforeach;
                endif;
              ?>
              </fieldset>

              <fieldset data-filter-group class="checkbox-group group-scriptures">
                <label class="checkbox-group-label">Scripture</label>
                <div class="group-scroll">
                  <?php
                  if( have_rows('bible_books') ):
                    while ( have_rows('bible_books') ) : the_row();
                    $book = get_sub_field('book_name');
                    $book_slug = str_replace(' ', '-', strtolower($book));
                    $book_slug = preg_replace("/[^a-zA-Z]/", "", $book_slug);
                    ?>
                    <div class="checkbox">
                      <input type="checkbox" id="type-<?php echo $book_slug; ?>" value=".<?php echo $book_slug; ?>" />
                      <label for="type-<?php echo $book_slug; ?>" class="checkbox-label"><?php echo $book; ?></label>
                    </div>
                    <?php
                    endwhile;
                  endif;
                ?>
                </div>
              </fieldset>
            </form>
          </div>
        </div>
      </aside>
      <div class="utb--content">
        <div class="utb--posts utb--results" id="archive-results">
        <?php
        $args = array(
          'post_type'      => array('post', 'sermon', 'lifekey', 'broadcast', 'story', 'series'),
          'post_status'    => 'publish',
          'nopaging'       => true,
          'orderby'        => 'date',
          'order'          => 'DESC',
          'posts_per_page' => -1
        );
        $query = new WP_Query( $args );
        ?>
        <?php if ($query->have_posts()) : ?>
          <?php while ($query->have_posts()) : $query->the_post();
          $post_id      = get_the_ID();
          $post_date    = get_the_date( 'l, F j, Y');
          $post_title   = get_the_title();

          $author_id    = $post->post_author;
          $author_url   = get_author_posts_url(get_the_author_meta('ID'));
          $post_type    = $query->post_type[0];

          // convert the string to all lowercase
          $clean_title  = strtolower($post_title);
          $type         = $post->post_type;

          $topics       = wp_get_object_terms( $post->ID, 'topic');
          $scripts      = wp_get_object_terms( $post->ID,  'scripture' );
          $scripture = array();
          if ( ! empty( $scripts ) ) {
            foreach( $scripts as $script) {
              $scripture[] = preg_replace("/[^a-zA-Z]/", "", $script->slug);
            }
          }

          ?>
          <article class="mix utb--post utb--result<?php echo ' ' . $type . ' ' . $scripture[0]; ?> <?php if ( ! empty( $topics ) ) {foreach( $topics as $topic) {echo ' ' . $topic->slug;}} ?>" data-title="<?php echo $clean_title; ?>">
            <div class="utb--post-img-wrap">
              <a href="<?php echo $post_link; ?>">
                <div class="utb--post-img utb--post-img-zoom loading">
                <?php
                    $thumb_id    = get_post_thumbnail_id();
                    $image       = wp_get_attachment_image_src( $thumb_id , 'product_thumb' );
                    if ($thumb_id) :
                    ?>
                    <img
                      src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw=="
                      data-src="<?php echo $image[0]; ?>"
                  	  class="lazyload" />
                    <?php
                    else :
                      echo '<img class="lazyload" data-src="' . get_stylesheet_directory_uri() . '/assets/img/default.jpg" />';
                    endif; ?>
                </div>
              </a>
            </div>
            <div class="utb--post-body">
              <h1 class="utb--post-title"><a href="<?php echo get_permalink(); ?>"><?php echo $post_title; ?></a></h1>
              <div class="utb--post-meta">
                <span class="utb--post-time"><?php echo $post_date; ?></span>
              </div>
            </div>
          </article>
          <?php
          endwhile;
        endif;
        wp_reset_query();
        ?>
        <div class="utb--result-404">
          <i class="icon-ghost"></i>
          <p>Sorry, we found nothing matching your search</p>
        </div>
        </div>
        <div class="controls-pagination">
          <div class="mixitup-page-list"></div>
        </div>
      </div>
    </div>
  </section>
	<?php
  get_template_part('partials/layout/tpart-end-page');
get_footer();
