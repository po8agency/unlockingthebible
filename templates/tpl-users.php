<?php
  get_header();
  /* Template name: Users Template */
  get_template_part('partials/layout/tpart-start-page');
  //page subnavigation
	get_template_part('partials/page/tpart-subnav');
  ?>
  <section class="utb--users">
    <div class="container">
      <div class="utb--page-title-area utb--align-center page-title-special">
        <h1 class="utb--page-title"><?php echo get_the_title(); ?></h1>
      </div>
      <?php if( '' !== get_post()->post_content ) : ?>
      <div class="utb--wrap utb--users-content utb--compact compact-small">
        <?php the_content(); ?>
        <hr>
      </div>
      <?php endif; ?>
      <div class="grid utb--user-grid">
      <?php
      $user_ids = get_field('user_listing');

      if ( ! empty( $user_ids ) ) :
        $args = array(
          'include' => $user_ids
        );
        // The Query
        $user_query = new WP_User_Query( $args );
        // User Loop
        if ( ! empty( $user_query->get_results() ) ) :
        	foreach ( $user_query->get_results() as $user ) :
          $user_link = get_author_posts_url( $user->ID );

          $user_gravatar 	= get_avatar( $user->ID, 150 );
          $user_img  			= get_field('author_image', 'user_'.$user->ID.'');
          $user_img_size	= 'product_thumb';

          $user_pos  = get_field('user_position', 'user_'.$user->ID.'');
          ?>
          <div class="column c1-4 user--column" itemprop="author" itemscope itemtype="http://schema.org/Person">
            <a href="<?php echo $user_link; ?>" title="<?php echo $user->display_name; ?>" itemprop="url">
              <div class="user--photo-wrap">
                <picture class="user--photo">
    						<?php
                if($user_img) :
    							echo wp_get_attachment_image( $user_img, $user_img_size );
    						elseif(validate_gravatar($user_id)) :
    							echo $user_gravatar;
    	          else :
                  echo $user_gravatar;
                endif;
    						?>
    						</picture>
              </div>
          	  <span class="user--name" itemprop="name"><?php echo $user->display_name; ?></span>
              <?php if($user_pos) : ?>
              <span class="user--pos" itemprop="name"><?php echo $user_pos; ?></span>
              <?php endif; ?>
            </a>
          </div>
          <?php
        	endforeach;
        endif;
      endif;
      ?>
      </div>
    </div>
  </section>
  <?php
  //-----------------------------
  // GLOBAL ARCHIVE PROMO TYPE
  //-----------------------------
  get_template_part('partials/posts/tpart-promo');
  //-----------------------------
  // SUBSCRIBE FIELDS
  //-----------------------------
  get_template_part('partials/posts/tpart-subscribe');

get_template_part('partials/layout/tpart-end-page');
get_footer();
