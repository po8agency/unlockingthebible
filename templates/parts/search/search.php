<?php
global $query_string;
get_template_part('partials/page/tpart-subnav');
?>
<div class="utb--search-page type-default">
  <div class="container">
    <div class="utb--page-title-area utb--align-center page-title-special">
      <h1 class="utb--page-title ">Search Results</h1>
    </div>
    <div class="search-form" id="search-container">
      <p>We have found <strong id="search-count"><?php echo $wp_query->found_posts; ?></strong><?php echo ' post' . plural( $wp_query->found_posts ); ?> matching your search: <?php printf( __( '<strong id="search-term">%s</strong>', 'utb' ), get_search_query() ); ?></p>
      <form role="search" action="<?php echo home_url( '/' ); ?>" method="get">
		    <input type="text" name="s" id="search-normal" value="" placeholder="Search" />
        <input type="hidden" name="post_type[]" value="post" />
				<input type="hidden" name="post_type[]" value="broadcast" />
				<input type="hidden" name="post_type[]" value="sermon" />
        <input type="hidden" name="post_type[]" value="lifekey" />
      </form>
      <!-- <span>or</span><a class="btn btn-gold btn-solid btn-gold btn-size-small" href="/archive">View All</a> -->
    </div>
    <div class="utb--posts results-posts grid col-pad" id="content">
    <?php

    if ( have_posts() ) :
      /* Start the Loop */
      query_posts( $query_string . '&posts_per_page=-1' );
      while ( have_posts() ) : the_post();
        get_template_part( 'partials/page/tpart-post' );
      endwhile; // End of the loop.
    else :
      get_template_part('partials/page/tpart-404');
    endif;
    ?>
    </div>
  </div>
</div>
<?php
get_template_part('partials/posts/tpart-promo');
get_template_part('partials/posts/tpart-subscribe');
?>
