<?php
global $query_string;
?>
<div class="utb--subnav">
  <div class="container">
    <ul>
      <li><a href="<?php echo get_permalink( wc_get_page_id( 'shop' ) ); ?>">Store Home</a></li>
      <li><a href="https://www.10ofthose.com/partners/unlockingthebibleuk">UK Store</a></li>
    </ul>
  </div>
</div>
<?php
//store wrappers
get_template_part('partials/layout/tpart-start-shop');
?>
<section class="utb--store-tax type-product">
  <div class="container">
    <div class="grid utb--products" id="content">
    <?php
      if ( have_posts() ) :
        query_posts( $query_string . '&posts_per_page=-1' );
        while ( have_posts() ) : the_post();
          do_action( 'woocommerce_shop_loop' );
          wc_get_template_part( 'content', 'product' );
        endwhile; // End of the loop.
        echo '<div class="controls-pagination">';
          echo '<div class="mixitup-page-list"></div>';
        echo '</div>';
      else :
        get_template_part('partials/page/tpart-404');
      endif;
    ?>
    </div>
  </div>
</section>
