<p class="woocommerce-result-count"><span class="mixitup-page-stats"></span></p>

<form class="controls product-sort">
  <span class="current">Sort by date (DESC)</span>
  <div class="control-group list">
    <button type="button" class="control control-sort" data-sort="default:asc">Sort by name (A-Z)</button>
    <button type="button" class="control control-sort" data-sort="default:desc">Sort by name (Z-A)</button>
    <button type="button" class="control control-sort" data-sort="date:desc">Sort by date (DESC)</button>
    <button type="button" class="control control-sort" data-sort="date:asc">Sort by date (ASC)</button>
    <button type="button" class="control control-sort" data-sort="price:desc">Sort by price (ASC)</button>
    <button type="button" class="control control-sort" data-sort="price:asc">Sort by price (DESC)</button>
  </div>
</form>
