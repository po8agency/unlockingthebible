<?php

$sidebar_enabled = false;
// open the WordPress loop
if (have_posts()) : while (have_posts()) : the_post();

  // are there any rows within within our flexible content?
  if ( have_rows('page_sections') ):
    echo '<div class="utb--builder-body">';
    // loop through all the rows of flexible content
    while ( have_rows('page_sections') ) : the_row();

    // load the module from the modules folder
    get_template_part( 'partials/page/modules/mod-'. get_row_layout() );

    endwhile; // close the loop of flexible content
    echo '</div>';
  else :
    echo '<div class="utb--builder-404">';
      echo '<div class="container">';
        echo '<i class="icon-grid"></i>';
        echo '<h1 class="utb--mod-title">“Predicting Rain Doesn’t Count. Building Arks Does” - <span>Warren Buffett</span></h1>';
        echo '<p class="utb--builder-404-message">This page doesnt have any content yet, start building the page by clicking the link below.</p>';
        echo '' . edit_post_link('<i class="icon-wrench"></i> Start building', '<div class="builder-404-action">', '</div>', null, 'btn btn-size-small btn-solid btn-gold') . '';
      echo '</div>';
    echo '</div>';
  endif; // close flexible content conditional

  if ($sidebar_enabled == true) :
    echo '<div class="utb--builder-sidebar">';
    get_template_part( 'partials/page/sidebar.php' );
    echo '</div>';
  endif;

endwhile; endif; // close the WordPress loop
