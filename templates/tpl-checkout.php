<?php
  get_header();
    /* Template name: Store Endpoint */
  get_template_part('partials/layout/tpart-start-page');
  if(is_checkout() || is_wc_endpoint_url()) :
  	//page subnavigation
  	get_template_part('partials/page/tpart-subnav');
    //store wrappers
    get_template_part('partials/layout/tpart-start-shop');
  	if (have_posts()) : while (have_posts()) : the_post();
  		echo '<div class="container">';
  			echo '<div class="utb--endpoint">';
  			   get_template_part( 'templates/parts/store/tpart-content' );
  			echo '</div>';
  		echo '</div>';
  	endwhile; endif; // close the WordPress loop
  endif;
  get_template_part('partials/layout/tpart-end-page');
get_footer();
