<?php
	get_header();
	/* Template name: Page Builder */
	$page_options       = get_field('page_options');
	//$enable_builder     = $page_options['page_builder_status'];
	get_template_part('partials/layout/tpart-start-page');
	//page subnavigation
	get_template_part('partials/page/tpart-subnav');

	//if ($enable_builder == true) :
	get_template_part('templates/parts/builder-loop');
	//endif;
	get_template_part('partials/layout/tpart-end-page');
get_footer();
