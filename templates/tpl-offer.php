<?php
  get_header();
  /* Template name: Monthly Offer */
  get_template_part('partials/layout/tpart-start-page');
  get_template_part('partials/page/tpart-subnav');

  $offer_title      = get_field('offer_title', 'option');
  $offer_subtitle   = get_field('offer_subtitle', 'option');
  $offer_btn_text   = get_field('offer_button_text', 'option');
  $offer_url        = get_field('offer_url', 'option');
  $offer_summary    = get_field('offer_summary', 'option');
  $offer_content    = get_field('offer_main_content', 'option');
  $offer_emphasize  = get_field('offer_emphasize', 'option');
  $offer_image      = get_field('offer_image', 'option');
  $offer_gifts      = get_field('offer_gifts', 'option');
  ?>
  <section class="utb--offer-page">
    <div class="container">
      <div class="utb--page-title-area utb--align-center page-title-special">
        <h1 class="utb--page-title ">For your gift of any amount</h1>
      </div>
      <div class="utb--wrap">
        <div class="grid offer-intro">
          <div class="column c1-2">
            <?php echo wp_get_attachment_image( $offer_image, 'large'); ?>
          </div>
          <div class="column c1-2">
            <h1><?php echo $offer_title; ?></h1>
            <?php if($offer_subtitle) : ?>
            <h5><?php echo $offer_subtitle; ?></h5>
            <?php endif; ?>
            <span class="offer-date"><?php echo date('F Y'); ?></span>
            <p><?php echo $offer_summary; ?></p>
            <div class="offer-month"><?php echo date('F'); ?>'s Monthly Offer</div>
            <br>
            <a class="btn btn-solid btn-gold btn-size-small" href="#offers">Get the offer</a>
          </div>
        </div>
        <?php if($offer_emphasize) : ?>
        <div class="offer-emphasize">
          <p><?php echo $offer_emphasize; ?></p>
        </div>
        <?php endif; if($offer_content) : ?>
        <div class="offer-content">
          <div class="utb--compact">
            <?php echo $offer_content; ?>
          </div>
        </div>
        <?php endif; ?>
      </div>
    </div>
    <?php
      if( have_rows('offer_gifts', 'option') ):
      $offer_count = count($offer_gifts);
    ?>
    <div class="offer-items" id="offers">
      <div class="grid">
      <?php while ( have_rows('offer_gifts', 'option') ) : the_row(); ?>
        <div class="column<?php echo ' c1-'. $offer_count; ?>">
        <?php
          $gift_image = get_sub_field('gift_image');
          if (!empty($gift_image)) :
          ?>
          <picture>
            <img
              src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw=="
              data-src="<?php echo $gift_image['url']; ?>"
              data-sizes="auto"
              class="lazyload" />
            <?php
            else :
              echo '<img class="utb--landscape lazyload" data-src="' . get_stylesheet_directory_uri() . '/assets/img/default.jpg" />';
            endif; ?>
          </picture>
          <h3><?php echo the_sub_field('gift_title'); ?></h3>
          <p><?php echo the_sub_field('gift_description'); ?></p>
          <a class="btn btn-solid btn-gold btn-size-small" href="<?php echo the_sub_field('gift_url'); ?>">Donate</a>
        </div>
      <?php endwhile; ?>
      </div>
    </div>
    <?php endif; ?>
  </section>
	<?php
  //-----------------------------
	// GLOBAL ARCHIVE PROMO TYPE
	//-----------------------------
	//get_template_part('partials/posts/tpart-promo');
	//-----------------------------
	// SUBSCRIBE FIELDS
	//-----------------------------
	get_template_part('partials/posts/tpart-subscribe');
  get_template_part('partials/layout/tpart-end-page');
get_footer();
