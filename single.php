<?php
	get_header();
	get_template_part('partials/layout/tpart-start-page');
	//page subnavigation
	get_template_part('partials/page/tpart-subnav');
	get_template_part('partials/posts/tpart-hero');

	$queried_post   = get_queried_object();
	$post_ID		    = $queried_post->post_ID;
	$topics 				= get_the_terms( $post_ID, 'topic' );
	$post_date     	= get_the_date( 'F j, Y', $post_id );
	$authorDesc 		= get_the_author_meta('user_description');
	$authorEmail		= get_the_author_meta('user_email');
	$author_name		= get_the_author_meta('display_name');
	$author					= get_the_author();
	$user_id				= $post->post_author;
	$author_first 	= explode(' ',trim($author_name));
	if($author_name) {
		$author_name = $author_first;
	} else {
		$author_name = $author;
	}
	$author_link   	= get_author_posts_url( get_the_author_meta( 'ID' ), get_the_author_meta( 'user_nicename' ) );

	$user_gravatar 	= get_avatar( $user_id, 150 );
	$user_img  			= get_field('author_image', 'user_'.$user_id.'');
	$user_img_size	= 'product_thumb';
	?>
  <section class="utb--single utb--single-article" >
    <div class="container">
      <div class="utb--single-post grid">
        <div class="utb--single-sidebar">
					<div class="utb-sticky">
	          <div class="utb--sidebar-author">
	            <a href="<?php echo $author_link; ?>">
	              <div class="author-image">
								<?php
								if($user_img) :
									echo wp_get_attachment_image( $user_img, $user_img_size );
								elseif(validate_gravatar($user_id)) :
									echo $user_gravatar;
								else :
									echo $user_gravatar;
								endif;
								?>
	              </div>
	              <span class="author-name"><?php the_author(); ?></span>
	            </a>
	          </div>
	          <div class="utb--sidebar-stamp">
	            <span class="stamp-date"><em><?php echo $post_date; ?></em></span>
	          </div>
	          <?php echo social_sharing_buttons(); ?>
					</div>
        </div>
        <div class="utb--single-body">
          <div class="single-content">
          <?php the_content(); ?>
          </div>
					<?php if ( $topics && ! is_wp_error( $topics ) ) : ?>
          <div class="single-meta">
            <div class="meta-topics">
              <div class="utb--post-tags">
								<span>Topics:</span>
              	<?php
                foreach ($topics as $topic) :
                $topic_link = get_term_link( $topic );
                ?>
                <a class="alink gold dark-hover" href="<?php echo $topic_link; ?>"><?php echo $topic->name; ?></a>
                <?php endforeach; ?>
              </div>
            </div>
	       	</div>
					<?php endif; ?>
          <hr>
          <div class="single-author">
            <small>The Author</small>
            <h5 class="author-name"><a class="series" href="<?php echo $author_link; ?>" title="<?php echo $author; ?>"><?php echo $author; ?></a></h5>
            <p><?php if($authorDesc) {echo $authorDesc . ' ';} ?><?php if($author) { echo 'Contact '.$author_name[0].' at <a class="alink gold dark-hover" href="mailto:'.$authorEmail.'" target="_blank">'.$authorEmail.'</a>'; } ?></p>
          </div>
          <hr>
        </div>
      </div>
			<?php get_template_part('partials/page/loops/tpart-related'); ?>
    </div>
  </section>
	<?php
	get_template_part('partials/posts/tpart-offer');
	get_template_part('partials/posts/tpart-subscribe');

	get_template_part('partials/layout/tpart-end-page');
get_footer();
