<?php
	get_header();
	get_template_part('partials/layout/tpart-start-page');
	//page subnavigation
	get_template_part('partials/page/tpart-subnav');

	$queried_post   = get_queried_object();
	$post_ID		    = $queried_post->post_ID;
	$topics 				= get_the_terms( $post_ID, 'topic' );

	$pod_rel 						= pods( 'series', get_the_id() );
	$linked_broadcasts 	= $pod_rel->field( 'linked_broadcast' );
	$rel_product 				= $pod_rel->field( 'linked_products' );
	$linked_sermons  		= $pod_rel->field( 'linked_sermons' );

	//$linked_sermons 			= $rel_sermon['ID'];
	$linked_product 			= $rel_product['ID'];
	?>
  <section class="utb--single utb--single-series">
    <div class="container">
			<div class="utb--page-title-area utb--align-center page-title-special">
				<h1 class="utb--page-title">Teaching Series</h1>
			</div>
			<div class="grid series--current-item">
				<div class="column c1-2">
					<picture>
						<?php echo get_the_post_thumbnail( $post_ID, 'large'); ?>
					</picture>
				</div>
				<div class="column c1-2">
					<h1 class="utb--cpt-title"><?php the_title(); ?></h1>
					<div class="utb--cpt-content"><?php the_content(); ?></div>
					<?php if(!empty($linked_product)): ?>
					<a class="btn btn-solid btn-gold btn-size-small" href="<?php echo get_permalink($linked_product); ?>">Purchase the Series</a>
					<?php endif; ?>
					<?php if(!empty($linked_sermon)): ?>
					<div class="sermon-link">
						<a class="alink gold purple-hover alink-big" href="<?php echo get_permalink($linked_sermon); ?>">Watch the Sermon Video</a>
					</div>
					<?php endif; ?>
				</div>
			</div>
    </div>
  </section>
	<?php
	//-----------------------------
	// Linked Broadcasts
	//-----------------------------

	if ( ! empty( $linked_broadcasts ) ) :
	?>
	<hr class="divider-small">
	<section class="utb--mod utb--mod-recent">
		<div class="container fluid">
			<div class="utb--mod-head utb--align-center">
				<h1 class="utb--mod-title">From this Series</h1>
			</div>
			<div class="utb--posts grid col-pad">
			<?php
				foreach ( $linked_broadcasts as $linked_broadcast ) :
				$post_id     	 = $linked_broadcast['ID'];
				$post_title    = get_the_title($post_id);
				$post_link     = get_permalink($post_id);
				$post_date     = get_the_date( 'l, F j, Y', $post_id );
				$author_link   = get_author_posts_url( get_the_author_meta( 'ID' ), get_the_author_meta( 'user_nicename' ) );

				$pod_rel 				= pods( 'broadcast', $post_id );
				$rel_series 		= $pod_rel->field( 'linked_series' );
				$rel_sermon 		= $pod_rel->field( 'linked_sermon' );
				$rel_product 		= $pod_rel->field( 'linked_product' );

				$linked_series 	= $rel_series['ID'];
				$linked_sermon 	= $rel_sermon['ID'];
				$linked_product = $rel_product['ID'];
				?>
				<article class="utb--post column c1-4">

					<div class="utb--post-img-wrap">
						<a href="<?php echo $post_link; ?>">
							<div class="utb--post-img utb--img-16-9 utb--post-img-zoom loading" data-expand="-30">
							<?php
								$thumb_id       = get_post_thumbnail_id($post_id);
								$image_desktop  = wp_get_attachment_image_src( $thumb_id , 'large' );
								$image_tablet   = wp_get_attachment_image_src( $thumb_id , 'medium' );

								$img_width  = $image_desktop[1];
								$img_height = $image_desktop[2];

								if ( $width > $height ) {
									$orientation = 'utb--portrait';
								} else {
									$orientation =  'utb--landscape';
								}
								if ($thumb_id) :
								?>
								<img
									src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw=="
									data-src="<?php echo $image_desktop[0]; ?>"
									data-srcset="<?php echo $image_tablet[0]; ?> 300w,
									<?php echo $image_tablet[0]; ?> 600w,
									<?php echo $image_desktop[0]; ?> 900w"
									data-sizes="auto"
									class="<?php echo $orientation; ?> lazyload" />
								<?php
								else :
									echo '<img class="utb--landscape lazyload" data-src="' . get_stylesheet_directory_uri() . '/assets/img/default.jpg" />';
								endif; ?>
							</div>
						</a>
					</div>

					<div class="utb--post-body">
					<?php
						$post_type     = get_post_type( get_the_ID() );
						$topics        = get_the_terms( $post_id, 'topic' );
						if ( $topics && ! is_wp_error( $topics ) ) :
						?>
						<div class="utb--post-tags">
						<?php
							foreach ($topics as $topic) :
							$topic_link = get_term_link( $topic );
							?>
							<a class="alink dark serious" href="<?php echo $topic_link; ?>"><?php echo $topic->name; ?></a>
							<?php endforeach; ?>
						</div>
						<?php endif; ?>
						<?php if($post_title): ?>
						<h1 class="utb--post-title">
							<a href="<?php echo $post_link; ?>"><?php echo $post_title; ?></a>
						</h1>
						<?php endif; ?>
						<div class="utb--post-meta">
							<span class="utb--post-time"><?php echo $post_date; ?></span>
						</div>
						<?php if($linked_series): ?>
						<div class="utb--post-series">
							<span>From </span> <a class="alink gold dark-hover" href="<?php echo get_permalink($linked_series); ?>"><?php echo get_the_title($linked_series); ?></a>
						</div>
						<?php endif; ?>

						<?php if( '' != get_the_excerpt($post_id) || '' !== get_post($post_id)->post_content ) : ?>
						<div class="utb--post-content">
						<?php
						  if( '' != get_the_excerpt($post_id) ) {
						    echo '<p>'. get_the_excerpt($post_id).'</p>';
						  } else {
						    echo '<p>'. wp_trim_words( get_the_content($post_id), 50, '...' ).'</p>';
						  }
						?>
						</div>
						<?php endif; ?>

						<a class="btn btn-size-small btn-outline btn-light dark-text" href="<?php echo $post_link; ?>">Listen Now</a>
					</div>
				</article>
				<?php
				endforeach;
			?>
			</div>
		</div>
	</section>
	<?php
	endif;
	wp_reset_query();

	//-----------------------------
	// Linked Sermons
	//-----------------------------

	if ( ! empty( $linked_sermons ) ) :
	?>
	<hr class="divider-small">
	<section class="utb--mod utb--mod-recent">
		<div class="container fluid">
			<div class="utb--mod-head utb--align-center">
				<h1 class="utb--mod-title">Sermons</h1>
			</div>
			<div class="utb--posts grid col-pad">
			<?php
				foreach ( $linked_sermons as $linked_sermon ) :
				$post_id     	 = $linked_sermon['ID'];
				$post_title    = get_the_title($post_id);
				$post_link     = get_permalink($post_id);
				$post_date     = get_the_date( 'l, F j, Y', $post_id );
				$author_link   = get_author_posts_url( get_the_author_meta( 'ID' ), get_the_author_meta( 'user_nicename' ) );

				$pod_rel 				= pods( 'broadcast', $post_id );
				$rel_series 		= $pod_rel->field( 'linked_series' );
				$rel_sermon 		= $pod_rel->field( 'linked_sermon' );
				$rel_product 		= $pod_rel->field( 'linked_product' );

				$linked_series 	= $rel_series['ID'];
				$linked_sermon 	= $rel_sermon['ID'];
				$linked_product = $rel_product['ID'];
				?>
				<article class="utb--post column c1-4">

					<div class="utb--post-img-wrap">
						<a href="<?php echo $post_link; ?>">
							<div class="utb--post-img utb--img-16-9 utb--post-img-zoom loading" data-expand="-30">
							<?php
								$thumb_id       = get_post_thumbnail_id($post_id);
								$image_desktop  = wp_get_attachment_image_src( $thumb_id , 'large' );
								$image_tablet   = wp_get_attachment_image_src( $thumb_id , 'medium' );

								$img_width  = $image_desktop[1];
								$img_height = $image_desktop[2];

								if ( $width > $height ) {
									$orientation = 'utb--portrait';
								} else {
									$orientation =  'utb--landscape';
								}
								if ($thumb_id) :
								?>
								<img
									src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw=="
									data-src="<?php echo $image_desktop[0]; ?>"
									data-srcset="<?php echo $image_tablet[0]; ?> 300w,
									<?php echo $image_tablet[0]; ?> 600w,
									<?php echo $image_desktop[0]; ?> 900w"
									data-sizes="auto"
									class="<?php echo $orientation; ?> lazyload" />
								<?php
								else :
									echo '<img class="utb--landscape lazyload" data-src="' . get_stylesheet_directory_uri() . '/assets/img/default.jpg" />';
								endif; ?>
							</div>
						</a>
					</div>

					<div class="utb--post-body">
					<?php
						$post_type     = get_post_type( get_the_ID() );
						$topics        = get_the_terms( $post_id, 'topic' );
						if ( $topics && ! is_wp_error( $topics ) ) :
						?>
						<div class="utb--post-tags">
						<?php
							foreach ($topics as $topic) :
							$topic_link = get_term_link( $topic );
							?>
							<a class="alink dark serious" href="<?php echo $topic_link; ?>"><?php echo $topic->name; ?></a>
							<?php endforeach; ?>
						</div>
						<?php endif; ?>
						<?php if($post_title): ?>
						<h1 class="utb--post-title">
							<a href="<?php echo $post_link; ?>"><?php echo $post_title; ?></a>
						</h1>
						<?php endif; ?>
						<div class="utb--post-meta">
							<span class="utb--post-time"><?php echo $post_date; ?></span>
						</div>
						<?php if($linked_series): ?>
						<div class="utb--post-series">
							<span>From </span> <a class="alink gold dark-hover" href="<?php echo get_permalink($linked_series); ?>"><?php echo get_the_title($linked_series); ?></a>
						</div>
						<?php endif; ?>

						<?php if( '' != get_the_excerpt($post_id) || '' !== get_post($post_id)->post_content ) : ?>
						<div class="utb--post-content">
						<?php
						  if( '' != get_the_excerpt($post_id) ) {
						    echo '<p>'. get_the_excerpt($post_id).'</p>';
						  } else {
						    echo '<p>'. wp_trim_words( get_the_content($post_id), 50, '...' ).'</p>';
						  }
						?>
						</div>
						<?php endif; ?>

						<a class="btn btn-size-small btn-outline btn-light dark-text" href="<?php echo $post_link; ?>">Listen Now</a>
					</div>
				</article>
				<?php
				endforeach;
			?>
			</div>
		</div>
	</section>
	<?php
	endif;
	wp_reset_query();


	//*****
	// Promo Section
	//*****
	get_template_part('partials/posts/tpart-promo');

	//*****
	// Subscribe newsletter
	//*****
	get_template_part('partials/posts/tpart-subscribe');

	get_template_part('partials/layout/tpart-end-page');
get_footer();
