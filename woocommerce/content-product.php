<?php
defined( 'ABSPATH' ) || exit;
global $product;
// Ensure visibility.
if ( empty( $product ) || ! $product->is_visible() ) {
	return;
}
$term 					= get_term_by( 'slug', get_query_var( 'term' ), get_query_var( 'taxonomy' ) );
$taxonomy 			= $term->taxonomy;
$fetchterm 			= get_queried_object()->term_id;
$has_children 	= get_terms($taxonomy, array('parent' => $fetchterm, 'hide_empty' => false));

$layout = '';
if (is_shop()) {
	$layout = ' c1-2';
} elseif(!empty($has_children)) {
	$layout = ' c1-2';
} elseif(is_product_category()) {
	$layout = ' c1-2';
} else {
	$layout = ' c1-3';
}

?>
<article class="mix utb--product column<?php echo $layout; ?>" data-price="<?php echo $product->get_price(); ?>" data-published-date="<?php echo get_the_date('Y-m-d', $post_id); ?>">
	<div class="product-inner">
		<div class="product-body grid align-middle">
			<div class="product-image">
				<a href="<?php echo get_permalink( $product->ID ); ?>">
					<?php do_action( 'woocommerce_before_shop_loop_item_title' ); ?>
				</a>
			</div>
			<div class="product-content">
				<a href="<?php echo get_permalink( $product->ID ); ?>">
					<?php do_action( 'woocommerce_shop_loop_item_title' ); ?>
					<?php //if(is_shop()) {echo '<span>More information &#187;</span>';} ?>
				</a>
				<?php do_action( 'woocommerce_after_shop_loop_item' ); ?>
			</div>
		</div>
	</div>
</article>
