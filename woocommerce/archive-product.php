<?php
//*****
// Store Home Template
//*****
get_header( 'shop' );
  //page wrappers
	get_template_part('partials/layout/tpart-start-page');
	//page subnavigation
	get_template_part('partials/page/tpart-subnav');
  //store wrappers
  get_template_part('partials/layout/tpart-start-shop');
  //store banner
  $slides 	= get_field('store_slides', 'option');
  if(!empty($slides)) {
  ?>
  <section class="store-banner utb--slides">
		<?php foreach($slides as $slide) : ?>
			<div class="utb--slide">
				<?php if($slide['slide_link']) : ?>
				<a href="<?php echo $slide['slide_link']; ?>">
				<?php endif; ?>
	    		<?php echo wp_get_attachment_image( $slide['slide_image']['ID'], 'full' ); ?>
				<?php if($slide['slide_link']) : ?>
				</a>
				<?php endif; ?>
			</div>
		<?php endforeach; ?>
  </section>
  <?php
  } //end store banner
  echo '<div class="container">';
    echo '<div class="utb--products grid'. ($banner !== '' ? ' banner-active' : '') .'">';

		$choices 	= get_field('featured_products', 'option');
		if($choices) {
			$args = array(
				'post_type' => 'product',
				'posts_per_page' => 4,
				'post__in' => $choices
			);
		} else {
			$args = array(
				'post_type' => 'product',
				'posts_per_page' => 4
			);
		}
		$products = new WP_Query( $args );

  	if ( $products->have_posts() ) {
  		while ( $products->have_posts() ) : $products->the_post();
  			do_action( 'woocommerce_shop_loop' );
  			wc_get_template_part( 'content', 'product' );
  		endwhile;
    } else {
			echo __( 'No products found' );
		}
		wp_reset_postdata();
    echo '</div>';
  echo '</div>';
	//do_action( 'woocommerce_after_shop_loop' );

  get_template_part('partials/layout/tpart-end-shop'); // END store wrappers
	get_template_part('partials/layout/tpart-end-page'); // END page wrapers
get_footer();
