<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}
?>

<form role="search" class="woocommerce-product-search" action="<?php bloginfo('home'); ?>/" method="get">
	<input type="text" id="search-product" value="" name="s" placeholder="Search Store">
	<input type="hidden" name="post_type" value="product" />
	<i class="icon-magnifier"></i>
</form>
