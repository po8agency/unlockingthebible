<?php
/**
 * The Template for displaying products in a product category. Simply includes the archive template
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/taxonomy-product_cat.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see 	    https://docs.woocommerce.com/document/template-structure/
 * @package 	WooCommerce/Templates
 * @version     1.6.4
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

get_header();
	get_template_part('partials/layout/tpart-start-page');
	//page subnavigation
	get_template_part('partials/page/tpart-subnav');
	//store wrappers
  get_template_part('partials/layout/tpart-start-shop');

	//-----------------------------
	// Get posts from current queried topic
	//-----------------------------
	$term 					= get_term_by( 'slug', get_query_var( 'term' ), get_query_var( 'taxonomy' ) );
	$term_id 				= get_queried_object();
	$taxonomy 			= $term->taxonomy;
	$parent 				= get_term_by( 'id', $term->parent, get_query_var( 'taxonomy' ) );
	$fetchterm 			= get_queried_object()->term_id;
  $termid 				= get_term($fetchterm, $taxonomy );
	$has_children 	= get_terms($taxonomy, array('parent' => $fetchterm, 'hide_empty' => false));
	?>
	<section class="utb--store-tax">
		<div class="container<?php echo empty($has_children) ? '' : ' grid'; ?>">
		<?php
			if(!empty($has_children)) :
			?>
			<aside class="taxonomy-sidebar">
			<?php if ( $termid->parent > 0 ) : ?>
				<h3>More under <?php echo $parent->name; ?></h3>
				<ul class="taxonomy-children">
				<?php
				$args = array(
					'orderby'       => 'name',
					'order'         => 'ASC',
					'hide_empty'    => false,
					'child_of'      => $termid->parent,
				);
				$siblingproducts = get_terms( $taxonomy, $args);
				foreach ($siblingproducts as $siblingproduct) {
					?>
					<li><a href="<?php echo get_term_link( $siblingproduct ); ?>"><?php echo $siblingproduct->name; ?></a></li>
					<?php }
				?>
				</ul>
			<?php else : ?>
				<h3>More in <?php echo $term->name; ?></h3>
				<ul class="taxonomy-children">
				<?php
				$args = array(
					'orderby'       => 'name',
					'order'         => 'ASC',
					'hide_empty'    => false,
					'child_of'      => $fetchterm
				);
				$subproducts = get_terms( $taxonomy, $args);
				foreach ($subproducts as $subproduct) {
					?>
					<li><a href="<?php echo get_term_link( $subproduct ); ?>"><?php echo $subproduct->name; ?></a></li>
					<?php }
				?>
				</ul>
			<?php endif; ?>
			</aside>
			<?php endif; ?>

			<div class="taxonomy-content<?php echo empty($has_children) ? '' : ' has-sidebar'; ?>">
			<?php
				$cover_image	= get_field('taxonomy_cover_image', $term_id);

				$image_desktop  = wp_get_attachment_image_src($cover_image['ID'], 'large');
				$image_tablet   = wp_get_attachment_image_src($cover_image['ID'], 'medium');

				if ( $width > $height ) {
					$orientation = 'utb--portrait';
				} else {
					$orientation =  'utb--landscape';
				}
				if ($cover_image) :
				?>
				<div class="taxonomy-hero">
					<img
						src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw=="
						data-src="<?php echo $image_desktop[0]; ?>"
						data-srcset="<?php echo $image_tablet[0]; ?> 300w,
						<?php echo $image_tablet[0]; ?> 600w,
						<?php echo $image_desktop[0]; ?> 900w"
						data-sizes="auto"
						class="<?php echo $orientation; ?> lazyload" />
				</div>
				<?php endif; ?>
				<div class="grid utb--products" id="content">
				<?php
				$args = array(
					'post_type' 			=> 'product',
					'post_status' 		=> 'publish',
					'orderby' 				=> 'title',
          'order' 					=> 'ASC',
					'posts_per_page' 	=> -1,
					'tax_query' => array(
						'relation' => 'AND', array(
							'taxonomy' => $taxonomy,
							'field'    => 'ID',
							'terms'    => $term_id
						)
					),
				);
				$products = new WP_Query( $args );
		  	if ( $products->have_posts() ) {
		  		while ( $products->have_posts() ) : $products->the_post();

		  			do_action( 'woocommerce_shop_loop' );
		  			wc_get_template_part( 'content', 'product' );
		  		endwhile;
					?>
					<div class="controls-pagination">
	          <div class="mixitup-page-list"></div>
	        </div>
					<?php
		    } else {
					echo __( 'No products found' );
				}
				?>
				</div>
			</div>
		</div>
	</section>
  <?php
	get_template_part('partials/layout/tpart-end-page');
get_footer();
