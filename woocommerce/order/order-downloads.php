<?php
/**
 * Order Downloads.
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/order/order-downloads.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @author  Automattic
 * @package WooCommerce/Templates
 * @version 3.3.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}
?>
<section class="woocommerce-order-downloads">

	<h3 class="woocommerce-order-downloads__title"><?php esc_html_e( 'Downloads', 'woocommerce' ); ?></h3>

	<div class="woocommerce-store-table woocommerce-downloads-table">
		<div class="woocommerce-table-head grid">
			<?php foreach ( wc_get_account_downloads_columns() as $column_id => $column_name ) : ?>
			<div class="column table-head-<?php echo esc_attr( $column_id ); ?>"><span class="nobr"><?php echo esc_html( $column_name ); ?></span></div>
			<?php endforeach; ?>
		</div>

		<div class="woocommerce-table-body">
		<?php foreach ( $downloads as $download ) : ?>
			<div class="grid woocommerce-downloads-table-row download">
				<?php foreach ( wc_get_account_downloads_columns() as $column_id => $column_name ) : ?>
					<div class="column woocommerce-downloads-table-col" data-title="<?php echo esc_attr( $column_name ); ?>">
						<?php
						if ( has_action( 'woocommerce_account_downloads_column_' . $column_id ) ) {
							do_action( 'woocommerce_account_downloads_column_' . $column_id, $download );
						} else {
							switch ( $column_id ) {
								case 'download-product':
									if ( $download['product_url'] ) {
										echo '<a class="alink gold purple-hover" href="' . esc_url( $download['product_url'] ) . '">' . esc_html( $download['product_name'] ) . '</a>';
									} else {
										echo esc_html( $download['product_name'] );
									}
									break;
								case 'download-file':
									echo '<a href="' . esc_url( $download['download_url'] ) . '" class="btn btn-size-small btn-solid btn-gold">Download File</a>';
									break;
								case 'download-remaining':
									//echo is_numeric( $download['downloads_remaining'] ) ? esc_html( $download['downloads_remaining'] ) : esc_html__( '&infin;', 'woocommerce' );
									break;
								case 'download-expires':
									if ( ! empty( $download['access_expires'] ) ) {
										//echo '<time datetime="' . esc_attr( date( 'Y-m-d', strtotime( $download['access_expires'] ) ) ) . '" title="' . esc_attr( strtotime( $download['access_expires'] ) ) . '">' . esc_html( date_i18n( get_option( 'date_format' ), strtotime( $download['access_expires'] ) ) ) . '</time>';
									} else {
										//esc_html_e( 'Never', 'woocommerce' );
									}
									break;
							}
						}
						?>
					</div>
				<?php endforeach; ?>
			</div>
		<?php endforeach; ?>
		</div>
	</div>
</section>
