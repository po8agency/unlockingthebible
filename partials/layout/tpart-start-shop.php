<?php
 global $woocommerce;
 global $product;
 $queried_product     = get_queried_object();
 $product_id		      = $queried_product->ID;
 $cart_contents_count = $woocommerce->cart->get_cart_contents_count();
 $permalink = get_site_url() . '/store/my-account/';
 ?>
<section class="utb--store notranslate">
  <div class="utb--store-head">
    <div class="container">
      <div class="store-head-left">
        <div class="store-filters">
          <ul class="has-dropdowns">
            <li class="dropdown-item has-dropdown">
              <a class="browse-button" href="#">
                <span>Browse by Format</span>
              </a>
              <div class="dropdown dropdown-normal">
                <?php click_taxonomy_dropdown( 'product_cat' ); ?>
              </div>
            </li>
          </ul>
        </div>
      </div>
      <div class="store-head-right">
        <ul class="store-head-menu has-dropdowns">
          <li>
            <span>
            <?php
            if ( is_user_logged_in() ) {
              $current_user = wp_get_current_user();
              echo '<span>Hello ' . $current_user->user_firstname . '.</span>';
            } else {
              echo '<span>Welcome, visitor!</span>';
            }
            ?>
            </span>
          </li>
          <?php if ( is_user_logged_in() ) { ?>
          <li class="dropdown-item has-dropdown">
            <a class="account-button" href="#" title="<?php _e( 'View your account' ); ?>">
              <span>Your Account</span>
            </a>
            <div class="dropdown dropdown-normal">
              <ul>
                <li><a href="<?php echo wc_get_endpoint_url( 'orders', $value, $permalink ); ?>">Recent Orders</a></li>
                <li><a href="<?php echo wc_get_endpoint_url( 'edit-address', $value, $permalink ); ?>">Billing & Shipping</a></li>
                <li><a href="<?php echo wc_get_endpoint_url( 'edit-account', $value, $permalink ); ?>">Edit Password</a></li>
                <li><a href="<?php echo wc_get_endpoint_url( 'customer-logout', $value, $permalink ); ?>">Logout</a></li>
              </ul>
            </div>
          </li>
          <?php } ?>
          <li class="dropdown-item has-dropdown dropdown-cart-button<?php echo $cart_contents_count > 0 ? ' cart-is-empty' : '' ?>">
            <a class="cart-button" href="#" title="<?php _e( 'View your shopping cart' ); ?>">
              <i class="icon-<?php echo $cart_contents_count < 1 ? 'basket' : 'basket-loaded' ?>"></i>
              <span class="utb-store-cart-count">Cart (<?php echo WC()->cart->get_cart_contents_count(); ?>)</span>
            </a>
            <div class="dropdown dropdown-special mini-cart-dropdown"><?php woocommerce_mini_cart(); ?></div>
          </li>
        </ul>
        <div class="store-search-box"><?php echo get_product_search_form(); ?></div>
      </div>
    </div>
  </div>
  <div class="utb--store-body">
  <?php
    if(is_tax('product_format')) :
    ?>
    <div class="container">
      <ul class="taxonomy-breacrumbs">
        <li><a href="<?php echo home_url(); ?>/store">Home</a></li>
        <?php
          $term = get_term_by("slug", get_query_var("term"), get_query_var("taxonomy") );
          $tmpTerm = $term;
          $tmpCrumbs = array();
          while ($tmpTerm->parent > 0){
            $tmpTerm = get_term($tmpTerm->parent, get_query_var("taxonomy"));
            $crumb = '<li><a href="' . get_term_link($tmpTerm, get_query_var('taxonomy')) . '">' . $tmpTerm->name . '</a></li>';
            array_push($tmpCrumbs, $crumb);
          }
          echo implode('', array_reverse($tmpCrumbs));
          echo '<li>' . $term->name . '</li>';
        ?>
      </ul>
    </div>
    <?php
    elseif(is_product()) :
    ?>
    <div class="container">
      <ul class="taxonomy-breacrumbs">
        <li><a href="<?php echo home_url(); ?>/store">Home</a></li>
        <?php
        $product_tax = 'product_cat'; // region taxonomy
        $topics = wp_get_post_terms( $post->ID, $product_tax, array( "fields" => "ids" ) ); // getting the term IDs
        if( $topics ) {
          $topic_array = trim( implode( ',', (array) $topics ), ' ,' );
          $newordertopics = get_terms($product_tax, 'orderby=none&include=' . $topic_array );
          foreach( $newordertopics as $topic ) {
            echo '<li><a href="' . get_term_link( $topic ) . '">' . $topic->name . '</a></li>';
          }
        } else {
          echo '<li>'.get_the_title().'</li>';
        }
        ?>
      </ul>
    </div>
    <?php
    endif;
  ?>
  <?php
  if(!is_cart() && !is_checkout()) {
    if(!is_shop() && !is_checkout() && !is_account_page() && !is_product()) {
      $term = get_term_by( 'slug', get_query_var( 'term' ), get_query_var( 'taxonomy' ) );

      echo '<div class="container">';
        echo '<div class="utb-archive-head grid align-bottom">';
          if($term) :
          echo '<h1>'.$term->name.'</h1>';
          else :
          echo '<h1>Results</h1>';
          endif;
          echo '<div class="archive-head-result grid align-middle">';
            get_template_part('templates/parts/store/loop/tpart-before-shop');
          echo '</div>';
        echo '</div>';
      echo '</div>';
    } elseif(!is_shop() && !is_product()) {
      echo '<div class="container">';
        echo '<div class="utb-archive-head grid align-bottom">';
          echo '<h1>'.get_the_title().'</h1>';
        echo '</div>';
      echo '</div>';
    } else {

    }
  } elseif(is_cart() || is_checkout() || is_wc_endpoint_url( 'order-pay' ) || is_wc_endpoint_url( 'order-received' )) {
    echo '<div class="container">';
      echo '<div class="utb-archive-head grid align-bottom">';
        echo '<ul class="order-steps">';
        if(is_cart()) :
          ?>
          <li class="order-step active">Shopping cart</li>
          <li class="order-step"> <a href="<?php echo esc_url( wc_get_checkout_url() );?>">Checkout Details</a></li>
          <li class="order-step">Order Complete</li>
          <?php
        elseif(is_wc_endpoint_url( 'order-received' )) :
          ?>
          <li class="order-step">Shopping cart</li>
          <li class="order-step">Checkout Details</li>
          <li class="order-step active">Order Complete</li>
          <?php
        elseif(is_checkout() || is_wc_endpoint_url( 'order-pay' )) :
          ?>
          <li class="order-step"><a href="<?php echo get_permalink( wc_get_page_id( 'cart' ) ); ?>">Shopping cart</a></li>
          <li class="order-step active">Checkout Details</li>
          <li class="order-step">Order Complete</li>
          <?php
        else :

        endif;
        echo '</ul>';
      echo '</div>';
    echo '</div>';
  }
  ?>
