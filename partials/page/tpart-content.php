<?php
  if ( !empty( get_the_content() ) ) { ?>
    <section class="utb--page">
      <div class="container">
        <div class="utb--page-title-area utb--align-center page-title-special">
          <h1 class="utb--page-title "><?php echo get_the_title(); ?></h1>
        </div>
        <?php
        echo '<div class="utb--content'. (basename(get_page_template()) === 'page.php' ? ' content-default' : ' content-tpl') .'">';
          the_content();
        echo '</div>';
        ?>
      </div>
    </section>
    <?php
    } else {
    ?>
    <section class="utb-error-404">
  		<div class="container utb--compact">
  			<div class="utb--page-title-area utb--align-center page-title-special">
          <h1 class="utb--page-title ">No Content Found</h1>
        </div>
  			<img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/404.svg" width="256" height="256" />
  			<h1 class="utb--page-title">Oops. Looks like this page needs some content.</h3>
  			<p>“Predicting Rain Doesn’t Count. Building Arks Does”</p>
        <?php echo '' . edit_post_link('Edit Page', '<div class="builder-404-action">', '</div>', null, 'btn btn-size-small btn-solid btn-gold') . ''; ?>
  		</div>
  	</section>
    <?php
  }
