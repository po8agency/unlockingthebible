<?php
//*****
// Content Module
//*****
?>
<section class="utb--mod utb--mod-content">
  <div class="container">
    <?php if(get_sub_field('title')) : ?>
    <div class="utb--page-title-area utb--align-center page-title-special">
      <h1 class="utb--page-title "><?php the_sub_field('title'); ?></h1>
    </div>
    <?php endif; ?>
    <div class="utb--content content-default">
      <?php the_sub_field('content'); ?>
    </div>
  </div>
</section>
