<?php
//*****
//Featured POSTS module
//*****
$posttypes        = get_sub_field('post_types');
$page_title       = get_the_title();
?>
<section class="utb--mod utb--mod-featured">
  <div class="container">
    <div class="utb--page-title-area utb--align-center page-title-special">
      <h1 class="utb--page-title "><?php echo $page_title; ?></h1>
    </div>
    <div class="utb--posts utb--posts-latest grid">
    <?php
    $counter = count($posttypes);

    for($i=0; $i<$counter;$i++) {
    $post_args = array(
      'post_type' => $posttypes[$i],
      'post_status' => 'publish',
      'posts_per_page' => 1
    );
    $latest_query = new WP_Query( $post_args );

    if ( $latest_query->have_posts() ) :
      while ( $latest_query->have_posts() ) : $latest_query->the_post();
      $post_series = get_field('linked_series', get_the_ID());
      ?>
      <article class="utb--post column c1-2">
      <?php
        $post_title    = get_the_title();
        $post_link     = get_permalink();
        $post_date     = get_the_date( 'l, F j, Y', $post_id );
        $post_type     = get_post_type( get_the_ID() );

        $pod_rel 				= pods( $post_type, get_the_id() );
        $rel_series 		= $pod_rel->field( 'linked_series' );
        $rel_sermon 		= $pod_rel->field( 'linked_sermon' );
        $rel_broadcast  = $pod_rel->field( 'linked_broadcast' );
        $rel_product 		= $pod_rel->field( 'linked_product' );

        $linked_series 	  = $rel_series['ID'];
        $linked_sermon 	  = $rel_sermon['ID'];
        $linked_broadcast = $rel_broadcast['ID'];
        $linked_product   = $rel_product['ID'];
        ?>
        <div class="utb--all-url utb--all-top">
          <?php if($post_type == 'broadcast') : ?>
          <a href="/broadcasts">Radio Program<i class="icon-arrow-right"></i></a>
          <?php elseif($post_type == 'sermon') : ?>
          <a href="/sermons">Sermon<i class="icon-arrow-right"></i></a>
          <?php elseif($post_type == 'lifekey') : ?>
          <a href="/lifekeys">Daily Devotional<i class="icon-arrow-right"></i></a>
          <?php elseif($post_type == 'story') : ?>
          <a href="/stories">Story<i class="icon-arrow-right"></i></a>
          <?php endif; ?>
        </div>
        <div class="utb--post-img-wrap">
          <a href="<?php echo $post_link; ?>">
            <div class="utb--post-img utb--img-16-9 utb--post-img-zoom loading">
            <?php
                $thumb_id       = get_post_thumbnail_id();
                $image_desktop  = wp_get_attachment_image_src( $thumb_id , 'large' );
                $image_tablet   = wp_get_attachment_image_src( $thumb_id , 'medium' );
                $image_mobile   = wp_get_attachment_image_src( $thumb_id , 'thumbnail' );

                $img_width  = $image_desktop[1];
                $img_height = $image_desktop[2];

                if ( $width > $height ) {
                  $orientation = 'utb--portrait';
                } else {
                  $orientation =  'utb--landscape';
                }
                if ($thumb_id) :
                ?>
                <img
                  src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw=="
                  data-src="<?php echo $image_desktop[0]; ?>"
                  data-srcset="<?php echo $image_mobile[0]; ?> 300w,
                  <?php echo $image_tablet[0]; ?> 600w,
                  <?php echo $image_desktop[0]; ?> 900w"
                  data-sizes="auto"
                  class="<?php echo $orientation; ?> lazyload" />
                <?php
                else :
                  echo '<img class="utb--landscape lazyload" data-src="' . get_stylesheet_directory_uri() . '/assets/img/default.jpg" />';
                endif; ?>
            </div>
          </a>
        </div>
        <div class="utb--post-body">
          <?php if($post_title): ?>
          <h1 class="utb--post-title">
            <a href="<?php echo $post_link; ?>"><?php echo $post_title; ?></a>
          </h1>
          <?php endif; ?>
          <div class="utb--post-meta">
            <span class="utb--post-time"><?php echo $post_date; ?></span>
          </div>
          <?php if($linked_series): ?>
          <div class="utb--post-series">
            <span>From </span> <a class="alink gold dark-hover" href="<?php echo get_permalink($linked_series); ?>"><?php echo get_the_title($linked_series); ?></a>
          </div>
          <?php endif; ?>
          <div class="utb--post-content">
            <p><?php echo wp_trim_words( get_the_content(), 50, '...' ); ?></p>
          </div>
          <?php if($post_type == 'broadcast') : ?>
          <a class="btn btn-size-small btn-outline btn-light dark-text" href="<?php echo $post_link; ?>">Listen Now</a>
          <?php elseif($post_type == 'sermon') : ?>
          <a class="btn btn-size-small btn-outline btn-light dark-text" href="<?php echo $post_link; ?>">Watch now</a>
          <?php
          endif;
          if($post_type == 'broadcast') {
            $more_text = 'Radio Programs';
          } elseif($post_type == 'sermon') {
            $more_text = 'Sermon Videos';
          } else {
            $more_text = 'Posts';
          }
          ?>
          <div class="utb--all-url utb--all-bot">
            <a class="alink gold dark-hover" href="/<?php echo $post_type; ?>s">More <?php echo $more_text; ?></a>
          </div>

        </div>
      </article>
      <?php
      endwhile;
    endif;
    wp_reset_query();
    }
    ?>
    </div>
  </div>
</section>
