<?php
//*****
//Topics module fields
//*****
$mod_options     = get_sub_field('section_options');

?>
<section class="utb--mod utb--mod-topics">
  <div class="container">
    <div class="utb--mod-wrap">
      <div class="utb--page-title-area utb--align-center">
        <h1 class="utb--page-title">Topics</h1>
      </div>
      <div class="utb--topics">
      <?php
        //check for topics taxonomy
        $topic_terms = get_terms(
          array(
          'taxonomy' => 'topic',
          'hide_empty' => false
          )
        );
        if($topic_terms) {
          $total_count = count($topic_terms);
          echo '<ul class="utb--topic-terms grid ' . ($total_count < 4 ?' utb--align-center' :  '') . '">';
            if($total_count == 1) {
              $count_val = '1-1';
            } else if($total_count == 2) {
              $count_val = '1-2';
            } else if($total_count == 3) {
              $count_val = '1-3';
            } else if($total_count == 4) {
              $count_val = '1-4';
            } else if($total_count == 5) {
              $count_val = '1-5';
            } else {
              $count_val = '1-6';
            }
            foreach($topic_terms as $topic) :
              echo '<li class="utb--topic-term column c' . $count_val . '"><a href="'. get_term_link($topic->slug, 'topic') .'">' . $topic->name . '</a></li>';
            endforeach;
          echo '</ul>';
        }
      ?>
      </div>
      <?php
      if($topic_terms) {
      $total_count = count($topic_terms);
      if($total_count >= 13) {
      ?>
      <div class="utb--align-center topics-more">
        <a class="btn btn-size-small btn-outline btn-light dark-text" href="#">All Topics</a>
      </div>
        <?php }
      } ?>
    </div>
  </div>
</section>
