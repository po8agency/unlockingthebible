<?php
//*****
//Recent POSTS module
//*****
$mod_options     = get_sub_field('section_options');
$post_type       = $mod_options['post_type'];
$mod_bg_type     = $mod_options['background_color'];

if($mod_bg_type == 'light') {
  $mod_bg = 'background-color: #f2f2f0; ';
} elseif($mod_bg_type == 'white') {
  $mod_bg = 'background-color: white; ';
} else {
  $mod_bg = '';
}

$mod_title       = get_sub_field('section_title');

if($mod_title) {
  $mod_title  = $mod_title;
} else {
  $mod_title  = 'Recent ' . $post_type['label'];
}

$mod_btn_txt     = get_sub_field('archive_btn_txt');

if($mod_btn_txt) {
  $mod_btn_txt = $mod_btn_txt;
} else {
  $mod_btn_txt = $post_type['label'] . 'Archive';
}
?>
<section class="utb--mod utb--mod-recent" style="<?php echo $mod_bg; ?>" id="<?php echo $post_type; ?>">
  <div class="container fluid">
    <div class="utb--mod-head utb--align-center">
      <?php if($mod_title) : ?>
      <h1 class="utb--mod-title"><?php echo $mod_title; ?></h1>
      <?php endif; ?>
    </div>
    <div class="utb--posts grid col-pad">
    <?php
    $recent_args = array(
      'post_type' => $post_type,
      'post_status' => 'publish',
      'posts_per_page' => 4,
      'offset' => 1
    );
    $recent_query = new WP_Query( $recent_args );

    if ( $recent_query->have_posts() ) :
      while ( $recent_query->have_posts() ) : $recent_query->the_post();

      $post_title    = get_the_title();
      $post_link     = get_permalink();
      $post_type     = get_post_type( get_the_ID() );
      $post_date     = get_the_date( 'l, F j, Y', $post_id );
      $author_link   = get_author_posts_url( get_the_author_meta( 'ID' ), get_the_author_meta( 'user_nicename' ) );

      $topics        = get_the_terms( $post_id, 'topic' );

      $pod_rel 				= pods(  $post_type, get_the_id() );
      $rel_series 		= $pod_rel->field( 'linked_series' );
      $rel_sermon 		= $pod_rel->field( 'linked_sermon' );
      $rel_broadcast  = $pod_rel->field( 'linked_broadcast' );
      $rel_product 		= $pod_rel->field( 'linked_product' );

      $linked_series 	  = $rel_series['ID'];
      $linked_sermon 	  = $rel_sermon['ID'];
      $linked_broadcast = $rel_broadcast['ID'];
      $linked_product   = $rel_product['ID'];

      if($post_type == 'broadcast') {
        $link_text = 'Listen Now';
      } elseif($post_type == 'sermon') {
        $link_text = 'Watch Now';
      }else {
        $link_text = 'Read Post';
      }

      ?>
      <article class="utb--post column c1-4">
        <?php if($post_type != 'lifekey' ) : ?>
        <div class="utb--post-img-wrap">
          <a href="<?php echo $post_link; ?>">
            <div class="utb--post-img utb--img-16-9 utb--post-img-zoom loading" data-expand="-30">
            <?php
              $thumb_id       = get_post_thumbnail_id();
              $image_desktop  = wp_get_attachment_image_src( $thumb_id , 'large' );
              $image_tablet   = wp_get_attachment_image_src( $thumb_id , 'medium' );

              $img_width  = $image_desktop[1];
              $img_height = $image_desktop[2];

              if ( $width > $height ) {
                $orientation = 'utb--portrait';
              } else {
                $orientation =  'utb--landscape';
              }
              if ($thumb_id) :
              ?>
              <img
                src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw=="
                data-src="<?php echo $image_desktop[0]; ?>"
                data-srcset="<?php echo $image_tablet[0]; ?> 300w,
                <?php echo $image_tablet[0]; ?> 600w,
                <?php echo $image_desktop[0]; ?> 900w"
                data-sizes="auto"
                class="<?php echo $orientation; ?> lazyload" />
              <?php
              else :
                echo '<img class="utb--landscape lazyload" data-src="' . get_stylesheet_directory_uri() . '/assets/img/default.jpg" />';
              endif; ?>
            </div>
          </a>
        </div>
        <?php endif; ?>
        <div class="utb--post-body">
        <?php
          if ( $topics && ! is_wp_error( $topics ) ) :
          ?>
          <div class="utb--post-tags">
          <?php
            foreach ($topics as $topic) :
            $topic_link = get_term_link( $topic );
            ?>
            <a class="alink dark serious" href="<?php echo $topic_link; ?>"><?php echo $topic->name; ?></a>
            <?php endforeach; ?>
          </div>
          <?php endif; ?>
          <?php if($post_title): ?>
          <h1 class="utb--post-title">
            <a href="<?php echo $post_link; ?>"><?php echo $post_title; ?></a>
          </h1>
          <?php endif; ?>
          <div class="utb--post-meta">
            <span class="utb--post-time"><?php echo $post_date; ?></span>
            <a class="alink dark serious" href="<?php echo $author_link; ?>"><span class="utb--post-author"><?php the_author(); ?></span></a>
          </div>

          <?php if($linked_series): ?>
          <div class="utb--post-series">
            <span>From </span> <a class="alink gold dark-hover" href="<?php echo get_permalink($linked_series); ?>"><?php echo get_the_title($linked_series); ?></a>
          </div>
          <?php endif; ?>

          <div class="utb--post-content">
            <p><?php echo wp_trim_words( get_the_content(), 50, '...' ); ?></p>
          </div>

          <a class="btn btn-size-small btn-outline btn-light dark-text" href="<?php echo $post_link; ?>"><?php echo $link_text; ?></a>

        </div>
      </article>
      <?php
      endwhile;
    endif;
    wp_reset_query();
    ?>
    </div>
    <div class="utb--mod-foot utb--align-center">
      <a class="alink gold purple-hover" href="/archive#type=<?php echo $post_type; ?>"><?php echo $mod_btn_txt; ?></a>
    </div>
  </div>
</section>
