<?php
//*****
//Promotional Module Fields
//*****

//layout options
$mod_options      = get_sub_field('section_options');
$mod_bg           = $mod_options['background_color'];
$mod_img_align    = $mod_options['image_align'];
$mod_colors       = $mod_options['color_scheme'];
$color_scheme     = 'mod-color-'.$mod_colors.'';
$mod_cont_align   = $mod_options['content_align'];
$mod_text_align   = $mod_options['text_align'];

//module content
$mod_image        = get_sub_field('promo_image');
$img_size         = 'full';
$mod_content      = get_sub_field('promo_content');
$mod_title        = $mod_content['title'];
$mod_subtitle     = $mod_content['subtitle'];
$mod_copy         = $mod_content['copy'];
$mod_btn_text     = $mod_content['button_text'];
$mod_btn_url      = $mod_content['button_url'];

if ($mod_img_align == 'imageright') {$img_align = ' mod-image-right';} else {$img_align = ' mod-image-left';}
if ($mod_text_align == 'right') {
  $text_align = 'mod-text-right';
} elseif ($mod_text_align == 'left') {
  $text_align = 'mod-text-left';
} else {
  $text_align = 'mod-text-center';
}

$overrides      = $img_align . ' ' . $text_align . ' ' . $color_scheme;
?>
<section class="utb--mod utb--mod-promo" style="background-color: <?php echo $mod_bg; ?>;">
  <div class="container">
    <div class="utb--mod-wrap">
      <div class="grid align-bottom">
        <div class="column c1-2 col-pad utb--mod-content<?php echo ' ' . $overrides; ?>">
          <?php if($mod_title) : ?>
          <h1 class="utb--mod-title"><?php echo $mod_title; ?></h1>
          <?php
          endif;

          if($mod_cont_align == 'bot') {
            if($mod_copy) {echo '<p class="utb-mod-copy">' . $mod_copy . '</p>';}
            if($mod_subtitle) {echo '<h2 class="utb--mod-subtitle">' . $mod_subtitle . '</h2>';}
          } else {
            if($mod_subtitle) {echo '<h2 class="utb--mod-subtitle">' . $mod_subtitle . '</h2>';}
            if($mod_copy) {echo '<p class="utb-mod-copy">' . $mod_copy . '</p>';}
          }

          if($mod_btn_text) :
          ?>
          <div class="utb--mod-action">
            <a class="btn btn-outline btn-size-small dark-text btn-light" href="<?php echo $mod_btn_url; ?>" target="_blank"><?php echo $mod_btn_text; ?></a>
          </div>
          <?php endif; ?>
        </div>
        <div class="column c1-2 col-pad utb--mod-image">
        <?php if( $mod_image ) {echo wp_get_attachment_image( $mod_image, $img_size, false, array('title' => ''.$pro_title.'', 'alt' => ''.$pro_title.'', 'class' => 'img-fluid align-bottom')); } ?>
        </div>
      </div>
    </div>
  </div>
</section>
