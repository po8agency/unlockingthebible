<?php
//*****
//Newsletter module fields
//*****
$mod_options     = get_sub_field('section_options');
$mod_bg          = $mod_options['background_color'];
$mod_form_id     = $mod_options['form_id'];

$mod_title       = $mod_options['title'];
$mod_content     = $mod_options['content'];
?>
<section class="utb--mod utb--mod-subscribe mod-sub-padding" style="background-color:<?php echo $mod_bg; ?>;">
  <div class="container">
    <div class="utb--mod-wrap">
      <h3 class="utb--mod-title"><?php echo $mod_title; ?></h3>
      <p class="utb-mod-copy"><em><?php echo $mod_content; ?></em></p>
      <div class="utb--news-form">
      <?php gravity_form( $mod_form_id, $display_title = false, $display_description = false, $display_inactive = false, $field_values = null, $ajax = true, $tabindex, $echo = true ); ?>
      </div>
      <span class="utb--mob-info-snippet">We will never sell or misuse your information.</span>
    </div>
  </div>
</section>
