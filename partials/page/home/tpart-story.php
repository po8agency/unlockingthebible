<?php
//*****
// Home latest story template
//*****

$args = array(
	'post_type' => 'story', // enter your custom post type
	'orderby' => 'date',
	'order' => 'DESC',
	'posts_per_page'=> 1, // overrides posts per page in theme settings
);
$loop = new WP_Query( $args );
if( $loop->have_posts() ):
  while( $loop->have_posts() ): $loop->the_post(); global $post;
  ?>
  <section class="utb--featured-story">
    <div class="container">
      <hr>
      <div class="utb--story-inner">
        <h1 class="utb--mod-title">
          <a href="<?php echo get_permalink(); ?>"><?php echo get_the_title(); ?></a>
        </h1>
        <div class="utb--compact">
          <span class="utb--small-intro">Story of Impact</span>
          <?php get_template_part('partials/posts/tpart-content'); ?>
          <a class="btn btn-size-small btn-outline btn-light dark-text" href="<?php echo get_permalink(); ?>">Read Story</a>
          <div class="utb--featured-story-action">
            <a class="alink gold purple-hover" href="<?php echo get_post_type_archive_link( ''. $post->post_type .'' ); ?>">Share your story</a>
          </div>
        </div>
      </div>
    </div>
  </section>
  <?php
  endwhile;
else:
  echo'<p>No stories were found.</p>';
endif;
wp_reset_postdata();
?>
