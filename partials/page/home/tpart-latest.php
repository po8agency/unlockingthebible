<section class="utb--posts utb--posts-latest utb--posts-balance section-pad">
  <div class="container fluid">
    <div class="grid">
    <?php
      //-----------------------------
      // FETCH LATEST ARTICLE/LIFEKEY
      //-----------------------------
      ?>
      <?php
      $posttypes = array('post','lifekey');
      $counter = count($posttypes);

      for($i=0; $i<$counter;$i++) {
      $post_args = array(
        'post_type' => $posttypes[$i],
        'post_status' => 'publish',
        'posts_per_page' => 1
      );
      $post_query = new WP_Query( $post_args );

      if ( $post_query->have_posts() ) :
        while ( $post_query->have_posts() ) : $post_query->the_post();

        echo '<div class="column c1-3">';

        $post_title    = get_the_title();
        $post_link     = get_permalink();
        $post_id     	 = get_the_ID();
        $post_type     = get_post_type( get_the_ID() );
        $topics        = get_the_terms( $post_id, 'topic' );
        $post_date     = get_the_date( 'l, F j, Y', $post_id );
        $author_link   = get_author_posts_url( get_the_author_meta( 'ID' ), get_the_author_meta( 'user_nicename' ) );

        $pod_rel 				= pods( $post_type, get_the_id() );
        $rel_series 		= $pod_rel->field( 'linked_series' );
        $rel_sermon 		= $pod_rel->field( 'linked_sermon' );
        $rel_product 		= $pod_rel->field( 'linked_product' );

        $linked_series 	= $rel_series['ID'];
        $linked_sermon 	= $rel_sermon['ID'];
        $linked_product = $rel_product['ID'];
        ?>
        <article class="utb--post utb--post-home">

          <div class="utb--all-url utb--all-top">
            <?php if($post_type == 'post') : ?>
            <a href="/articles">Article<i class="icon-arrow-right"></i></a>
            <?php elseif($post_type == 'lifekey') : ?>
            <a href="/lifekeys">Devotional<i class="icon-arrow-right"></i></a>
            <?php endif; ?>
          </div>

          <div class="utb--post-img-wrap">
            <a href="<?php echo $post_link; ?>">
              <div class="utb--post-img utb--img-16-9 utb--post-img-zoom loading">
              <?php
                  $post_image     = wp_get_attachment_image_src( $thumb_id , 'large' );

                  $thumb_id       = get_post_thumbnail_id();
                  $image_desktop  = wp_get_attachment_image_src( $thumb_id , 'large' );
                  $image_tablet   = wp_get_attachment_image_src( $thumb_id , 'medium' );
                  $image_mobile   = wp_get_attachment_image_src( $thumb_id , 'thumbnail' );

                  $img_width  = $image_desktop[1];
                  $img_height = $image_desktop[2];

                  if ( $width > $height ) {
                    $orientation = 'utb--portrait';
                  } else {
                    $orientation =  'utb--landscape';
                  }
                  if ($thumb_id) :
                  ?>
                  <img
                    src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw=="
                    data-src="<?php echo $image_desktop[0]; ?>"
                    data-srcset="<?php echo $image_mobile[0]; ?> 300w,
                    <?php echo $image_tablet[0]; ?> 600w,
                    <?php echo $image_desktop[0]; ?> 900w"
                    data-sizes="auto"
                    class="<?php echo $orientation; ?> lazyload" />
                  <?php
                  else :
                    echo '<img class="utb--landscape lazyload" data-src="' . get_stylesheet_directory_uri() . '/assets/img/default.jpg" />';
                  endif; ?>
              </div>
            </a>
          </div>
          <?php
          if ( $topics && ! is_wp_error( $topics ) ) :
          ?>
          <div class="utb--post-tags">
          <?php
            foreach ($topics as $topic) :
            $topic_link = get_term_link( $topic );
            ?>
            <a class="alink dark serious" href="<?php echo $topic_link; ?>"><?php echo $topic->name; ?></a>
            <?php endforeach; ?>
          </div>
          <?php endif; ?>

          <h1 class="utb--post-title">
            <a href="<?php echo $post_link; ?>"><?php echo $post_title; ?></a>
          </h1>

          <div class="utb--post-meta">
            <span class="utb--post-time"><?php echo $post_date; ?></span>
            <a class="alink dark serious" href="<?php echo $author_link; ?>"><span class="utb--post-author"><?php the_author(); ?></span></a>
          </div>

          <?php get_template_part('partials/posts/tpart-content'); ?>
          <a class="btn btn-size-small btn-outline btn-light dark-text" href="<?php echo $post_link; ?>">Read More</a>
        </article>
        <?php
        echo '</div>';
        endwhile;
      endif;
      wp_reset_query();
      }

      $product = get_field('featured_product');
      if( $product ):
        $post = $product;
  	    setup_postdata( $post );

        $post_title    = get_the_title();
        $post_link     = get_permalink();
        $post_id     	 = get_the_ID();
      ?>
      <div class="column c1-3 column-featured-product">
        <article class="utb--post utb--post-home">
          <div class="utb--all-url utb--all-top">
            <a href="/store">Featured Product<i class="icon-arrow-right"></i></a>
          </div>
          <div class="utb--post-img-wrap">
            <a href="<?php echo $post_link; ?>">
              <div class="utb--post-img utb--img-16-9 utb--post-img-zoom loading">
              <?php
                  $post_image     = wp_get_attachment_image_src( $thumb_id , 'large' );

                  $thumb_id       = get_post_thumbnail_id();
                  $image_desktop  = wp_get_attachment_image_src( $thumb_id , 'large' );
                  $image_tablet   = wp_get_attachment_image_src( $thumb_id , 'medium' );
                  $image_mobile   = wp_get_attachment_image_src( $thumb_id , 'thumbnail' );

                  $img_width  = $image_desktop[1];
                  $img_height = $image_desktop[2];

                  if ( $width > $height ) {
                    $orientation = 'utb--portrait';
                  } else {
                    $orientation =  'utb--landscape';
                  }
                  if ($thumb_id) :
                  ?>
                  <img
                    src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw=="
                    data-src="<?php echo $image_desktop[0]; ?>"
                    data-srcset="<?php echo $image_mobile[0]; ?> 300w,
                    <?php echo $image_tablet[0]; ?> 600w,
                    <?php echo $image_desktop[0]; ?> 900w"
                    data-sizes="auto"
                    class="<?php echo $orientation; ?> lazyload" />
                  <?php
                  else :
                    echo '<img class="utb--landscape lazyload" data-src="' . get_stylesheet_directory_uri() . '/assets/img/default.jpg" />';
                  endif; ?>
              </div>
            </a>
          </div>

          <h1 class="utb--post-title">
            <a href="<?php echo $post_link; ?>"><?php echo $post_title; ?></a>
          </h1>

          <?php get_template_part('partials/posts/tpart-content'); ?>
          <a class="btn btn-size-small btn-outline btn-light dark-text" href="<?php echo $post_link; ?>">Learn more</a>
        </article>
      </div>
      <?php
      endif;
      ?>
    </div>
  </div>
</section>
