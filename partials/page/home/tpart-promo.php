<?php
//*****
//Promotional Module Fields
//*****

//layout options
$pro_bg_color       = '#f2f2f0';
$pro_cont_width     = ' fluid';

//content fieldset
$pro_title       = get_field('home_promo_title');
$pro_subtitle    = get_field('home_promo_subtitle');
$pro_content     = get_field('home_promo_content');
$pro_action      = get_field('home_promo_action');
$pro_text        = $pro_action['button_text'];
$pro_url         = $pro_action['button_url'];
$pro_image       = get_field('home_promo_image');
$pro_size        = 'large';
?>
<section class="utb--mod utb--mod-promo" style="background-color: <?php echo $pro_bg_color; ?>;">
  <div class="container<?php echo $pro_cont_width; ?>;">
    <div class="utb--mod-wrap">
      <div class="grid align-bottom">
        <div class="column c1-2 col-pad utb--mod-content">
          <?php if($pro_title) : ?>
          <h1 class="utb--mod-title"><?php echo $pro_title; ?></h1>
          <?php
          endif;
          if($pro_subtitle) :
          ?>
          <h2 class="utb--mod-subtitle"><?php echo $pro_subtitle; ?></h2>
          <?php
          endif;
          if($pro_content) :
            echo '<p class="utb-mod-copy">' . $pro_content . '</p>';
          endif;
          if($pro_text) :
          ?>
          <div class="utb--mod-action">
            <a class="btn btn-outline btn-size-small dark-text btn-light" href="<?php echo $pro_url; ?>"><?php echo $pro_text; ?></a>
          </div>
          <?php endif; ?>
        </div>
        <div class="column c1-2 col-pad utb--mod-image">
        <?php if( $pro_image ) {echo wp_get_attachment_image( $pro_image, $pro_size, false, array('title' => ''.$pro_title.'', 'alt' => ''.$pro_title.'', 'class' => 'img-fluid align-bottom')); } ?>
        </div>
      </div>
    </div>
  </div>
</section>
