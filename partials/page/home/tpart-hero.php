<?php
//*****
//HERO area fields
//*****

//today's offer fieldset

//monthly offer fieldset
$offer_image    = get_field('offer_image', 'option');
$offer_bg       = get_field('offer_background_image', 'option');
$offer_title    = get_field('offer_title', 'option');
$offer_subtitle = get_field('offer_subtitle', 'option');
$offer_btn      = get_field('offer_button_text', 'option');
$offer_btn_url  = get_field('offer_url', 'option');
?>
<section class="utb--hero-area utb--hero-area-home">
  <div class="grid">
  <?php
  $args = array(
    'post_type' => 'broadcast',
    'post_status' => 'publish',
    'posts_per_page' => 1
  );
  $query = new WP_Query( $args );

  if ( $query->have_posts() ) :
    while ( $query->have_posts() ) : $query->the_post();

    $post_title     = get_the_title();
    $post_link      = get_permalink();
    $post_type      = $query->post_type[0];
    $thumb_id       = get_post_thumbnail_id();
    $post_cover     = get_field('cover_image');

    $linked_broadcast	= get_field('linked_broadcast');
    $linked_sermon	  = get_field('linked_sermon');

    $pod_rel 				= pods( 'broadcast', get_the_id() );
    $rel_series 		= $pod_rel->field( 'linked_series' );
    $rel_sermon 		= $pod_rel->field( 'linked_sermon' );
    $rel_product 		= $pod_rel->field( 'linked_product' );

    $linked_series 	= $rel_series['ID'];
    $linked_sermon 	= $rel_sermon['ID'];
    $linked_product = $rel_product['ID'];

    if ($post_cover) {
      $broadcast_bg = wp_get_attachment_image_src($post_cover['ID'], 'large')[0];
    } elseif($thumb_id) {
      $broadcast_bg = wp_get_attachment_image_src( $thumb_id , 'large' )[0];
    } else {
      $broadcast_bg = get_stylesheet_directory_uri() . '/assets/img/hero-placeholder.png';
    }
    ?>
    <div class="column c1-2 utb--hero-today">
      <div class="utb--hero-bg" style="background-image: url(<?php echo $broadcast_bg; ?>);"></div>
      <article class="utb--inner">
        <h3 class=""><i class="icon-microphone"></i>Today's Radio Program</h3>
        <h1 class=""><a href="<?php echo $post_link; ?>" title=""><?php echo $post_title; ?></a></h1>
        <a class="btn btn-solid btn-gold btn-size-small" href="<?php echo $post_link; ?>">Listen Now <i class="icon-volume-1"></i></a>
        <div class="utb--hero-extra">
          <?php if($linked_series): ?>
          <div class="series-link">
            <span>From the Series:</span>
            <h4><a class="alink gold light-hover" href="<?php echo get_permalink($linked_series); ?>"><em><?php echo get_the_title($linked_series); ?></em></a></h4>
          </div>
          <?php endif; ?>
          <a class="alink gold light-hover" href="/broadcasts"><em>See Recent Radio Programs</em></a>
        </div>
      </article>
    </div>
    <?php
    endwhile;
    endif;
    wp_reset_query();
    ?>
    <div class="column c1-2 utb--hero-monthly">
      <article class="utb--inner">
        <h3 class=""><i class="icon-present"></i>For your gift of any amount</h3>
        <?php if($offer_title) : ?>
        <h1 class="offer-title"><a href="<?php echo $offer_btn_url; ?>" title="<?php echo $offer_title; ?>"><?php echo $offer_title; ?></a></h1>
        <?php endif; ?>
        <?php if($offer_subtitle) : ?>
        <h2 class="offer-subtitle"><?php echo $offer_subtitle; ?></h2>
        <?php endif; ?>
        <div class="offer-action">
          <?php if($offer_image) : ?>
          <a href="<?php echo $offer_btn_url; ?>" title="<?php echo $offer_title; ?>">
            <?php echo wp_get_attachment_image( $offer_image, 'full' ); ?>
          </a>
          <?php endif; ?>
          <?php if($offer_btn_url && $offer_btn) : ?>
          <a class="btn btn-size-small btn-outline btn-light dark-text" href="<?php echo $offer_btn_url; ?>" title=""><?php echo $offer_btn; ?></a>
          <?php endif; ?>
        </div>

      </article>
    </div>
  </div>
</section>
