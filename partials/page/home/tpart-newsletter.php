<?php
//*****
//Newsletter module fields
//*****
$news_title       = get_field('home_news_title');
$news_content     = get_field('home_news_content');
$news_form_id     = get_field('home_form_id');

$offer_img_size = ' background-color: white; ';
$modpbot        = '';
$modptop        = '';

$overrides      = $modptop . ' ' . $modpbot . ' ' . $align;
?>
<section class="utb--mod utb--mod-subscribe" style="<?php echo $overrides; ?>">
  <div class="container">
    <div class="utb--mod-wrap">
      <h3 class="utb--mod-title"><?php echo $news_title; ?></h3>
      <p class="utb-mod-copy"><em><?php echo $news_content; ?></em></p>
      <div class="utb--news-form">
      <?php gravity_form( $news_form_id, $display_title = false, $display_description = false, $display_inactive = false, $field_values = null, $ajax = true, $tabindex, $echo = true ); ?>
      </div>
      <span class="utb--mob-info-snippet">We will never sell or misuse your information.</span>
    </div>
    <hr>
  </div>
</section>
