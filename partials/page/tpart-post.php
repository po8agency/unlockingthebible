<?php
$post_date    = get_the_date( 'l, F j, Y');
$post_title   = get_the_title();

$author_id    = $post->post_author;
$author_url   = get_author_posts_url(get_the_author_meta('ID'));
$post_type    = get_post_type( get_the_ID() );

if ($post_type == 'post') {
  $type = 'Article';
} elseif ($post_type == 'broadcast') {
  $type = 'Radio Program';
} elseif ($post_type == 'sermon') {
  $type = 'Sermon';
} elseif ($post_type == 'series') {
  $type = 'Series';
} elseif ($post_type == 'product') {
  $type = 'Products';
} elseif ($post_type == 'lifekey') {
  $type = 'Devotional';
} elseif ($post_type == 'story') {
  $type = 'Story';
}
?>
<article id="post-<?php the_ID(); ?>" class="utb--post">

  <div class="utb--post-body">
    <span class="utb--post-from"><?php echo $type; ?></span>
    <h1 class="utb--post-title"><a href="<?php the_permalink(); ?>"><?php echo $post_title; ?></a></h1>
    <div class="utb--post-content">
      <p><?php the_excerpt(); ?></p>
    </div>
    <div class="utb--post-meta">
      <div class="search-info">
        <span class="utb--post-time"><?php echo $post_date; ?></span>
        <?php if($post_type == 'post') : ?>
          <br />
        <a class="alink dark serious" href="<?php echo $author_url; ?>"><span class="utb--post-author"><?php the_author(); ?></span></a>
        <?php endif; ?>
      </div>
    </div>
  </div>

</article><!-- #post-## -->
