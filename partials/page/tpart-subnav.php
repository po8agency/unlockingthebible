<?php
//*****
// Subnavigation items
//*****
$subnav_visibility = get_field('subnav_visibility');
if ($subnav_visibility == true || is_archive() || is_singular() || is_home() && !is_front_page()) :

$teaching_id = get_id_by_slug('teaching');
$articles_id = get_id_by_slug('articles');
$features_id = get_id_by_slug('features');

$subnav_page_links = get_field('subnav_page_links');
$subnav_cust_links = get_field('subnav_custom_links');

$teaching_cust_links = get_field('subnav_custom_links', $teaching_id);
$articles_cust_links = get_field('subnav_custom_links', $articles_id);
$features_cust_links = get_field('subnav_custom_links', $features_id);

$previous = "javascript:history.go(-1)";
if(isset($_SERVER['HTTP_REFERER'])) {
  $previous = $_SERVER['HTTP_REFERER'];
}

?>
<div class="utb--subnav">
  <div class="container">
    <ul>
    <?php if(is_singular( array( 'broadcast', 'sermon', 'series' ) ) || is_post_type_archive( array('broadcast', 'sermon', 'series') ) ) : ?>
      <?php
      if($teaching_cust_links) {
        foreach($teaching_cust_links as $clink) {
          echo '<li><a href="'. $clink['link_url'] .'">'. $clink['link_title'] .'</a></li>';
        }
      } else {
      ?>
      <li<?php if(is_post_type_archive('broadcast') || is_singular('broadcast')) { echo ' class="current"'; } ?>><a href="/broadcasts">Radio Program</a></li>
      <li<?php if(is_post_type_archive('sermon') || is_singular('sermon')) { echo ' class="current"'; } ?>><a href="/sermons">Sermons</a></li>
      <li<?php if(is_page_template('templates/tpl-archive.php')) { echo ' class="current"'; } ?>><a href="/archive#type=broadcast,sermon">Archive</a></li>
    <?php } ?>
    <?php elseif(is_singular('post') || is_post_type_archive( 'post' ) || is_page( 'articles' ) || is_home() || is_page_template('templates/tpl-users.php') && !is_page('Our Staff')) : ?>
      <?php
      if($articles_cust_links) {
        foreach($articles_cust_links as $clink) {
          echo '<li><a href="'. $clink['link_url'] .'">'. $clink['link_title'] .'</a></li>';
        }
      } else {
      ?>
      <li<?php if(is_home() || is_singular('post')) { echo ' class="current"'; } ?>><a href="/articles">Articles Home</a></li>
      <li<?php if(is_page_template('templates/tpl-archive.php')) { echo ' class="current"'; } ?>><a href="/archive#type=post">Archive</a></li>
      <li<?php if(is_page_template('templates/tpl-users.php') || is_author()) { echo ' class="current"'; } ?>><a href="/authors">Authors</a></li>
      <?php } ?>
    <?php elseif(is_singular( array( 'lifekey', 'story' ) ) || is_post_type_archive( array('lifekey', 'story' ) ) || is_page( 'features' ) || is_page_template('templates/tpl-offer.php')  ||  is_page_template('templates/tpl-stations.php') ) : ?>
      <?php
      if($features_cust_links) {
        foreach($features_cust_links as $clink) {
          echo '<li><a href="'. $clink['link_url'] .'">'. $clink['link_title'] .'</a></li>';
        }
      } else {
      ?>
      <li<?php if(is_page( 'features' )) { echo ' class="current"'; } ?>><a href="/features">Features Home</a></li>
      <li<?php if(is_post_type_archive('lifekey') || is_singular('lifekey')) { echo ' class="current"'; } ?>><a href="/life-keys">Daily Devotional</a></li>
      <li<?php if(is_post_type_archive('story') || is_singular('story')) { echo ' class="current"'; } ?>><a href="/stories">Stories of Impact</a></li>
      <li<?php if(is_page_template('templates/tpl-stations.php')) { echo ' class="current"'; } ?>><a href="/station-finder">Station Finder</a></li>
      <?php } ?>
    <?php elseif(is_author()) : ?>
      <li><a href="<?= $previous ?>">Back</a></li>
    <?php elseif(is_page( 'About' ) || is_page( 'Mission & History' ) || is_page( 'Colin Smith' ) || is_page_template('templates/tpl-users.php') && !is_page( 'Our Authors' )) : ?>
      <li<?php if(is_page( 'About' )) { echo ' class="current"'; } ?>><a href="/about">About Home</a></li>
      <li<?php if(is_page( 'Mission & History' )) { echo ' class="current"'; } ?>><a href="/mission-and-history">Mission & History</a></li>
      <li<?php if(is_page( 'Colin Smith' )) { echo ' class="current"'; } ?>><a href="/colin-smith">Colin Smith</a></li>
      <li<?php if(is_page_template('templates/tpl-users.php')) { echo ' class="current"'; } ?>><a href="/staff">Our Staff</a></li>
    <?php elseif(is_tax( 'topic' )) : ?>
      <li><a href="<?= $previous ?>">Back</a></li>
      <li<?php if(is_tax( 'topic' )) { echo ' class="current"'; } ?>><a>Topic</a></li>
    <?php elseif(is_woocommerce() || is_checkout() || is_cart() || is_checkout() || is_account_page() || is_product()) : ?>
      <li<?php if(is_shop()) { echo ' class="current"'; } ?>><a href="<?php echo get_permalink( wc_get_page_id( 'shop' ) ); ?>">Store Home</a></li>
      <!-- <li><a href="https://www.10ofthose.com/partners/unlockingthebibleuk">UK Store</a></li> -->
    <?php
    elseif( !is_archive() && !is_page_template('templates/tpl-stations.php') && !is_home() && !is_single() && !is_author() ) :
      if($subnav_page_links) {
        foreach($subnav_page_links as $plink) {
          $pID = $plink;
          echo '<li><a href="'. get_permalink(''.$pID.'') .'">'.  get_the_title(''.$pID.'') .'</a></li>';
        }
      };
      if($subnav_cust_links) {
        foreach($subnav_cust_links as $clink) {
          echo '<li><a href="'. $clink['link_url'] .'">'. $clink['link_title'] .'</a></li>';
        }
      };
    else : ?>
      <li><a href="<?= $previous ?>">Back</a></li>
      <li class="current"><a href="<?php echo get_permalink(); ?>"><?php echo get_the_title(); ?></a></li>
    <?php
    endif;
    ?>
    </ul>
  </div>
</div>
<?php
endif; ?>
