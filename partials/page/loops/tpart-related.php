<?php
//-----------------------------
// EXPLORE TOPIC FURTHER LOOP
//-----------------------------
$queried_post   = get_queried_object();
$current_type		=	$queried_post->post_type;
$queried_id     = $queried_post->ID;
//get the post's topics
$args = array(
  'orderby' => 'name',
  'order' => 'DESC',
  'fields' => 'all'
);
$single_topics 	= wp_get_post_terms($queried_id, 'topic', $args);
//var_dump($single_topics);

if( $single_topics ) :
  echo '<hr>';
  // going to hold our topics_query params
  $topics_query = array();
  // add the relation parameter
  if( count( $single_topics > 1 ) )
    $topics_query['relation'] = 'OR' ;

  // loop through topics and build a topic query
  foreach( $single_topics as $single_topic ) {
    $topics_query[] = array(
      'taxonomy' => 'topic',
      'field' => 'slug',
      'terms' => $single_topic->slug,
    );
  }
endif;
?>
<div class="utb--topic-explorer">
  <div class="explorer-head">
    <h3>Explore this topic further</h3>
  </div>
  <div class="utb--posts grid col-pad">
  <?php
    $args = array(
      'post_type' => $current_type,
      'post_status' => 'publish',
      'posts_per_page' => 3,
      'tax_query' => $topics_query
    );
    $query = new WP_Query( $args );

    if ( $query->have_posts() ) :
      while ( $query->have_posts() ) : $query->the_post();

      $post_title    = get_the_title();
      $post_link     = get_permalink();
      $post_id     	 = get_the_ID();
      $post_date     = get_the_date( 'l, F j, Y', $post_id );
      $author_link   = get_author_posts_url( get_the_author_meta( 'ID' ), get_the_author_meta( 'user_nicename' ) );

      $linked_series	= get_field('linked_series');
      ?>
      <article class="utb--post column c1-3">

        <div class="utb--post-img-wrap">
          <a href="#">
            <div class="utb--post-img utb--img-16-9 utb--post-img-zoom loading" data-expand="-30">
            <?php
              $thumb_id       = get_post_thumbnail_id();
              $image_desktop  = wp_get_attachment_image_src( $thumb_id , 'large' );
              $image_tablet   = wp_get_attachment_image_src( $thumb_id , 'medium' );

              $img_width  = $image_desktop[1];
              $img_height = $image_desktop[2];

              if ( $width > $height ) {
                $orientation = 'utb--portrait';
              } else {
                $orientation =  'utb--landscape';
              }
              if ($thumb_id) :
              ?>
              <img
                src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw=="
                data-src="<?php echo $image_desktop[0]; ?>"
                data-srcset="<?php echo $image_tablet[0]; ?> 300w,
                <?php echo $image_tablet[0]; ?> 600w,
                <?php echo $image_desktop[0]; ?> 900w"
                data-sizes="auto"
                class="<?php echo $orientation; ?> lazyload" />
              <?php
              else :
                echo '<img class="utb--landscape lazyload" data-src="' . get_stylesheet_directory_uri() . '/assets/img/default.jpg" />';
              endif; ?>
            </div>
          </a>
        </div>

        <div class="utb--post-body">
        <?php
          $post_type     = get_post_type( get_the_ID() );
          $topics        = get_the_terms( $post_id, 'topic' );
          if ( $topics && ! is_wp_error( $topics ) ) :
          ?>
          <div class="utb--post-tags">
          <?php
            foreach ($topics as $topic) :
            $topic_link = get_term_link( $topic );
            ?>
            <a class="alink dark serious" href="<?php echo $topic_link; ?>"><?php echo $topic->name; ?></a>
            <?php endforeach; ?>
          </div>
          <?php endif; ?>
          <?php if($post_title): ?>
          <h1 class="utb--post-title">
            <a href="<?php echo $post_link; ?>"><?php echo $post_title; ?></a>
          </h1>
          <?php endif; ?>
          <?php if($linked_series): ?>
          <div class="utb--post-series">
            <span>From </span> <a class="alink gold dark-hover" href="<?php echo get_permalink($linked_series); ?>"><?php echo get_the_title($linked_series); ?></a>
          </div>
          <?php endif; ?>
          <div class="utb--post-content">
            <p><?php echo wp_trim_words( get_the_content(), 50, '...' ); ?></p>
          </div>
          <a class="btn btn-size-small btn-outline btn-light dark-text" href="<?php echo $post_link; ?>">Read Post</a>
        </div>
      </article>
      <?php
      endwhile;
    endif;
    wp_reset_query();
  ?>
  </div>
</div>
<?php //endif; ?>
