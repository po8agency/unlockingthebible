<div class="mobile-overlay">
	<div class="top-bar">
		<div class="container fluid">
		<?php
			$main_area		= get_field('main_area', 'option');
			$svg_logo 		= $main_area['vector_logo_light'];
			$png_logo 		= $main_area['png_logo_light'];

			$topbar				= get_field('top_bar', 'option');
			$site_tagline = $topbar['site_tagline'];
			$site_number 	= $topbar['contact_number'];

			if ($site_tagline) {echo '<span class="site-tagline">' . $site_tagline . '</span>';}
			if ($site_number) {
				echo '<div class="top-right">';
				echo '<span class="site-number">' . $site_number . '</span>';
			}
			if ($site_number) {echo '</div>';}
		?>
		</div>
	</div>
	<div class="overlay-head">
		<div class="container fluid">
			<div class="inner">
				<div class="logo" itemscope itemtype="https://schema.org/Organization">
					<a itemprop="url" href="<?php echo get_home_url(); ?>" title="">
						<img itemprop="logo" width="130" height="83" src="<?php echo $svg_logo; ?>" alt="Unlocking the Bible logo" onerror="this.src='<?php echo $png_logo; ?>'" />
					</a>
				</div>
			</div>
		</div>
	</div>
	<div class="overlay-body">
		<div class="container grid">
			<div class="column c2-3	overlay-content">
				<div class="overlay-row">
					<nav class="global-nav">
						<?php wp_nav_menu(array('menu' => 'mobile-top', 'container' => false, 'menu_class'	=> 'menu', 'depth' => 1)); ?>
					</nav>
					<select onchange="doGTranslate(this);" class="notranslate select-dress" id="gtranslate_selector">
						<option value="en|en" selected="selected">English</option>
						<option value="en|es">Spanish</option>
						<option value="en|zh-CN">Mandarin</option>
						<option value="en|zh-TW">Chinese</option>
						<option value="en|hi">Hindi</option>
						<option value="en|ko">Korean</option>
						<option value="en|ar">Arabic</option>
						<option value="en|ja">Japanese</option>
						<option value="en|hu">Hungarian</option>
						<option value="en|fr">French</option>
						<option value="en|it">Italian</option>
						<option value="en|pl">Polish</option>
						<option value="en|ru">Russian</option>
						<option value="en|id">Indonesian</option>
						<option value="en|nl">Dutch</option>
						<option value="en|tl">Tagalog (Filipino)</option>
						<option value="en|de">German</option>
					</select>
				</div>
				<div class="overlay-row">
					<a class="nav-link parent" href="<?php echo get_site_url(); ?>/donate/gift">FOR YOUR GIFT OF ANY AMOUNT</a>
				</div>

				<?php wp_nav_menu(array('menu' => 'full-menu', 'container' => false, 'menu_class'	=> 'full-menu full-menu-mobile', 'depth' => 2)); ?>

			</div>
			<div class="column c1-3	overlay-sidebar">
				<h3>Follow Unlocking the Bible</h3>
				<?php
				get_template_part('partials/global/tpart-social');

				$address_fields = get_field('address', 'option');
				$street 				= $address_fields['address_street'];
				$locality 			= $address_fields['address_locality'];
				$postal 				= $address_fields['address_code'];
				?>
				<div class="address" itemscope itemtype="http://schema.org/Organization">
  				<span itemprop="name">Unlocking the Bible</span>
					<div itemprop="address" itemscope itemtype="http://schema.org/PostalAddress">
					<?php
						if($street) {echo '<span itemprop="streetAddress">'.$street.'</span><br/>';}
						if($locality) {echo '<span itemprop="addressLocality">'.$locality.'</span>';}
						if($postal) {echo '<span itemprop="postalCode">'.$postal.'</span>';}
						if ($site_number) {
							echo '<span class="site-number-mobile"><a href="tel:' . $site_number . '">' . $site_number . '</a></span>';
						}
					?>
					</div>
				</div>
				<?php wp_nav_menu(array('menu' => 'footer-menu', 'container' => false, 'menu_class'	=> 'menu footer-menu global', 'depth' => 1)); ?>
			</div>
		</div>
	</div>
</div>
