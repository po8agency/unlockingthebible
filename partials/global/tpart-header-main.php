<header class="site-header">
	<div class="top-search" id="top-search">
		<div class="container">
			<form role="search" action="<?php echo home_url( '/' ); ?>" method="get">
		    <input type="search" name="s" id="search-normal" value="" placeholder="Search" />
				<input type="hidden" name="post_type[]" value="post" />
				<input type="hidden" name="post_type[]" value="broadcast" />
				<input type="hidden" name="post_type[]" value="sermon" />
				<input type="hidden" name="post_type[]" value="lifekey" />
			</form>
		</div>
	</div>
	<div class="top-bar">
		<div class="container fluid">
		<?php
			$topbar				= get_field('top_bar', 'option');
			$site_tagline = $topbar['site_tagline'];
			$site_number 	= $topbar['contact_number'];

			if ($site_tagline) {echo '<span class="site-tagline">' . $site_tagline . '</span>';}
			if ($site_number) {
				echo '<div class="top-right">';
				echo '<span class="site-number">' . $site_number . '</span>';
			}
			wp_nav_menu(array('menu' => 'top-menu', 'container' => false, 'menu_class'	=> 'menu', 'depth' => 1));
			if ($site_number) {echo '</div>';}
		?>
		</div>
	</div>
	<div class="header-main<?php if(is_front_page() == true) {echo ' is-home';} ?>">
	<?php
		$main_area		= get_field('main_area', 'option');
		$svg_logo 		= $main_area['vector_logo'];
		$png_logo 		= $main_area['png_logo'];
		?>
		<div class="container fluid">
			<div class="logo" itemscope itemtype="https://schema.org/Organization">
				<a itemprop="url" href="<?php echo get_home_url(); ?>" title="">
					<img itemprop="logo" width="150" height="96" src="<?php echo $svg_logo; ?>" alt="Unlocking the Bible logo" onerror="this.src='<?php echo $png_logo; ?>'" />
				</a>
			</div>
			<div class="main-right">
				<nav class="navigation" itemscope="itemscope" itemtype="http://schema.org/SiteNavigationElement">
					<?php wp_nav_menu(array('menu' => 'main-menu', 'container' => false, 'menu_class'	=> 'menu', 'depth' => 2)); ?>
				</nav>
				<select onchange="doGTranslate(this);" class="notranslate select-dress" id="gtranslate_selector">
					<option value="en|en" selected="selected">English</option>
					<option value="en|es">Spanish</option>
					<option value="en|zh-CN">Mandarin</option>
					<option value="en|zh-TW">Chinese</option>
					<option value="en|hi">Hindi</option>
					<option value="en|ko">Korean</option>
					<option value="en|ar">Arabic</option>
					<option value="en|ja">Japanese</option>
					<option value="en|hu">Hungarian</option>
					<option value="en|fr">French</option>
					<option value="en|it">Italian</option>
					<option value="en|pl">Polish</option>
					<option value="en|ru">Russian</option>
					<option value="en|id">Indonesian</option>
					<option value="en|nl">Dutch</option>
					<option value="en|tl">Tagalog (Filipino)</option>
					<option value="en|de">German</option>
				</select>

				<button class="search-toggle"><i class="icon-magnifier"></i></button>

				<button class="menu-toggle">
					<span></span>
					<span></span>
					<span></span>
					<span></span>
					<span></span>
					<span></span>
				</button>
			</div>
		</div>
	</div>
</header>
