<?php

// check if the repeater field has rows of data
if( have_rows('networks', 'option') ):
	echo '<div class="followers">';
 	// loop through the rows of data
    while ( have_rows('networks', 'option') ) : the_row();
		// display a sub field value
		$icon = get_sub_field('network_name');

		$name = basename(substr(strrchr(ucwords($icon), "-"), 1), "");
		$url 	= get_sub_field('network_link');
		?>
		<a href="<?php echo $url; ?>" title="<?php echo $name; ?>"><i class="icon-<?php echo $icon; ?>"></i></a>
		<?php
    endwhile;
	echo '</div>';
endif;
?>
