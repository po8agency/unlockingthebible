<?php if( '' != get_the_excerpt(get_the_ID()) || '' !== get_post(get_the_ID())->post_content ) : ?>
<div class="utb--post-content">
<?php
  if( '' != get_the_excerpt(get_the_ID()) ) {
    echo '<p>'. wp_trim_words( get_the_excerpt(get_the_ID()), 50, '...' ).'</p>';
  } else {
    echo '<p>'. wp_trim_words( get_the_content(get_the_ID()), 50, '...' ).'</p>';
  }
?>
</div>
<?php endif; ?>
