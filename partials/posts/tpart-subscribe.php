<?php
//*****
//Newsletter module fields
//*****

//layout options
$sub_options      = get_field('subscribe_global_options', 'option');
$sub_bg           = 'background-color: ' . $sub_options['background_color'] . ';';
$sub_title        = $sub_options['title'];
$sub_content      = $sub_options['content'];
$sub_form_id      = $sub_options['form_id'];
?>
<section class="utb--mod utb--mod-subscribe utb--mod-subscribe-archive" style="<?php echo $sub_bg; ?>">
  <div class="container">
    <div class="utb--mod-wrap">
      <h3 class="utb--mod-title"><?php echo $sub_title; ?></h3>
      <p class="utb-mod-copy"><em><?php echo $sub_content ; ?></em></p>
      <div class="utb--news-form">
      <?php gravity_form( $sub_form_id, $display_title = false, $display_description = false, $display_inactive = false, $field_values = null, $ajax = true, $tabindex, $echo = true ); ?>
      </div>
      <span class="utb--mob-info-snippet">We will never sell or misuse your information.</span>
    </div>
  </div>
</section>
