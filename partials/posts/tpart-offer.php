<?php
//*****
// This Month's offer fields
//*****

//layout options
$offer_image    = get_field('offer_image', 'option');
$offer_bg       = get_field('offer_background_image', 'option');
$offer_title    = get_field('offer_title', 'option');
$offer_subtitle = get_field('offer_subtitle', 'option');
$offer_btn      = get_field('offer_button_text', 'option');
$offer_btn_url  = get_field('offer_url', 'option');
?>
<section class="utb--monthly-offer" style="background-image: url(<?php echo $offer_bg; ?>);">
  <div class="container">
    <div class="offer-box">
      <div class="offer-box-title"><span>For your gift of any amount</span></div>
      <div class="offer-content">
        <?php if($offer_title) : ?>
        <h1 class="offer-title"><?php echo $offer_title; ?></h1>
        <?php endif; ?>
        <?php if($offer_subtitle) : ?>
        <h2 class="offer-subtitle"><?php echo $offer_subtitle; ?></h2>
        <?php endif; ?>
        <div class="offer-action">
          <?php if($offer_image) : ?>
          <a href="<?php echo $offer_btn_url; ?>" title="<?php echo $offer_title; ?>">
            <?php echo wp_get_attachment_image( $offer_image, 'full' ); ?>
          </a>
          <?php endif; ?>
          <?php if($offer_btn_url && $offer_btn) : ?>
          <a class="btn btn-outline btn-light btn-size-small" href="<?php echo $offer_btn_url; ?>" title=""><?php echo $offer_btn; ?></a>
          <?php endif; ?>
        </div>
      </div>
    </div>
  </div>
</section>
