<?php
//-----------------------------
// GLOBAL ARCHIVE PROMO TYPE
//-----------------------------

//layout options
$promo_options      = get_field('promo_global_options', 'option');
$mod_bg           = $promo_options['background_color'];
$mod_img_align    = $promo_options['image_align'];
$mod_colors       = $promo_options['color_scheme'];
$color_scheme     = 'mod-color-'.$mod_colors.'';
$mod_cont_align   = $promo_options['content_align'];
$mod_text_align   = $promo_options['text_align'];

//module content
$promo_image        = get_field('promo_global_image', 'option');
$img_size         = 'full';
$promo_content      = get_field('promo_global_content', 'option');
$mod_title        = $promo_content['title'];
$mod_subtitle     = $promo_content['subtitle'];
$mod_copy         = $promo_content['copy'];
$mod_btn_text     = $promo_content['button_text'];
$mod_btn_url      = $promo_content['button_url'];

if ($mod_img_align == 'imageright') {$img_align = ' mod-image-right';} else {$img_align = ' mod-image-left';}
if ($text_align == 'left') {$text_align = 'mod-text-left';} elseif($mod_text_align == 'center') {$text_align = 'mod-text-center';} else {$text_align = 'mod-text-right';}
?>
<section class="utb--mod utb--mod-promo utb--mod-promo-archive" style="background-color: <?php echo $mod_bg; ?>;">
  <div class="container">
    <div class="utb--mod-wrap">
      <div class="grid align-bottom">
        <div class="column c1-2 col-pad utb--mod-content mod-text-center mod-image-right mod-color-purple">
          <?php if($mod_title) : ?>
          <h1 class="utb--mod-title"><?php echo $mod_title; ?></h1>
          <?php
          endif;

          if($mod_cont_align == 'bot') {
            if($mod_copy) {echo '<p class="utb-mod-copy">' . $mod_copy . '</p>';}
            if($mod_subtitle) {echo '<h2 class="utb--mod-subtitle">' . $mod_subtitle . '</h2>';}
          } else {
            if($mod_subtitle) {echo '<h2 class="utb--mod-subtitle">' . $mod_subtitle . '</h2>';}
            if($mod_copy) {echo '<p class="utb-mod-copy">' . $mod_copy . '</p>';}
          }

          if($mod_btn_text) :
          ?>
          <div class="utb--mod-action">
            <a class="btn btn-outline btn-size-small dark-text btn-light" href="<?php echo $mod_btn_url; ?>" target="_blank"><?php echo $mod_btn_text; ?></a>
          </div>
          <?php endif; ?>
        </div>
        <div class="column c1-2 col-pad utb--mod-image">
        <?php if( $promo_image ) {echo wp_get_attachment_image( $promo_image, $img_size, false, array('title' => ''.$pro_title.'', 'alt' => ''.$pro_title.'', 'class' => 'img-fluid align-bottom')); } ?>
        </div>
      </div>
    </div>
  </div>
</section>
