<?php
//*****
// SINGLE CUSTOM POST TYPE HERO
//*****
$queried_post   = get_queried_object();
$queried_id     = get_queried_object_id();
$post_type      = $queried_post->post_type;
$hero_media      = get_field(''.$post_type.'_media_shortcode');
$hero_date       = get_field(''.$post_type.'_date');
$hero_download   = get_field(''.$post_type.'_download_url');
$hero_pod        = get_field(''.$post_type.'_podcast_url');
$hero_program    = get_field(''.$post_type.'_program_number');
$hero_pdf        = get_field(''.$post_type.'_transcript_pdf');
$hero_audio      = get_field(''.$post_type.'_audio_url');
$hero_dl_file    = get_field(''.$post_type.'_download_file');
$thumb_id       = get_post_thumbnail_id($queried_id);
$post_image     = wp_get_attachment_image_src( $thumb_id , 'cpt-cover' );
$post_cover     = get_field('cover_image');
$post_cover     = wp_get_attachment_image_src( $post_cover['ID'], 'cpt-cover' );
if($post_cover) {$post_cover = $post_cover[0];} elseif($thumb_id) {$post_cover = $post_image[0];} else { $post_cover = get_stylesheet_directory_uri() . '/assets/img/default.jpg';}
$pod_rel 				= pods( $post_type, get_the_id() );
$rel_broadcast 	= $pod_rel->field( 'linked_broadcast' );
$rel_sermon 		= $pod_rel->field( 'linked_sermon' );
$rel_product 		= $pod_rel->field( 'linked_product' );
$linked_broadcast = $rel_broadcast['ID'];
$linked_sermon 	  = $rel_sermon['ID'];
$linked_product   = $rel_product['ID'];
?>
<div class="utb--single-hero">

  <div class="utb--media-wrap">
  <?php
    if ($post_type === 'broadcast') :
    ?>
    <div class="media-type media-type-audio">
    <?php
      //*****
      // AUDIO MEDIA OUTPUT
      //*****
      if ($hero_download || $hero_audio) :
      if ($hero_audio) {
        $audio_file = $hero_audio;
      } else {
        $audio_file = $hero_download;
      }
      ?>
      <div class="media-holder">
        <div class="container">
          <div class="audio-title" itemprop="name"><p><?php echo the_title(); ?></p></div>
          <div class="audio-player grid align-middle">
            <div class="play-state" id="play-btn"><i class="icon-control-play"></i></div>
            <div class="audio-wrapper" id="player-container" href="javascript:;">
              <audio id="player" ontimeupdate="initProgressBar()">
        			  <source src="<?php echo $audio_file; ?>" type="audio/mp3">
        			</audio>
            </div>
            <div class="player-controls scrubber">
              <small class="start-time">00:00</small>
              <span id="seekObjContainer">
        			  <progress id="seekObj" value="0" max="1"></progress>
        			</span>
              <small class="end-time">00:00</small>
            </div>
            <div class="player-volume">
              <i class="icon-volume-1"></i>
              <input id="volume" name="volume" min="0" max="1" step="0.1" type="range" value="0.5" onchange="setVolume()">
              <i class="icon-volume-2"></i>
            </div>
          </div>

        </div>
      </div>
      <?php endif; //end hero_mp3 ?>
      <div class="media-options">
        <ul class="grid has-dropdowns">
          <?php if($linked_product) : ?>
          <li><a class="btn btn-outline btn-white btn-size-small" href="<?php echo get_permalink($linked_product); ?>">Purchase</a></li>
          <?php endif; ?>
          <?php if($linked_sermon) : ?>
          <li><a class="btn btn-outline btn-white btn-size-small" href="<?php echo get_permalink($linked_sermon); ?>">Sermon</a></li>
          <?php endif; ?>
          <?php if($hero_download || $hero_pdf) : ?>
          <li class="has-dropdown">
            <a class="btn btn-outline btn-white btn-size-small" href="#">Download</a>
            <div class="media-dropdown dropdown">
              <div id="counter"></div>
              <?php
              if($hero_dl_file) : ?>
              <a id="download-mp3" href="<?php echo $hero_dl_file; ?>" type="application/octet-stream" rel="download" download>Audio MP3</a>
              <?php endif; ?>
              <?php if($hero_pdf) : ?>
              <a href="<?php echo $hero_pdf; ?>" type="application/octet-stream" rel="download" download>Transcript PDF</a>
              <?php endif; ?>
            </div>
          </li>
          <?php endif; ?>
          <?php if ($hero_pod) : ?>
          <li><a class="btn btn-outline btn-white btn-size-small" href="<?php echo $hero_pod; ?>" target="_blank">Podcast</a></li>
          <?php endif; ?>
          <?php if ($hero_shortcode) : ?>
          <li><a class="btn btn-outline btn-white btn-size-small" href="#">Full Sermon</a></li>
          <?php endif; ?>
        </ul>
  		</div>
    </div>
    <div class="utb--hero-title-area" style="background-image: url(<?php echo $post_cover; ?>);">
      <h1 class="utb--hero-title" itemprop="name"><?php echo get_the_title(); ?></h1>
    </div>
    <?php
    elseif ($post_type == 'sermon') :
    ?>
    <div class="media-type media-type-video">
    <?php
      //*****
      // VIDEO/YOUTUBE MEDIA OUTPUT
      //*****
      if ($hero_media || $hero_audio) :
      ?>
      <div class="media-holder">
        <div class="container">
          <?php if(!empty($hero_audio) && !empty($hero_media) || !empty($hero_download) && !empty($hero_media)) : ?>
          <div class="hero--media-tabs">
            <div class="hero--media-tab active" data-content="2"><?php //Start Video Tab ?>
            <?php endif; ?>
              <?php if(!empty($hero_media)) : ?>
              <div class="video-container">
                <div class="video-aspect">
                <?php
                  //forward slashes are the start and end delimeters
                  //third parameter is the array we want to fill with matches
                  if (preg_match('/"([^"]+)"/', $hero_media, $m)) {
                    echo '<iframe width="1000" height="563" src="https://www.youtube.com/embed/'.$m[1].'" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen=""></iframe>';
                  }
                ?>
                </div>
              </div>
              <?php endif; ?>
            <?php if(!empty($hero_audio) && !empty($hero_media) || !empty($hero_download) && !empty($hero_media)) : ?>
            </div><?php //END Video Tab ?>
            <div class="hero--media-tab" data-content="1"><?php //Start Audio Tab ?>
            <?php endif; ?>
              <?php if(!empty($hero_audio)) : ?>
              <div class="audio-title" itemprop="name"><p><?php echo the_title(); ?></p></div>
              <div class="audio-player grid align-middle">
                <div class="play-state" id="play-btn"><i class="icon-control-play"></i></div>
                <div class="audio-wrapper" id="player-container" href="javascript:;">
                  <audio id="player" ontimeupdate="initProgressBar()">
            			  <source src="<?php echo $hero_audio; ?>" type="audio/mp3">
            			</audio>
                </div>
                <div class="player-controls scrubber">
                  <small class="start-time">00:00</small>
                  <span id="seekObjContainer">
            			  <progress id="seekObj" value="0" max="1"></progress>
            			</span>
                  <small class="end-time">00:00</small>
                </div>
                <div class="player-volume">
                  <i class="icon-volume-1"></i>
                  <input id="volume" name="volume" min="0" max="1" step="0.1" type="range" value="0.5" onchange="setVolume()">
                  <i class="icon-volume-2"></i>
                </div>
              </div>
              <?php endif; ?>
            <?php if(!empty($hero_audio) && !empty($hero_media) || !empty($hero_download) && !empty($hero_media)) : ?>
            </div><?php //END Audio Tab ?>
          </div>
          <?php endif; ?>
        </div>
      </div>
      <div class="media-options">
        <ul class="grid has-dropdowns">
          <?php if($hero_audio && $hero_media || $hero_download && $hero_media) : ?>
          <li><a class="btn btn-outline btn-white btn-size-small" href="#" data-tab="1">Audio</a></li>
          <li><a class="btn btn-outline btn-white btn-size-small active" href="#" data-tab="2">Video</a></li>
          <?php endif; ?>
          <?php if($linked_product) : ?>
          <li><a class="btn btn-outline btn-white btn-size-small" href="<?php echo get_permalink($linked_product); ?>">Purchase</a></li>
          <?php endif; ?>
          <li class="has-dropdown">
            <a class="btn btn-outline btn-white btn-size-small" href="#">Download</a>
            <div class="media-dropdown dropdown">
            <?php
              if($hero_download || $hero_dl_file) :
                if($hero_dl_file) {
                  $hero_download = $hero_dl_file;
                } else {
                  $hero_download = $hero_download;
                }
                echo '<a href="'.$hero_download.'" type="application/octet-stream" rel="download" download>Audio MP3</a>';
              endif;
            ?>
            <?php if($hero_pdf) : ?>
            <a href="<?php echo $hero_pdf; ?>" type="application/octet-stream" rel="download" download>Transcript PDF</a>
            <?php endif; ?>
            </div>
          </li>
          <?php if ($hero_pod) : ?>
          <li><a class="btn btn-outline btn-white btn-size-small" href="<?php echo $hero_pod; ?>" target="_blank">Podcast</a></li>
          <?php endif; ?>
        </ul>
  		</div>
    </div>
    <?php endif; //end hero_media ?>
  </div>
  <?php else : ?>
  <div class="utb--hero-title-area" style="background-image: url(<?php echo $post_cover; ?>);">
    <h1 class="utb--hero-title" itemprop="name"><?php echo get_the_title(); ?></h1>
  </div>
<?php endif; ?>
</div>
</div>
