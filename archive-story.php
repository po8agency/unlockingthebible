<?php
//*****
// Archive Template for Stories
//*****
get_header();
	get_template_part('partials/layout/tpart-start-page');
	//page subnavigation
	get_template_part('partials/page/tpart-subnav');

	$choices 	= get_field('picked_stories', 'option');
	if($choices) {
		$args = array(
			'post_type' => 'story',
			'posts_per_page' => 1,
			'post__in' => $choices,
			'orderby' => 'post__in'
		);
	} else {
		$args = array(
	    'post_type' => 'story',
	    'post_status' => 'publish',
	    'posts_per_page' => 1
	  );
	}
  $query = new WP_Query( $args );

  if ( $query->have_posts() ) :
    while ( $query->have_posts() ) : $query->the_post();
  	?>
    <section class="utb--mod utb--mod-featured">
      <div class="container">
        <div class="utb--page-title-area utb--align-center page-title-special">
          <h1 class="utb--page-title ">Stories of Impact</h1>
        </div>
        <div class="utb--posts utb--posts-latest grid">

          <article class="utb--post post--view-full">
          <?php
            $post_title    = get_the_title();
            $post_link     = get_permalink();
            $post_type     = $query->post_type[0];
            $post_date     = get_the_date( 'l, F j, Y', $post_id );
            $author_link   = get_author_posts_url( get_the_author_meta( 'ID' ), get_the_author_meta( 'user_nicename' ) );
            ?>
            <div class="utb--post-img-wrap">
              <a href="<?php echo $post_link; ?>">
                <div class="utb--post-img utb--img-16-9 utb--post-img-zoom loading">
                <?php
                    $thumb_id       = get_post_thumbnail_id();
                    $image_desktop  = wp_get_attachment_image_src( $thumb_id , 'large' );
                    $image_tablet   = wp_get_attachment_image_src( $thumb_id , 'medium' );
                    $image_mobile   = wp_get_attachment_image_src( $thumb_id , 'thumbnail' );

                    $img_width  = $image_desktop[1];
                    $img_height = $image_desktop[2];

                    if ( $width > $height ) {
                      $orientation = 'utb--portrait';
                    } else {
                      $orientation =  'utb--landscape';
                    }
                    if ($thumb_id) :
                    ?>
                    <img
                      src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw=="
                      data-src="<?php echo $image_desktop[0]; ?>"
                      data-srcset="<?php echo $image_mobile[0]; ?> 300w,
                      <?php echo $image_tablet[0]; ?> 600w,
                      <?php echo $image_desktop[0]; ?> 900w"
                  	  data-sizes="auto"
                  	  class="<?php echo $orientation; ?> lazyload" />
                    <?php
                    else :
                      echo '<img class="utb--landscape lazyload" data-src="' . get_stylesheet_directory_uri() . '/assets/img/default.jpg" />';
                    endif; ?>
                </div>
              </a>
            </div>
            <div class="utb--post-body">
              <?php if($post_title): ?>
              <h1 class="utb--post-title">
                <a href="<?php echo $post_link; ?>"><?php echo $post_title; ?></a>
              </h1>
              <?php endif; ?>
              <div class="utb--post-meta">
                <span class="utb--post-time"><?php echo $post_date; ?></span>
                <a class="alink dark serious" href="<?php echo $author_link; ?>"><span class="utb--post-author"><?php the_field('listeners_name'); ?></span></a>
              </div>
              <?php get_template_part('partials/posts/tpart-content'); ?>
              <a class="btn btn-size-small btn-outline btn-light dark-text" href="<?php echo $post_link; ?>">Read Story</a>
            </div>
          </article>

        </div>
      </div>
    </section>
    <?php
    endwhile;
    endif;
    wp_reset_query();

		$sub_content 				= get_field('submit_content', 'option');
		$sub_form 					= get_field('submit_form', 'option');

		$sub_content_title 	= $sub_content['submit_content_title'];
		$sub_content_edit 	= $sub_content['submit_content_editor'];

		$sub_form_title 		= $sub_form['submit_form_title'];
		$sub_form_id 				= $sub_form['submit_story_form'];
    ?>
		<section class="tell-story">
			<div class="container">
				<div class="grid">
					<div class="column c1-2">
						<?php if($sub_content_title) {echo '<h3 class="utb--mod-title">'.$sub_content_title.'</h3>';} ?>
						<?php if($sub_content_edit) {echo '<div>'.$sub_content_edit.'</div>';} ?>
					</div>
					<div class="column c1-2">
						<?php if($sub_form_title) {echo '<h3 class="utb--mod-title">'.$sub_form_title.'</h3>';} ?>
						<?php gravity_form( $sub_form_id, $display_title = false, $display_description = false, $display_inactive = false, $field_values = null, $ajax = true, $tabindex, $echo = true ); ?>
					</div>
				</div>
			</div>
		</section>
    <?php
    //-----------------------------
    // Recent Posts Loop
    //-----------------------------
    ?>
    <section class="utb--mod utb--mod-recent" style="<?php echo $mod_bg; ?>" id="article">
      <div class="container fluid">
				<?php if(empty($choices)) : ?>
        <div class="utb--mod-head utb--align-center">
          <h1 class="utb--mod-title">Recent Stories</h1>
        </div>
				<?php endif; ?>
        <div class="utb--posts grid col-pad">
        <?php
				if($choices) {
					$recent_args = array(
						'post_type' => 'story',
						'posts_per_page' => 4,
						'post__in' => $choices,
						'offset' => 1,
						'orderby' => 'post__in'
					);
				} else {
					$recent_args = array(
				    'post_type' => 'story',
				    'post_status' => 'publish',
				    'posts_per_page' => 4,
						'offset' => 1
				  );
				}
        $recent_query = new WP_Query( $recent_args );

        if ( $recent_query->have_posts() ) :
          while ( $recent_query->have_posts() ) : $recent_query->the_post();

          $post_title    = get_the_title();
          $post_link     = get_permalink();
          $post_type     = $query->post_type[0];
          $post_date     = get_the_date( 'l, F j, Y', $post_id );
          $author_link   = get_author_posts_url( get_the_author_meta( 'ID' ), get_the_author_meta( 'user_nicename' ) );

          $topics        = get_the_terms( $post_id, 'topic' );
          ?>
          <article class="utb--post column c1-4">

            <div class="utb--post-img-wrap">
              <a href="#">
                <div class="utb--post-img utb--img-16-9 utb--post-img-zoom loading" data-expand="-30">
                <?php
                  $thumb_id       = get_post_thumbnail_id();
                  $image_desktop  = wp_get_attachment_image_src( $thumb_id , 'large' );
                  $image_tablet   = wp_get_attachment_image_src( $thumb_id , 'medium' );

                  $img_width  = $image_desktop[1];
                  $img_height = $image_desktop[2];

                  if ( $width > $height ) {
                    $orientation = 'utb--portrait';
                  } else {
                    $orientation =  'utb--landscape';
                  }
                  if ($thumb_id) :
                  ?>
                  <img
                    src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw=="
                    data-src="<?php echo $image_desktop[0]; ?>"
                    data-srcset="<?php echo $image_tablet[0]; ?> 300w,
                    <?php echo $image_tablet[0]; ?> 600w,
                    <?php echo $image_desktop[0]; ?> 900w"
                	  data-sizes="auto"
                	  class="<?php echo $orientation; ?> lazyload" />
                  <?php
                  else :
                    echo '<img class="utb--landscape lazyload" data-src="' . get_stylesheet_directory_uri() . '/assets/img/default.jpg" />';
                  endif; ?>
                </div>
              </a>
            </div>

            <div class="utb--post-body">
            <?php
              if ( $topics && ! is_wp_error( $topics ) ) :
              ?>
              <div class="utb--post-tags">
              <?php
                foreach ($topics as $topic) :
                $topic_link = get_term_link( $topic );
                ?>
                <a class="alink dark serious" href="<?php echo $topic_link; ?>"><?php echo $topic->name; ?></a>
                <?php endforeach; ?>
              </div>
              <?php endif; ?>
              <?php if($post_title): ?>
              <h1 class="utb--post-title">
                <a href="<?php echo $post_link; ?>"><?php echo $post_title; ?></a>
              </h1>
              <?php endif; ?>
              <div class="utb--post-meta">
                <span class="utb--post-time"><?php echo $post_date; ?></span>
                <a class="alink dark serious" href="<?php echo $author_link; ?>"><span class="utb--post-author"><?php the_author(); ?></span></a>
              </div>
              <?php get_template_part('partials/posts/tpart-content'); ?>
              <a class="btn btn-size-small btn-outline btn-light dark-text" href="<?php echo $post_link; ?>">Read Story</a>
            </div>
          </article>
          <?php
          endwhile;
        endif;
        wp_reset_query();
        ?>
        </div>

        <div class="utb--mod-foot utb--align-center">
          <a class="alink gold purple-hover" href="/archive#type=story">Story Archive</a>
        </div>

      </div>
    </section>
    <?php
    //-----------------------------
    // TOPICS LOOP
    //-----------------------------
		$topic_terms = get_terms_by_post_type( 'topic', 'story' );
		if($topic_terms) {
		$total_count = count($topic_terms);
    ?>
    <section class="utb--mod utb--mod-topics">
      <div class="container">
        <div class="utb--mod-wrap">
          <div class="utb--page-title-area utb--align-center">
            <h1 class="utb--page-title">Topics</h1>
          </div>
          <div class="utb--topics">
          <?php
            //check for topics taxonomy
            echo '<ul class="utb--topic-terms grid ' . ($total_count < 4 ?' utb--align-center' :  '') . '">';
              if($total_count == 1) {
                $count_val = '1-1';
              } else if($total_count == 2) {
                $count_val = '1-2';
              } else if($total_count == 3) {
                $count_val = '1-3';
              } else if($total_count == 4) {
                $count_val = '1-4';
              } else if($total_count == 5) {
                $count_val = '1-5';
              } else {
                $count_val = '1-6';
              }
              foreach($topic_terms as $topic) :
                echo '<li class="utb--topic-term column c' . $count_val . '"><a href="'. get_term_link($topic->slug, 'topic') .'">' . $topic->name . '</a></li>';
              endforeach;
            echo '</ul>';
          ?>
          </div>
          <div class="utb--align-center topics-more">
            <a class="btn btn-size-small btn-outline btn-light dark-text" href="#">All Topics</a>
          </div>
        </div>
      </div>
    </section>
		<?php
		}
    //-----------------------------
    // GLOBAL ARCHIVE PROMO TYPE
    //-----------------------------
		get_template_part('partials/posts/tpart-promo');
    //-----------------------------
    // SUBSCRIBE FIELDS
    //-----------------------------
		get_template_part('partials/posts/tpart-subscribe');

	get_template_part('partials/layout/tpart-end-page');
get_footer();
