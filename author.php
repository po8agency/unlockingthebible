<?php
//*****
// Author Single Template
//*****
global $wp_query;
get_header();
	get_template_part('partials/layout/tpart-start-page');
	//page subnavigation
	get_template_part('partials/page/tpart-subnav');
	?>
	<section class="utb--author">
		<div class="container">
			<div class="utb--compact">
			<?php
				$queried_user   = get_queried_object();
				$user_id		    = $queried_user->ID;
				$user_data  		= get_userdata( $user_id );
				$user_meta  		= get_user_meta( $user_id );
				$user_email			= $user_data->user_email;
				$user_display		= $user_data->display_name;
				$user_desc			= $user_meta['description'][0];
				$user_first			= $user_meta['first_name'][0];
				$prof_google		= $user_meta['googleplus'][0];
				$prof_twitter		= $user_meta['twitter'][0];
				$prof_facebook	= $user_meta['facebook'][0];

				if ($user_first) {$user_name = $user_first;
				} else {$user_name = $user_display;}

				$user_gravatar 	= get_avatar( $user_id, 150 );
				$user_img  			= get_field('author_image', 'user_'.$user_id.'');
				$user_img_size	= 'product_thumb';

			 	?>
				<div class="author-content grid align-middle">
					<div class="author-column-image">
						<picture class="author-image">
						<?php
            if($user_img) :
							echo wp_get_attachment_image( $user_img, $user_img_size );
						elseif(validate_gravatar($user_id)) :
							echo $user_gravatar;
	          else :
              echo $user_gravatar;
            endif;
						?>
						</picture>
					</div>
					<div class="author-column-info">
						<div class="author-info">
							<h1 class="utb--page-title"><?php echo $user_display; ?></h1>
							<?php if($prof_google || $prof_facebook  || $prof_twitter) : ?>
							<ul class="followers">
								<?php if($prof_facebook){ ?><a href="<?php echo $prof_facebook; ?>" target="_blank" title="<?php echo $user_display; ?>'s Facebook Profile"><i class="icon-social-facebook"></i></a><?php } ?>
								<?php if($prof_twitter){ ?><a href="https://twitter.com/<?php echo $prof_twitter; ?>" target="_blank"  title="Follow <?php echo $user_display; ?> on Twitter"><i class="icon-social-twitter"></i></a><?php } ?>
								<?php if($prof_google){ ?><a href="<?php echo $prof_google; ?>" target="_blank"  title="<?php echo $user_display; ?>'s Google+ Profile"><i class="icon-social-google"></i></a><?php } ?>
							</ul>
							<?php
							endif;
						?>
						</div>
					</div>
				</div>
				<div class="utb--author-info">
				<?php
				if ( $user_desc || $user_email ) :
					echo '<div class="single-author"><p>';
					if($user_desc) {echo $user_desc;}
					echo '</p></div>';
				endif;
				?>
				</div>
				<?php
				if ( have_posts() ) : the_post();
				?>
				<div class="utb--user-posts">
					<hr>
					<div class="author-articles">
					<h3><?php echo $user_name; ?>'s Articles</h3>
					<?php
						rewind_posts();
						$args = array(
					    'author'        	=> $user_id,
					    'orderby'       	=> 'post_date',
							'post_type' 			=> 'post',
					    'post_status' 		=> 'publish',
					    'order'         	=>  'DESC',
					    'posts_per_page' 	=> 6
				    );

						$articles = new WP_Query( $args );

						if ( $articles->have_posts() ) :
							echo '<div class="utb--posts utb--results">';
							while ( $articles->have_posts() ) : $articles->the_post();

							$post_title    = get_the_title();
							$post_link     = get_permalink();
							$post_date     = get_the_date( 'l, F j, Y', $post_id );
							?>
							<article class="mix utb--post utb--result post article">
								<div class="utb--post-body">
									<h1 class="utb--post-title">
										<a href="<?php echo $post_link; ?>"><?php echo $post_title; ?></a>
									</h1>
									<div class="utb--post-meta">
										<span class="utb--post-time"><?php echo $post_date; ?></span>
									</div>
			          </div>
							</article>
							<?php
							endwhile;
							echo '</div>';
						endif;
					?>
					</div>
				</div>
				<?php
				endif;
			?>
			</div>
		</div>
	</section>
	<?php
	//-----------------------------
	// GLOBAL ARCHIVE PROMO TYPE
	//-----------------------------
	get_template_part('partials/posts/tpart-offer');
	//-----------------------------
	// SUBSCRIBE FIELDS
	//-----------------------------
	get_template_part('partials/posts/tpart-subscribe');

	get_template_part('partials/layout/tpart-end-page');
get_footer();
