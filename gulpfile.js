var styleSRC                = './assets/scss/style.scss'; // Path to main .scss file.
var styleDestination        = './assets/css/'; // Path to place the compiled CSS file.

// JS Vendor related.
var jsVendorSRC             = './assets/js/vendor/*.js'; // Path to JS vendor folder.
var jsVendorDestination     = './assets/js/'; // Path to place the compiled JS vendors file.
var jsVendorFile            = 'vendors'; // Compiled JS vendors file name.

// JS Custom related.
var jsCustomSRC             = './assets/js/main/*.js'; // Path to JS custom scripts folder.
var jsCustomDestination     = './assets/js/'; // Path to place the compiled JS custom scripts file.
var jsCustomFile            = 'main'; // Compiled JS custom file name.

// Images related.
var imagesSRC               = './assets/img/raw/**/*.{png,jpg,gif,svg}'; // Source folder of images which should be optimized.
var imagesDestination       = './assets/img/'; // Destination folder of optimized images. Must be different from the imagesSRC folder.

// Watch files paths.
var styleWatchFiles         = './assets/scss/**/*.scss'; // Path to all *.scss files inside css folder and inside them.
var vendorJSWatchFiles      = './assets/js/vendor/*.js'; // Path to all vendor JS files.
var customJSWatchFiles      = './assets/js/main/*.js'; // Path to all custom JS files.
var projectPHPWatchFiles    = './**/*.php'; // Path to all PHP files.

var remotePath              = '/public_html'; // Remote path to deploy SFTP changes

// Browsers you care about for autoprefixing.
const AUTOPREFIXER_BROWSERS = [
  'last 2 version',
  '> 1%',
  'ie >= 9',
  'ie_mob >= 10',
  'ff >= 30',
  'chrome >= 34',
  'safari >= 7',
  'opera >= 23',
  'ios >= 7',
  'android >= 4',
  'bb >= 10'
];

var fs          = require('fs');
var gulp        = require('gulp'); // Gulp of-course
//var runSequence = require('run-sequence');
//var vinylftp  = require('vinyl-ftp');
var sftp        = require('gulp-sftp');
var server      = require('./.server.json');
// add mysqldump as a dependency
var shell       = require('shelljs'),
    mysql       = require('mysql'),
    mysqldump   = require('mysqldump');

// CSS related plugins.
var sass         = require('gulp-sass'), // Gulp pluign for Sass compilation.
    minifycss    = require('gulp-uglifycss'), // Minifies CSS files.
    autoprefixer = require('gulp-autoprefixer'), // Autoprefixing magic.
    mmq          = require('gulp-merge-media-queries'); // Combine matching media queries into one media query definition.

// JS related plugins.
var concat       = require('gulp-concat'); // Concatenates JS files
var uglify       = require('gulp-uglify'); // Minifies JS files

// Image realted plugins.
var imagemin     = require('gulp-imagemin'); // Minify PNG, JPEG, GIF and SVG images with imagemin.

// Utility related plugins.
var rename       = require('gulp-rename'), // Renames files E.g. style.css -> style.min.css
    lineec       = require('gulp-line-ending-corrector'), // Consistent Line Endings for non UNIX systems. Gulp Plugin for Line Ending Corrector (A utility that makes sure your files have consistent line endings)
    filter       = require('gulp-filter'), // Enables you to work on a subset of the original files by filtering them using globbing.
    sourcemaps   = require('gulp-sourcemaps'), // Maps code in a compressed file (E.g. style.css) back to it’s original position in a source file (E.g. structure.scss, which was later combined with other css files to generate style.css)
    livereload   = require('gulp-livereload'),
    notify       = require('gulp-notify'); // Sends message notification to you

/**
 * Task: `styles`.
 *
 * Compiles Sass, Autoprefixes it and Minifies CSS.
 *
 * This task does the following:
 *    1. Gets the source scss file
 *    2. Compiles Sass to CSS
 *    3. Writes Sourcemaps for it
 *    4. Autoprefixes it and generates style.css
 *    5. Renames the CSS file with suffix .min.css
 *    6. Minifies the CSS file and generates style.min.css
 *    7. Injects CSS or reloads the browser via browserSync
 */
 gulp.task('styles', function () {
    gulp.src( styleSRC )
    .pipe( sourcemaps.init() )
    .pipe( sass( {
      errLogToConsole: true,
      outputStyle: 'compact',
      // outputStyle: 'compressed',
      // outputStyle: 'nested',
      // outputStyle: 'expanded',
      precision: 10
    } ) )
    .on('error', console.error.bind(console))
    .pipe( sourcemaps.write( { includeContent: false } ) )
    .pipe( sourcemaps.init( { loadMaps: true } ) )
    .pipe( autoprefixer( AUTOPREFIXER_BROWSERS ) )

    .pipe( sourcemaps.write ( './' ) )
    .pipe( lineec() ) // Consistent Line Endings for non UNIX systems.
    .pipe( gulp.dest( styleDestination ) )

    .pipe( filter( '**/*.css' ) ) // Filtering stream to only css files
    .pipe( mmq( { log: true } ) ) // Merge Media Queries only for .min.css version.

    .pipe( livereload() )

    .pipe( rename( { suffix: '.min' } ) )
    .pipe( minifycss( {
      maxLineLen: 10
    }))
    .pipe( lineec() ) // Consistent Line Endings for non UNIX systems.
    .pipe( gulp.dest( styleDestination ) )

    .pipe( filter( '**/*.css' ) ) // Filtering stream to only css files
    .pipe( livereload() ) // Reloads style.min.css if that is enqueued.
    .pipe( notify( { message: 'TASK: "styles" Completed! 💯', onLast: true } ) )
 });


 /**
  * Task: `vendorJS`.
  *
  * Concatenate and uglify vendor JS scripts.
  *
  * This task does the following:
  *     1. Gets the source folder for JS vendor files
  *     2. Concatenates all the files and generates vendors.js
  *     3. Renames the JS file with suffix .min.js
  *     4. Uglifes/Minifies the JS file and generates vendors.min.js
  */
 gulp.task( 'vendorjs', function() {
  gulp.src( jsVendorSRC )
    .pipe( concat( jsVendorFile + '.js' ) )
    .pipe( lineec() ) // Consistent Line Endings for non UNIX systems.
    .pipe( gulp.dest( jsVendorDestination ) )
    .pipe( rename( {
      basename: jsVendorFile,
      suffix: '.min'
    }))
    .pipe( uglify() )
    .pipe( lineec() ) // Consistent Line Endings for non UNIX systems.
    .pipe( gulp.dest( jsVendorDestination ) )
    .pipe( livereload() )
    .pipe( notify( { message: 'TASK: "vendorjs" Completed! 💯', onLast: true } ) );
 });


 /**
  * Task: `customJS`.
  *
  * Concatenate and uglify custom JS scripts.
  *
  * This task does the following:
  *     1. Gets the source folder for JS custom files
  *     2. Concatenates all the files and generates custom.js
  *     3. Renames the JS file with suffix .min.js
  *     4. Uglifes/Minifies the JS file and generates custom.min.js
  */
 gulp.task( 'basejs', function() {
    gulp.src( jsCustomSRC )
    .pipe( concat( jsCustomFile + '.js' ) )
    .pipe( lineec() ) // Consistent Line Endings for non UNIX systems.
    .pipe( gulp.dest( jsCustomDestination ) )
    .pipe( rename( {
      basename: jsCustomFile,
      suffix: '.min'
    }))
    .pipe( uglify() )
    .pipe( lineec() ) // Consistent Line Endings for non UNIX systems.
    .pipe( gulp.dest( jsCustomDestination ) )
    .pipe( livereload() )
    .pipe( notify( { message: 'TASK: "init" Completed! 💯', onLast: true } ) );
 });

 /**
  * Task: `images`.
  *
  * Minifies PNG, JPEG, GIF and SVG images.
  *
  * This task does the following:
  *     1. Gets the source of images raw folder
  *     2. Minifies PNG, JPEG, GIF and SVG images
  *     3. Generates and saves the optimized images
  *
  * This task will run only once, if you want to run it
  * again, do it with the command `gulp images`.
  */
 gulp.task( 'images', function() {
  gulp.src( imagesSRC )
    .pipe( imagemin( {
          progressive: true,
          optimizationLevel: 3, // 0-7 low-high
          interlaced: true,
          svgoPlugins: [{removeViewBox: false}]
        } ) )
    .pipe(gulp.dest( imagesDestination ))
    .pipe( notify( { message: 'TASK: "images" Completed! 💯', onLast: true } ) );
 });


 /**
  * Task: `deploy`.
  *
  * Deploys files to production using SFTP
  */
gulp.task('deploy', function () {
  return gulp.src(['./**', '!./node_modules/**', '!./sql/**'])
    .pipe(sftp({
      host: server.sftp.host,
      port: server.sftp.port,
      remotePath: server.sftp.path,
      user: server.sftp.user,
      pass: server.sftp.pass
    }))
    .pipe( notify( { message: 'TASK: "deploy" Completed! 💯', onLast: true } ) );
});

// var config = {
//   host: server.mysql.host,
//   port: server.mysql.port,
//   user: server.mysql.user,
//   password: server.mysql.pass,
//   database: server.mysql.database
// };

// Run the mysqldump
// gulp.task('dbdump', () => {
// 	var today = new Date(),
// 	dd = today.getDate(),
// 	mm = today.getMonth()+1 //January is 0!
// 	yyyy = today.getFullYear();
// 	if(dd<10) { dd = '0'+dd	}
// 	if(mm<10) { mm = '0'+mm }
// 	today = mm + '-' + dd + '-' + yyyy;
//
//   return new Promise((resolve, reject) => {
//     mysqldump({
//       connection: {
//         host: config.host,
//         port: config.port,
//         user: config.user,
//         password: config.password,
//         database: config.database,
//       },
//       dumpToFile: './sql/' + today + '.sql'
//     }, (err) => {
//       if (err !== null) return reject(err);
//     });
//   })
//   .catch((err) => {
//     console.log(err);
//   });
// });

 /**
  * Watch Tasks.
  *
  * Watches for file changes and runs specific tasks.
  */
 gulp.task( 'default', ['styles', 'vendorjs', 'basejs', 'images'], function () {
    livereload.listen()
    gulp.watch( projectPHPWatchFiles ); // Reload on PHP file changes.
    gulp.watch( styleWatchFiles, ['styles'] ); // Reload on SCSS file changes.
    gulp.watch( vendorJSWatchFiles, ['vendorjs'] ); // Reload on vendorsJs file changes.
    gulp.watch( customJSWatchFiles, ['basejs'] ); // Reload on init js file changes.
 });
