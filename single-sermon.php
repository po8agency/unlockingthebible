<?php
	get_header();
	get_template_part('partials/layout/tpart-start-page');
	//page subnavigation
	get_template_part('partials/page/tpart-subnav');
	get_template_part('partials/posts/tpart-hero');

	$queried_post   = get_queried_object();
	$current_type		=	$queried_post->post_type;
	$post_ID		    = $queried_post->ID;
	$topics 				= get_the_terms( $post_ID, 'topic' );
	$post_date     	= get_the_date( 'F j, Y', $post_id );
	$authorDesc 		= get_the_author_meta('user_description');
	$authorEmail		= get_the_author_meta('user_email');
	$author_name		= get_the_author_meta('display_name');
	$author					= get_the_author();
	$author_first 	= explode(' ',trim($author_name));
	if($author_name) {
		$author_name = $author_first;
	} else {
		$author_name = $author;
	}
	$cover_image		= get_field('cover_image');
	$author_link   	= get_author_posts_url( get_the_author_meta( 'ID' ), get_the_author_meta( 'user_nicename' ) );

	$pod_rel 				= pods( 'sermon', get_the_id() );
	$rel_series 		= $pod_rel->field( 'linked_series' );
	$rel_broadcast 	= $pod_rel->field( 'linked_broadcast' );
	$rel_product 		= $pod_rel->field( 'linked_product' );

	$linked_series 		= $rel_series['ID'];
	$linked_broadcast	= $rel_broadcast['ID'];
	$linked_product 	= $rel_product['ID'];

	$series   			= get_post( $linked_series );
	$series_title   = $series->post_title;
	$series_image   = wp_get_attachment_image_src( $thumb_id , 'thumbnail' );
	?>
  <section class="utb--single utb--single-broadcast">
    <div class="container">
      <div class="utb--single-post grid">
        <div class="utb--single-sidebar">
					<div class="utb-sticky">
						<?php if($linked_series) : ?>
						<div class="utb--sidebar-linked">
	          	<span class="series-head">From the Series:</span>
							<a href="<?php echo the_permalink($linked_series); ?>">
								<picture>
									<?php echo get_the_post_thumbnail( $linked_series, 'cpt-thumb'); ?>
								</picture>
								<span class="lnk-title"><?php echo $series_title; ?></span>
							</a>
	          </div>
						<?php endif; ?>
	          <div class="utb--sidebar-stamp">
	            <span class="stamp-date"><em><?php echo $post_date; ?></em></span>
	          </div>
	          <?php echo social_sharing_buttons(); ?>
					</div>
        </div>
        <div class="utb--single-body">
          <div class="single-content">
            <?php the_content(); ?>
          </div>
          <div class="single-meta">
            <div class="meta-topics">
						<?php
              if ( $topics && ! is_wp_error( $topics ) ) :
              ?>
              <div class="utb--post-tags">
								<span>Topics:</span>
              	<?php
                foreach ($topics as $topic) :
                $topic_link = get_term_link( $topic );
                ?>
                <a class="alink gold dark-hover" href="<?php echo $topic_link; ?>"><?php echo $topic->name; ?></a>
                <?php endforeach; ?>
              </div>
              <?php endif; ?>
            </div>
          </div>
          <hr>
        </div>
      </div>
			<?php get_template_part('partials/page/loops/tpart-related'); ?>
    </div>
  </section>
	<?php
	//*****
	// This month's offer
	//*****
	get_template_part('partials/posts/tpart-offer');

	//*****
	// Subscribe newsletter
	//*****
	get_template_part('partials/posts/tpart-subscribe');

	get_template_part('partials/layout/tpart-end-page');
get_footer();
