<?php
	get_header();
	get_template_part('partials/layout/tpart-start-page');
	//page subnavigation
	get_template_part('partials/page/tpart-subnav');
	get_template_part('partials/posts/tpart-hero');

	$queried_post   = get_queried_object();
	$post_ID		    = $queried_post->post_ID;
	$topics 				= get_the_terms( $post_ID, 'topic' );
	$post_date     	= get_the_date( 'F j, Y', $post_id );

	$listner_name   = get_field('listeners_name');
	$listner_img    = get_field('listners_image');
	?>
  <section class="utb--single utb--single-story">
    <div class="container">
      <div class="utb--single-post grid">
        <div class="utb--single-sidebar">
					<div class="utb-sticky">
	          <div class="utb--sidebar-author">
						<?php
							$image = get_field('image');
							if( !empty($listner_img) ): ?>
              <div class="author-image">
                <img src="<?php echo $listner_img['url']; ?>" alt="<?php echo $listner_img['alt']; ?>" />
              </div>
							<?php endif; ?>
							<?php if($listner_name) : ?>
              <span class="author-name"><?php echo $listner_name; ?></span>
							<?php endif; ?>
	   	      </div>
						<div class="utb--sidebar-stamp">
	            <span class="stamp-date"><em><?php echo $post_date; ?></em></span>
	          </div>
	          <?php echo social_sharing_buttons(); ?>
					</div>
        </div>
        <div class="utb--single-body">
          <div class="single-content">
            <?php the_content(); ?>
          </div>
					<?php if ( $topics && ! is_wp_error( $topics ) ) : ?>
          <div class="single-meta">
            <div class="meta-topics">
              <div class="utb--post-tags">
								<span>Topics:</span>
              	<?php
                foreach ($topics as $topic) :
                $topic_link = get_term_link( $topic );
                ?>
                <a class="alink gold dark-hover" href="<?php echo $topic_link; ?>"><?php echo $topic->name; ?></a>
                <?php endforeach; ?>
              </div>
            </div>
	       	</div>
					<?php endif; ?>
        </div>
      </div>
    </div>


  </section>
	<?php

	$sub_content 				= get_field('submit_content', 'option');
	$sub_form 					= get_field('submit_form', 'option');

	$sub_content_title 	= $sub_content['submit_content_title'];
	$sub_content_edit 	= $sub_content['submit_content_editor'];

	$sub_form_title 		= $sub_form['submit_form_title'];
	$sub_form_id 				= $sub_form['submit_story_form'];
	?>
	<section class="tell-story">
		<div class="container">
			<div class="grid">
				<div class="column c1-2">
					<?php if($sub_content_title) {echo '<h3 class="utb--mod-title">'.$sub_content_title.'</h3>';} ?>
					<?php if($sub_content_edit) {echo '<div>'.$sub_content_edit.'</div>';} ?>
				</div>
				<div class="column c1-2">
					<?php if($sub_form_title) {echo '<h3 class="utb--mod-title">'.$sub_form_title.'</h3>';} ?>
					<?php gravity_form( $sub_form_id, $display_title = false, $display_description = false, $display_inactive = false, $field_values = null, $ajax = true, $tabindex, $echo = true ); ?>
				</div>
			</div>
		</div>
	</section>
	<?php
	get_template_part('partials/layout/tpart-end-page');
get_footer();
