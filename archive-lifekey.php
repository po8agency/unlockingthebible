<?php
//*****
// Archive Template for Lifekeys
//*****
get_header();
	get_template_part('partials/layout/tpart-start-page');
	//page subnavigation
	get_template_part('partials/page/tpart-subnav');

  $args = array(
    'post_type' => 'lifekey',
    'post_status' => 'publish',
    'posts_per_page' => 1
  );
  $query = new WP_Query( $args );

  if ( $query->have_posts() ) :
    while ( $query->have_posts() ) : $query->the_post();
  	?>
    <section class="utb--mod utb--mod-featured featured-action-type">
      <div class="container">
        <div class="utb--page-title-area utb--align-center page-title-special">
          <h1 class="utb--page-title ">Daily Devotionals</h1>
        </div>
        <div class="utb--posts utb--posts-latest grid">

          <article class="utb--post post--view-full">
          <?php
            $post_title    = get_the_title();
            $post_link     = get_permalink();
            $post_type     = $query->post_type[0];
            $post_date     = get_the_date( 'l, F j, Y', $post_id );
            $author_link   = get_author_posts_url( get_the_author_meta( 'ID' ), get_the_author_meta( 'user_nicename' ) );
            ?>
            <div class="utb--post-img-wrap">
              <a href="<?php echo $post_link; ?>">
                <div class="utb--post-img utb--img-16-9 utb--post-img-zoom loading">
                <?php
                    $thumb_id       = get_post_thumbnail_id();
                    $image_desktop  = wp_get_attachment_image_src( $thumb_id , 'large' );
                    $image_tablet   = wp_get_attachment_image_src( $thumb_id , 'medium' );
                    $image_mobile   = wp_get_attachment_image_src( $thumb_id , 'thumbnail' );

                    $img_width  = $image_desktop[1];
                    $img_height = $image_desktop[2];

                    if ( $width > $height ) {
                      $orientation = 'utb--portrait';
                    } else {
                      $orientation =  'utb--landscape';
                    }
                    if ($thumb_id) :
                    ?>
                    <img
                      src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw=="
                      data-src="<?php echo $image_desktop[0]; ?>"
                      data-srcset="<?php echo $image_mobile[0]; ?> 300w,
                      <?php echo $image_tablet[0]; ?> 600w,
                      <?php echo $image_desktop[0]; ?> 900w"
                  	  data-sizes="auto"
                  	  class="<?php echo $orientation; ?> lazyload" />
                    <?php
                    else :
                      echo '<img class="utb--landscape lazyload" data-src="' . get_stylesheet_directory_uri() . '/assets/img/default.jpg" />';
                    endif; ?>
                </div>
              </a>
            </div>
            <div class="utb--post-body">
              <?php if($post_title): ?>
              <h1 class="utb--post-title">
                <a href="<?php echo $post_link; ?>"><?php echo $post_title; ?></a>
              </h1>
              <?php endif; ?>
              <div class="utb--post-meta">
                <span class="utb--post-time"><?php echo $post_date; ?></span>
                <a class="alink dark serious" href="<?php echo $author_link; ?>"><span class="utb--post-author"><?php the_author(); ?></span></a>
              </div>
							<?php get_template_part('partials/posts/tpart-content'); ?>
              <a class="btn btn-size-small btn-outline btn-light dark-text" href="<?php echo $post_link; ?>">Read Post</a>
            </div>
          </article>

					<!-- <div class="utb--archive-actions">
						<a class="btn btn-size-med btn-solid btn-gold" href="#subscribe">Subscribe to Lifekeys Daily</a>
						<a class="btn btn-size-med btn-solid btn-purple" href="#request">Request a free sample</a>
					</div> -->

        </div>
      </div>
    </section>
    <?php
    endwhile;
    endif;
    wp_reset_query();
    ?>
    <?php
    //-----------------------------
    // Recent Posts Loop
    //-----------------------------
    ?>
    <section class="utb--mod utb--mod-recent" style="<?php echo $mod_bg; ?>" id="article">
      <div class="container fluid">
        <div class="utb--mod-head utb--align-center">
          <h1 class="utb--mod-title">Recent Daily Devotionals</h1>
        </div>
        <div class="utb--posts grid col-pad">
        <?php
        $recent_args = array(
          'post_type' => 'lifekey',
          'post_status' => 'publish',
          'posts_per_page' => 4
        );
        $recent_query = new WP_Query( $recent_args );

        if ( $recent_query->have_posts() ) :
          while ( $recent_query->have_posts() ) : $recent_query->the_post();

          $post_title    = get_the_title();
          $post_link     = get_permalink();
          $post_type     = $query->post_type[0];
          $post_date     = get_the_date( 'l, F j, Y', $post_id );
          $author_link   = get_author_posts_url( get_the_author_meta( 'ID' ), get_the_author_meta( 'user_nicename' ) );

          $topics        = get_the_terms( $post_id, 'topic' );
          ?>
          <article class="utb--post column c1-4">

            <div class="utb--post-body">
            <?php
              if ( $topics && ! is_wp_error( $topics ) ) :
              ?>
              <div class="utb--post-tags">
              <?php
                foreach ($topics as $topic) :
                $topic_link = get_term_link( $topic );
                ?>
                <a class="alink dark serious" href="<?php echo $topic_link; ?>"><?php echo $topic->name; ?></a>
                <?php endforeach; ?>
              </div>
              <?php endif; ?>
              <?php if($post_title): ?>
              <h1 class="utb--post-title">
                <a href="<?php echo $post_link; ?>"><?php echo $post_title; ?></a>
              </h1>
              <?php endif; ?>
              <div class="utb--post-meta">
                <span class="utb--post-time"><?php echo $post_date; ?></span>
                <a class="alink dark serious" href="<?php echo $author_link; ?>"><span class="utb--post-author"><?php the_author(); ?></span></a>
              </div>
              <?php get_template_part('partials/posts/tpart-content'); ?>
              <a class="btn btn-size-small btn-outline btn-light dark-text" href="<?php echo $post_link; ?>">Read Post</a>
            </div>
          </article>
          <?php
          endwhile;
        endif;
        wp_reset_query();
        ?>
        </div>
        <div class="utb--mod-foot utb--align-center">
          <a class="alink gold purple-hover" href="/archive#type=lifekey">Devotional Archive</a>
        </div>
      </div>
    </section>
		<?php
    //-----------------------------
    // Subscribe Section
    //-----------------------------
    ?>
    <section class="utb--mod utb--section-lfsubscribe" id="subscribe">
      <div class="container utb--compact">
        <div class="utb--mod-head utb--align-center">
          <h1 class="utb--mod-title"><?php echo the_field('subscribe_section_title', 'option'); ?></h1>
					<div class="utb--head-content">
						<p><?php echo the_field('subscribe_section_content', 'option'); ?></p>
					</div>
        </div>
			</div>
			<div class="container">
				<div class="feature-boxes grid">
				<?php
				$rowCount = count(get_field('subscribe_feature_box', 'option'));
    		if( have_rows('subscribe_feature_box', 'option') ):
      		while ( have_rows('subscribe_feature_box', 'option') ) : the_row();
		      $box_title  	= get_sub_field('box_title');
		      $box_content  = get_sub_field('box_content');
					?>
					<div class="column c1-<?php echo $rowCount; ?>">
					<?php
						if($box_title) {
							echo '<h3>'.$box_title.'</h3>';
						}
						if($box_content) {
							echo '<p>'.$box_content.'</p>';
						}
					?>
					</div>
					<?php
					endwhile;
				endif;
				?>
				</div>
			</div>
		</section>
		<?php
    //-----------------------------
    // Request Section
    //-----------------------------
    ?>
    <section class="utb--mod utb--section-lfrequest" id="request">
		<?php
			$request 					= get_field('request_content', 'option');
			$request_title 		= $request['request_section_title'];
			$request_form_id 	= $request['request_gravity_form_id'];
			$request_image	 	= get_field('request_image', 'option');
		 	?>
      <div class="container utb--compact">
        <div class="utb--mod-head utb--align-center">
          <h1 class="utb--mod-title"><?php echo $request_title; ?></h1>
        </div>
			</div>
			<div class="utb--request-form">
				<div class="container grid">
					<?php if( $request_image ) : ?>
					<div class="request-image-column column c1-2">
						<?php echo wp_get_attachment_image( $request_image, 'large' ); ?>
					</div>
					<?php endif; ?>
					<div class="request-form-column<?php if( $request_image ) { echo ' column c1-2'; } ?>">
						<?php gravity_form( $request_form_id, $display_title = false, $display_description = false, $display_inactive = false, $field_values = null, $ajax = true, $tabindex, $echo = true ); ?>
					</div>
				</div>
			</div>
		</section>
		<?php
    //-----------------------------
    // GLOBAL ARCHIVE PROMO TYPE
    //-----------------------------
		get_template_part('partials/posts/tpart-promo');
    //-----------------------------
    // SUBSCRIBE FIELDS
    //-----------------------------
		get_template_part('partials/posts/tpart-subscribe');

	get_template_part('partials/layout/tpart-end-page');
get_footer();
