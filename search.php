<?php
/*
Template Name: Search Page
*/
global $query;
get_header();
get_template_part('partials/layout/tpart-start-page');
if(isset($_GET['post_type'])) :
$post_type = $_GET['post_type'];
  if ($post_type == 'product') :
    load_template(TEMPLATEPATH . '/templates/parts/search/product-search.php');
  elseif ($post_type == ('post' || 'sermon'|| 'broadcast')) :
    load_template(TEMPLATEPATH . '/templates/parts/search/normal-search.php');
  endif;
else :
    load_template(TEMPLATEPATH . '/templates/parts/search/search.php');
endif;
get_template_part('partials/layout/tpart-end-page');
get_footer();
