<?php
//* Template Name: Page Builder
get_header();
get_template_part('partials/layout/tpart-start-page');

if(is_checkout() || is_cart() || is_checkout() || is_account_page() || is_product()) :
	//page subnavigation
	get_template_part('partials/page/tpart-subnav');
  //store wrappers
  get_template_part('partials/layout/tpart-start-shop');
	if (have_posts()) : while (have_posts()) : the_post();
		if(is_account_page()) {
		echo '<div class="container">';
			echo '<div class="utb--my-account">';}
			get_template_part( 'templates/parts/store/tpart-content' );
			if(is_account_page()) {
			echo '</div>';
		echo '</div>';
		}
	endwhile; endif; // close the WordPress loop
else :
	// open the WordPress loop
	if (have_posts()) : while (have_posts()) : the_post();

		if(!basename(get_page_template()) === 'page.php') :
			// are there any rows within within our flexible content?
			if ( have_rows('page_sections') ):
				echo '<div class="agt--builder-content">';
				// loop through all the rows of flexible content
				while ( have_rows('page_sections') ) : the_row();

				// load the module from the modules folder
				get_template_part( 'partials/modules/'. get_row_layout() );

				endwhile; // close the loop of flexible content
				echo '</div>';
			else :
				echo 'This page has no sections. Edit the page and add some.';
			endif; // close flexible content conditional

			if ($sidebar_enabled == true) :
				echo '<div class="agt--builder-sidebar">';
				get_template_part( 'partials/page/sidebar.php' );
				echo '</div>';
			endif;
		else :
			get_template_part( 'partials/page/tpart-content' );
		endif;

	endwhile; endif; // close the WordPress loop
	//-----------------------------
	// GLOBAL ARCHIVE PROMO TYPE
	//-----------------------------
	get_template_part('partials/posts/tpart-promo');
	//-----------------------------
	// SUBSCRIBE FIELDS
	//-----------------------------
	get_template_part('partials/posts/tpart-subscribe');
endif;

get_template_part('partials/layout/tpart-end-page');
get_footer();
