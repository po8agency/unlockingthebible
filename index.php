<?php
//*****
// Archive Template for Articles
//*****
get_header();
	get_template_part('partials/layout/tpart-start-page');
	//page subnavigation
	get_template_part('partials/page/tpart-subnav');

  $args = array(
    'post_type' => 'post',
    'post_status' => 'publish',
    'posts_per_page' => 1
  );
  $query = new WP_Query( $args );

  if ( $query->have_posts() ) :
    while ( $query->have_posts() ) : $query->the_post();
  	?>
    <section class="utb--mod utb--mod-featured">
      <div class="container">
        <div class="utb--page-title-area utb--align-center page-title-special">
          <h1 class="utb--page-title ">Articles</h1>
        </div>
        <div class="utb--posts utb--posts-latest grid">

          <article class="utb--post post--view-full">
          <?php
            $post_title    = get_the_title();
            $post_link     = get_permalink();
            $post_type     = $query->post_type[0];
            $post_date     = get_the_date( 'l, F j, Y', $post_id );
            $author_link   = get_author_posts_url( get_the_author_meta( 'ID' ), get_the_author_meta( 'user_nicename' ) );
            ?>
            <div class="utb--post-img-wrap">
              <a href="<?php echo $post_link; ?>">
                <div class="utb--post-img utb--img-16-9 utb--post-img-zoom loading">
                <?php
                    $thumb_id       = get_post_thumbnail_id();
                    $image_desktop  = wp_get_attachment_image_src( $thumb_id , 'large' );
                    $image_tablet   = wp_get_attachment_image_src( $thumb_id , 'medium' );
                    $image_mobile   = wp_get_attachment_image_src( $thumb_id , 'thumbnail' );

                    $img_width  = $image_desktop[1];
                    $img_height = $image_desktop[2];

                    if ( $width > $height ) {
                      $orientation = 'utb--portrait';
                    } else {
                      $orientation =  'utb--landscape';
                    }
                    if ($thumb_id) :
                    ?>
                    <img
                      src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw=="
                      data-src="<?php echo $image_desktop[0]; ?>"
                      data-srcset="<?php echo $image_mobile[0]; ?> 300w,
                      <?php echo $image_tablet[0]; ?> 600w,
                      <?php echo $image_desktop[0]; ?> 900w"
                  	  data-sizes="auto"
                  	  class="<?php echo $orientation; ?> lazyload" />
                    <?php
                    else :
                      echo '<img class="utb--landscape lazyload" data-src="' . get_stylesheet_directory_uri() . '/assets/img/default.jpg" />';
                    endif; ?>
                </div>
              </a>
            </div>
            <div class="utb--post-body">
              <?php if($post_title): ?>
              <h1 class="utb--post-title">
                <a href="<?php echo $post_link; ?>"><?php echo $post_title; ?></a>
              </h1>
              <?php endif; ?>
              <div class="utb--post-meta">
                <span class="utb--post-time"><?php echo $post_date; ?></span>
                <a class="alink dark serious" href="<?php echo $author_link; ?>"><span class="utb--post-author"><?php the_author(); ?></span></a>
              </div>
              <?php get_template_part('partials/posts/tpart-content'); ?>
              <a class="btn btn-size-small btn-outline btn-light dark-text" href="<?php echo $post_link; ?>">Read Post</a>
            </div>
          </article>

        </div>
      </div>
    </section>
    <?php
    endwhile;
    endif;
    wp_reset_query();
    ?>
    <hr class="divider-small">
    <?php
    //-----------------------------
    // Recent Posts Loop
    //-----------------------------
    ?>
    <section class="utb--mod utb--mod-recent" style="<?php echo $mod_bg; ?>" id="article">
      <div class="container fluid">
        <div class="utb--mod-head utb--align-center">
          <h1 class="utb--mod-title">Recent Articles</h1>
        </div>
        <div class="utb--posts grid col-pad">
        <?php
        $recent_args = array(
          'post_type' => 'post',
          'post_status' => 'publish',
          'posts_per_page' => 8
        );
        $recent_query = new WP_Query( $recent_args );

        if ( $recent_query->have_posts() ) :
          while ( $recent_query->have_posts() ) : $recent_query->the_post();

          $post_title    = get_the_title();
          $post_link     = get_permalink();
          $post_type     = get_post_type( get_the_ID() );
          $post_date     = get_the_date( 'l, F j, Y', $post_id );
          $author_link   = get_author_posts_url( get_the_author_meta( 'ID' ), get_the_author_meta( 'user_nicename' ) );

          $topics        = get_the_terms( $post_id, 'topic' );
          ?>
          <article class="utb--post column c1-4">

            <div class="utb--post-img-wrap">
              <a href="<?php echo $post_link; ?>">
                <div class="utb--post-img utb--img-16-9 utb--post-img-zoom loading" data-expand="-30">
                <?php
                  $thumb_id       = get_post_thumbnail_id();
                  $image_desktop  = wp_get_attachment_image_src( $thumb_id , 'large' );
                  $image_tablet   = wp_get_attachment_image_src( $thumb_id , 'medium' );

                  $img_width  = $image_desktop[1];
                  $img_height = $image_desktop[2];

                  if ( $width > $height ) {
                    $orientation = 'utb--portrait';
                  } else {
                    $orientation =  'utb--landscape';
                  }
                  if ($thumb_id) :
                  ?>
                  <img
                    src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw=="
                    data-src="<?php echo $image_desktop[0]; ?>"
                    data-srcset="<?php echo $image_tablet[0]; ?> 300w,
                    <?php echo $image_tablet[0]; ?> 600w,
                    <?php echo $image_desktop[0]; ?> 900w"
                	  data-sizes="auto"
                	  class="<?php echo $orientation; ?> lazyload" />
                  <?php
                  else :
                    echo '<img class="utb--landscape lazyload" data-src="' . get_stylesheet_directory_uri() . '/assets/img/default.jpg" />';
                  endif; ?>
                </div>
              </a>
            </div>

            <div class="utb--post-body">
            <?php
              if ( $topics && ! is_wp_error( $topics ) ) :
              ?>
              <div class="utb--post-tags">
              <?php
                foreach ($topics as $topic) :
                $topic_link = get_term_link( $topic );
                ?>
                <a class="alink dark serious" href="<?php echo $topic_link; ?>"><?php echo $topic->name; ?></a>
                <?php endforeach; ?>
              </div>
              <?php endif; ?>
              <?php if($post_title): ?>
              <h1 class="utb--post-title">
                <a href="<?php echo $post_link; ?>"><?php echo $post_title; ?></a>
              </h1>
              <?php endif; ?>
              <div class="utb--post-meta">
                <span class="utb--post-time"><?php echo $post_date; ?></span>
                <a class="alink dark serious" href="<?php echo $author_link; ?>"><span class="utb--post-author"><?php the_author(); ?></span></a>
              </div>
              <?php get_template_part('partials/posts/tpart-content'); ?>
              <a class="btn btn-size-small btn-outline btn-light dark-text" href="<?php echo $post_link; ?>">Read Post</a>

              <?php if($asdf) : ?>
              <div class="utb--all-url utb--all-bot">
                <a class="alink gold dark-hover" href="<?php echo get_post_type_archive_link( $post_type ); ?>">More Radio Broadcasts</a>
              </div>
              <?php endif; ?>
            </div>
          </article>
          <?php
          endwhile;
        endif;
        wp_reset_query();
        ?>
        </div>
        <div class="utb--mod-foot utb--align-center">
          <a class="alink gold purple-hover" href="/archive#type=post">Article Archive</a>
        </div>
      </div>
    </section>
		<?php
    //-----------------------------
    // TOPICS LOOP
    //-----------------------------
		//$topic_terms = get_terms_by_post_type( 'topic', 'post' );

		$topic_terms = get_terms(
			array(
		    'taxonomy' => 'topic',
		    'hide_empty' => false,
			)
		);
		if($topic_terms) {
		$total_count = count($topic_terms);
    ?>
    <section class="utb--mod utb--mod-topics">
      <div class="container">
        <div class="utb--mod-wrap">
          <div class="utb--page-title-area utb--align-center">
            <h1 class="utb--page-title">Topics</h1>
          </div>
          <div class="utb--topics">
          <?php
	          echo '<ul class="utb--topic-terms grid ' . ($total_count < 4 ?' utb--align-center' :  '') . '">';
	            if($total_count == 1) {
	              $count_val = '1-1';
	            } else if($total_count == 2) {
	              $count_val = '1-2';
	            } else if($total_count == 3) {
	              $count_val = '1-3';
	            } else if($total_count == 4) {
	              $count_val = '1-4';
	            } else if($total_count == 5) {
	              $count_val = '1-5';
	            } else {
	              $count_val = '1-6';
	            }
	            foreach($topic_terms as $topic) :
	              echo '<li class="utb--topic-term column c' . $count_val . '"><a href="'. get_term_link($topic->slug, 'topic') .'">' . $topic->name . '</a></li>';
	            endforeach;
	          echo '</ul>';
          ?>
          </div>
					<div class="utb--align-center topics-more">
            <a class="btn btn-size-small btn-outline btn-light dark-text" href="#">All Topics</a>
          </div>
        </div>
      </div>
    </section>
		<?php
		}
    //-----------------------------
    // GLOBAL ARCHIVE PROMO TYPE
    //-----------------------------
		get_template_part('partials/posts/tpart-promo');
    //-----------------------------
    // SUBSCRIBE FIELDS
    //-----------------------------
		get_template_part('partials/posts/tpart-subscribe');

	get_template_part('partials/layout/tpart-end-page');
get_footer();
