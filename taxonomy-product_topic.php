<?php
//*****
// Product Topics Taxonomy template
//*****
get_header();
	get_template_part('partials/layout/tpart-start-page');
	//page subnavigation
	get_template_part('partials/page/tpart-subnav');
	//store wrappers
  get_template_part('partials/layout/tpart-start-shop');

	//-----------------------------
	// Get posts from current queried topic
	//-----------------------------
	$term = get_term_by( 'slug', get_query_var( 'term' ), get_query_var( 'taxonomy' ) );
	?>
	<section class="utb--store-tax">
		<div class="container">
			<aside class="taxonomy-sidebar">
				<h3>More in <?php echo $term->name; ?></h3>
			</aside>
			<div class="taxonomy-content grid">
			<?php
			$args = array(
				'post_type' => array('product'),
				'post_status' => 'publish',
				'nopaging' => true,
				'orderby' => 'date',
				'order'   => 'DESC',
				'posts_per_page' => -1,
				'tax_query' => array(
					'relation' => 'AND', array(
						'taxonomy' => 'product_topic',
						'field'    => 'ID',
						'terms'    => $term->term_id
					)
				),
			);
			$query = new WP_Query( $args );
			?>
			<?php if ($query->have_posts()) : ?>
				<?php while ($query->have_posts()) : $query->the_post();
				$post_title   = get_the_title();
				?>
				<article class="utb--product column c1-2">
					<div class="product-inner">
						<div class="product-body grid align-middle">
							<div class="product-image">
								<a href="<?php echo get_permalink( $product->ID ); ?>">
									<picture><?php	do_action( 'woocommerce_before_shop_loop_item_title' ); ?></picture>
								</a>
							</div>
							<div class="product-content">
								<a href="<?php echo get_permalink( $product->ID ); ?>">
									<?php do_action( 'woocommerce_shop_loop_item_title' ); ?>
									<span>More information &#187;</span>
								</a>
								<?php do_action( 'woocommerce_after_shop_loop_item' ); ?>
							</div>
						</div>
					</div>
				</article>
				<?php
				endwhile;
			endif;
			wp_reset_query();
			?>
			</div>
		</div>
	</section>
  <?php
	get_template_part('partials/layout/tpart-end-page');
get_footer();
