<?php
//*****
// Archive Template for Series
//*****
get_header();
	get_template_part('partials/layout/tpart-start-page');
	//page subnavigation
	get_template_part('partials/page/tpart-subnav');

	// TODO: Custom Archive Template for Series?
	// - [x] Setup base template
	// - [ ] Code out full template?

	//-----------------------------
	// GLOBAL ARCHIVE PROMO TYPE
	//-----------------------------
	get_template_part('partials/posts/tpart-promo');
	//-----------------------------
	// SUBSCRIBE FIELDS
	//-----------------------------
	get_template_part('partials/posts/tpart-subscribe');

	get_template_part('partials/layout/tpart-end-page');
get_footer();
