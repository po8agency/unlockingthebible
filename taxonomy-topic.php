<?php
//*****
// Topics Taxonomy template
//*****
get_header();
	get_template_part('partials/layout/tpart-start-page');
	//page subnavigation
	get_template_part('partials/page/tpart-subnav');

	//-----------------------------
	// Get posts from current queried topic
	//-----------------------------
	$term = get_term_by( 'slug', get_query_var( 'term' ), get_query_var( 'taxonomy' ) );
	?>
	<section class="utb--archive utb--archive-all">
		<div class="container utb--compact">
	    <div class="utb--page-title-area utb--align-center page-title-special">
	      <h1 class="utb--page-title ">Topic: <strong><?php echo $term->name; ?></strong></h1>
	    </div>
	    <div class="utb--wrap">
				<div class="utb--content">
					<div class="utb--posts utb--results" id="archive-results">
					<?php
					$args_archive = array(
						'post_type' => array('post', 'sermon', 'lifekey', 'broadcast', 'story'),
						'post_status' => 'publish',
						'nopaging' => true,
						'orderby' => 'date',
						'order'   => 'DESC',
						'posts_per_page' => -1,
						'tax_query' => array(
				    	'relation' => 'AND', array(
				        'taxonomy' => 'topic',
				        'field'    => 'ID',
				        'terms'    => $term->term_id
				      )
				    ),
					);
					$query_archive = new WP_Query( $args_archive );
					?>
					<?php if ($query_archive->have_posts()) : ?>
						<?php while ($query_archive->have_posts()) : $query_archive->the_post();

						$post_date    = get_the_date( 'l, F j, Y');
						$post_title   = get_the_title();

						$author_id    = $post->post_author;
						$author_url   = get_author_posts_url(get_the_author_meta('ID'));

						$type         = $post->post_type;
						$terms        = get_the_terms($post->ID,'topic');
						$topics       = array();
						if ($terms) :
							foreach ( $terms as $term ) :
								$topics[] = $term->slug;
							endforeach;
						endif;
						?>
						<article class="mix utb--post utb--result<?php echo ' ' . $type . ' ' . implode(' ', $topics); ?>" data-topic="<?php echo implode(' ', $topics); ?>">
							<div class="utb--post-body">
								<h1 class="utb--post-title"><a href="<?php echo get_permalink(); ?>"><?php echo $post_title; ?></a></h1>
								<div class="utb--post-meta">
									<span class="utb--post-time"><?php echo $post_date; ?></span>
								</div>
							</div>
						</article>
						<?php
						endwhile;
					endif;
					wp_reset_query();
					?>
					<div class="utb--result-404">
						<i class="icon-ghost"></i>
						<p>Sorry, we found nothing matching your search</p>
					</div>
					</div>
					<div class="controls-pagination">
						<div class="mixitup-page-list"></div>
					</div>
				</div>
			</div>
		</div>
	</section>
  <?php
	//-----------------------------
	// GLOBAL ARCHIVE PROMO TYPE
	//-----------------------------
	get_template_part('partials/posts/tpart-promo');
	//-----------------------------
	// SUBSCRIBE FIELDS
	//-----------------------------
	get_template_part('partials/posts/tpart-subscribe');

	get_template_part('partials/layout/tpart-end-page');
get_footer();
