</main>
<?php if(!is_woocommerce() && !is_checkout() && !is_cart()) : ?>
<div class="footer-portal">
	<div class="container fluid">
		<?php wp_nav_menu(array('menu' => 'full-menu', 'container' => false, 'menu_class'	=> 'full-menu full-menu-footer', 'depth' => 2)); ?>
	</div>
</div>
<?php endif; ?>
<footer class="site-footer notranslate" role="contentinfo" itemscope itemtype="http://schema.org/WPFooter">
	<meta itemprop="name" content="Webpage footer for <?php echo get_the_title(); ?>"/>
	<div class="container fluid">
		<div class="grid">
			<div class="column c1-3">
				<h5>Unlocking the Bible</h5>
				<div class="address" itemscope itemtype="http://schema.org/Organization">
					<div itemprop="address" itemscope itemtype="http://schema.org/PostalAddress">
					<?php
						$address_fields = get_field('address', 'option');
						$street 				= $address_fields['address_street'];
						$locality 			= $address_fields['address_locality'];
						$postal 				= $address_fields['address_code'];

						if($street) {echo '<span itemprop="streetAddress">'.$street.' </span>';}
						if($locality) {echo '<span itemprop="addressLocality">'.$locality.' </span>';}
						if($postal) {echo '<span itemprop="postalCode">'.$postal.'</span>';}
					?>
					</div>
				</div>
				<div class="contact-details">Call<span itemprop="telephone"> 866-UNLOCKED</span></div>
				<?php wp_nav_menu(array('menu' => 'footer-menu', 'container' => false, 'menu_class'	=> 'menu', 'depth' => 1)); ?>
			</div>
			<div class="column c1-3 footer-follow">
				<h5>Follow Unlocking the Bible</h5>
				<?php get_template_part('partials/global/tpart-social'); ?>
			</div>
			<div class="column c1-3">
				<div class="logo" itemscope itemtype="https://schema.org/Organization">
				<?php
					$main_area		= get_field('main_area', 'option');
					$svg_logo 		= $main_area['vector_logo_light'];
					$png_logo 		= $main_area['png_logo_light'];
					?>
					<img itemprop="logo" width="130" height="83" src="<?php echo $svg_logo; ?>" alt="Unlocking the Bible logo" onerror="this.src='<?php echo $png_logo; ?>'" />
				</div>
				<div class="copyright">© Unlocking the Bible 2018</div>
			</div>
		</div>
	</div>
</footer>
<?php get_template_part('partials/posts/tpart-subscribe-modal'); ?>
</div><!-- /.utb -->

<style type="text/css">
#goog-gt-tt {display:none !important;}
.goog-te-banner-frame {display:none !important;}
.goog-te-menu-value:hover {text-decoration:none !important;}
.goog-text-highlight {background-color:transparent !important;box-shadow:none !important;}
body {top:0 !important;}
#google_translate_element2 {display:none!important;}
</style>

<div id="google_translate_element2"></div>
<script type="text/javascript">
function googleTranslateElementInit2() {new google.translate.TranslateElement({pageLanguage: 'en',autoDisplay: false}, 'google_translate_element2');}
</script><script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit2"></script>

<script type="text/javascript">
function GTranslateGetCurrentLang() {var keyValue = document['cookie'].match('(^|;) ?googtrans=([^;]*)(;|$)');return keyValue ? keyValue[2].split('/')[2] : null;}
function GTranslateFireEvent(element,event){try{if(document.createEventObject){var evt=document.createEventObject();element.fireEvent('on'+event,evt)}else{var evt=document.createEvent('HTMLEvents');evt.initEvent(event,true,true);element.dispatchEvent(evt)}}catch(e){}}
function doGTranslate(lang_pair){if(lang_pair.value)lang_pair=lang_pair.value;if(lang_pair=='')return;var lang=lang_pair.split('|')[1];if(GTranslateGetCurrentLang() == null && lang == lang_pair.split('|')[0])return;var teCombo;var sel=document.getElementsByTagName('select');for(var i=0;i<sel.length;i++)if(/goog-te-combo/.test(sel[i].className)){teCombo=sel[i];break;}if(document.getElementById('google_translate_element2')==null||document.getElementById('google_translate_element2').innerHTML.length==0||teCombo.length==0||teCombo.innerHTML.length==0){setTimeout(function(){doGTranslate(lang_pair)},500)}else{teCombo.value=lang;GTranslateFireEvent(teCombo,'change');GTranslateFireEvent(teCombo,'change')}}
</script>

<?php wp_footer(); ?>

</body>
</html>
