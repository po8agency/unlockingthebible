<?php
get_header();
get_template_part('partials/layout/tpart-start-page');

  //Hero Area - Today's Broadcast / Monthly Offer
  get_template_part('partials/page/home/tpart-hero');
  //Newsletter Module
  get_template_part('partials/page/home/tpart-newsletter');
  //Latest Posts from Taxonomies
  get_template_part('partials/page/home/tpart-latest');
  //Random Reader story
  get_template_part('partials/page/home/tpart-story');
  //Promotional Module
  get_template_part('partials/page/home/tpart-promo');

get_template_part('partials/layout/tpart-end-page');
get_footer();
