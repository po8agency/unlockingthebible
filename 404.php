<?php
/**
 * The template for displaying 404 pages (Not Found)
 */
get_header();
	get_template_part('partials/layout/tpart-start-page');
	?>
	<section class="utb-error-404">
		<div class="container utb--compact">
			<div class="utb--page-title-area utb--align-center page-title-special">
        <h1 class="utb--page-title ">404</h1>
      </div>
			<img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/404.svg" width="256" height="256" />
			<h1 class="utb--page-title">Oops. The page you were looking for doesn't exist.</h3>
			<p>You may have mistyped the address or the page may have moved.</p>
			<a href="<?php echo get_site_url(); ?>/" class="btn btn-size-small btn-solid btn-gold">Take me back to the home page</a>
		</div>
	</section>
	<?php
  //-----------------------------
  // GLOBAL ARCHIVE PROMO TYPE
  //-----------------------------
  get_template_part('partials/posts/tpart-promo');
  //-----------------------------
  // SUBSCRIBE FIELDS
  //-----------------------------
  get_template_part('partials/posts/tpart-subscribe');
	get_template_part('partials/layout/tpart-end-page');
get_footer();
