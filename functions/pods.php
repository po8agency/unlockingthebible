<?php

/* ===============================================================
    PODS Functions
=============================================================== */

add_filter('pods_meta_default_box_title','linked_types_title_series',10,5);
function linked_types_title_series($title, $pod, $fields, $type, $name ) {
  $title = ($name=='series') ? __('Linked Broadcasts, Sermons and Products', 'plugin_lang') : $title ;
  return $title;
}

add_filter('pods_meta_default_box_title','linked_types_title_broadcast',10,5);
function linked_types_title_broadcast($title, $pod, $fields, $type, $name ) {
  $title = ($name=='broadcast') ? __('Linked Series, Sermon and Product', 'plugin_lang') : $title ;
  return $title;
}

add_filter('pods_meta_default_box_title','linked_types_title_sermon',10,5);
function linked_types_title_sermon($title, $pod, $fields, $type, $name ) {
  $title = ($name=='sermon') ? __('Linked Series, Broadcasts and Products', 'plugin_lang') : $title ;
  return $title;
}

add_filter('pods_meta_default_box_title','linked_types_title_product',10,5);
function linked_types_title_product($title, $pod, $fields, $type, $name ) {
  $title = ($name=='product') ? __('Linked Series, Broadcasts and Sermons', 'plugin_lang') : $title ;
  return $title;
}
