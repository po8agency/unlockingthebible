<?php
// Woocommerce Setup
function utb_add_woocommerce_support() {
	add_theme_support( 'woocommerce', array(
		'thumbnail_image_width' => 150,
		'single_image_width'    => 528,
    'product_grid'          => array(
        'default_rows'    => 2,
        'min_rows'        => 1,
        'max_rows'        => 2,
        'default_columns' => 2,
        'min_columns'     => 2,
        'max_columns'     => 4,
    ),
	) );
}
add_action( 'after_setup_theme', 'utb_add_woocommerce_support' );

// Remove Categories and Tags
function woo_remove_tax() {
  //unregister_taxonomy_for_object_type( 'product_tag', 'product' );
	//unregister_taxonomy_for_object_type( 'product_cat', 'product' );
}
add_action('init', 'woo_remove_tax');

/* Customize Product Categories Labels */
add_filter( 'woocommerce_taxonomy_args_product_cat', 'custom_wc_taxonomy_args_product_cat' );
function custom_wc_taxonomy_args_product_cat( $args ) {
	$args['label'] = __( 'Formats', 'woocommerce' );
	$args['labels'] = array(
	  'name' 				=> __( 'Format', 'woocommerce' ),
	  'singular_name' 	=> __( 'Format', 'woocommerce' ),
	  'menu_name'			=> _x( 'Formats', 'Admin menu name', 'woocommerce' ),
	  'search_items' 		=> __( 'Search Format', 'woocommerce' ),
	  'all_items' 		=> __( 'All Format', 'woocommerce' ),
	  'parent_item' 		=> __( 'Parent Format', 'woocommerce' ),
	  'parent_item_colon' => __( 'Parent Format:', 'woocommerce' ),
	  'edit_item' 		=> __( 'Edit Format', 'woocommerce' ),
	  'update_item' 		=> __( 'Update Format', 'woocommerce' ),
	  'add_new_item' 		=> __( 'Add New Format', 'woocommerce' ),
	  'new_item_name' 	=> __( 'New Format', 'woocommerce' )
	);

	return $args;
}

/**
 * Show cart contents / total Ajax
 */
add_filter( 'woocommerce_add_to_cart_fragments', 'woocommerce_header_add_to_cart_fragment' );

function woocommerce_header_add_to_cart_fragment( $fragments ) {
	global $woocommerce;

	ob_start();

	?>
	<a class="cart-customlocation" href="<?php echo esc_url(wc_get_cart_url()); ?>" title="<?php _e('View your shopping cart', 'woothemes'); ?>"><?php echo sprintf(_n('%d item', '%d items', $woocommerce->cart->cart_contents_count, 'woothemes'), $woocommerce->cart->cart_contents_count);?> - <?php echo $woocommerce->cart->get_cart_total(); ?></a>
	<?php
	$fragments['a.cart-customlocation'] = ob_get_clean();
	return $fragments;
}

function click_taxonomy_dropdown($taxonomy) { ?>
	<?php if($taxonomy) : ?>
	<ul>
	<?php
	$terms = get_terms(array( 'taxonomy' => $taxonomy, 'parent' => 0, 'category__in' => array ('exclude'=>1,'fields'=>'ids'), ));
	foreach ($terms as $term) {
		printf( '<li><a href="%s">%s</a></li>', get_term_link($term), $term->name );
	}
	?>
	</ul>
<?php endif; }


/**
 * Include the post term in the permalink of CPT posts
 * The tag %company% has been added in the Pod settings for the CPT
 */
add_filter( 'post_link', 'product_tax_permalinks', 10, 3 );
add_filter( 'post_type_link', 'product_tax_permalinks', 10, 3 );

function product_tax_permalinks( $permalink, $post_id, $leavename ) {
    // check if our custom tag is present
    if( strpos( $permalink, '%store%' ) === FALSE )
        return $permalink;

    // get post
    $post = get_post( $post_id );

    // if there isn't a post
    if( !$post )
        return $permalink;

    // fetch the terms
    $terms = wp_get_object_terms( $post->ID, 'product' );

    // make sure an object is returned and that no errors occured
    if( !is_wp_error( $terms ) && !empty( $terms ) && is_object( $terms[0] ) )
        $taxonomy_slug = $terms[0]->slug;
    else
        $taxonomy_slug = 'not-related';

    // return the new permalink with term inserted
    return str_replace( '%store%', $taxonomy_slug, $permalink );
}

// Edit WooCommerce dropdown menu item of shop page//
// Options: menu_order, popularity, rating, date, price, price-desc

remove_action( 'woocommerce_before_shop_loop_item_title', 'woocommerce_template_loop_product_thumbnail', 10);
add_action( 'woocommerce_before_shop_loop_item_title', 'woocommerce_template_loop_product_thumbnail', 10);

if ( ! function_exists( 'woocommerce_template_loop_product_thumbnail' ) ) {
    function woocommerce_template_loop_product_thumbnail() {
        echo woocommerce_get_product_thumbnail();
    }
}
if ( ! function_exists( 'woocommerce_get_product_thumbnail' ) ) {
    function woocommerce_get_product_thumbnail( $size = 'product_thumb', $placeholder_width = 0, $placeholder_height = 0  ) {
        global $post, $woocommerce, $product;
        $output = '<picture class="product-image loading">';
				$image_url 			= wp_get_attachment_image_src( get_post_thumbnail_id(), $size)[0];
				$attachment_ids = $product->get_gallery_attachment_ids();
				$gallery_url		= wp_get_attachment_image_src( $attachment_ids[0], $size)[0];
				$default_url		= get_stylesheet_directory_uri() . '/assets/img/default.jpg';
        if ( has_post_thumbnail() ) {
          $output .= '<img src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" data-src="'.$image_url.'" class="lazyload" />';
        } elseif($attachment_ids[0]) {
					$output .= '<img src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" data-src="'.$gallery_url.'" class="lazyload" />';
				} else {
					$output .= '<img src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" data-src="'.$default_url.'" class="lazyload" />';
				}
        $output .= '</picture>';
        return $output;
    }
}


add_filter ( 'woocommerce_account_menu_items', 'utb_remove_my_account_links' );
function utb_remove_my_account_links( $menu_links ){
	unset( $menu_links['dashboard'] ); // Dashboard
	//unset( $menu_links['payment-methods'] ); // Payment Methods
	//unset( $menu_links['orders'] ); // Orders
	//unset( $menu_links['downloads'] ); // Downloads
	//unset( $menu_links['edit-address'] ); // Addresses
	//unset( $menu_links['edit-account'] ); // Account details
	//unset( $menu_links['customer-logout'] ); // Logout

	return $menu_links;

}

/*
 * Change the order of the endpoints that appear in My Account Page - WooCommerce 2.6
 * The first item in the array is the custom endpoint URL - ie http://mydomain.com/my-account/my-custom-endpoint
 * Alongside it are the names of the list item Menu name that corresponds to the URL, change these to suit
 */
function wpb_woo_my_account_order() {
	$myorder = array(
		'orders'             => __( 'Recent Orders', 'woocommerce' ),
		'downloads'          => __( 'Downloads', 'woocommerce' ),
		'edit-address'       => __( 'Shipping', 'woocommerce' ),
		'payment-methods'    => __( 'Billing', 'woocommerce' ),
		'edit-account'       => __( 'Edit Account', 'woocommerce' ),
		'customer-logout'    => __( 'Logout', 'woocommerce' ),
	);
	return $myorder;
}
add_filter ( 'woocommerce_account_menu_items', 'wpb_woo_my_account_order' );

function utb_cart_count_fragments( $fragments ) {
  $fragments['span.utb-store-cart-count'] = '<span class="utb-store-cart-count">Cart (' . WC()->cart->get_cart_contents_count() . ')</span>';
  return $fragments;
}
add_filter( 'woocommerce_add_to_cart_fragments', 'utb_cart_count_fragments', 10, 1 );

function utb_cart_item_fragments( $fragments ) {
	ob_start();
	?>
	<div class="dropdown dropdown-special mini-cart-dropdown"><?php woocommerce_mini_cart(); ?></div>
	<?php $fragments['div.mini-cart-dropdown'] = ob_get_clean();
	return $fragments;
}
add_filter( 'woocommerce_add_to_cart_fragments', 'utb_cart_item_fragments');

remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_title', 5 );
remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_rating', 10 );
remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_price', 10 );
remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_excerpt', 20 );
remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_meta', 40 );

add_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_rating', 5 );
add_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_title', 10 );
add_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_price', 20 );
add_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_excerpt', 30 );
add_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_meta', 15 );

/**
 * Rename product data tabs
 */
add_filter( 'woocommerce_product_tabs', 'woo_rename_tabs', 98 );
function woo_rename_tabs( $tabs ) {

	$tabs['description']['title'] = __( 'Full Description' );		// Rename the description tab
	$tabs['additional_information']['title'] = __( 'Additional Information' );	// Rename the additional information tab
	$tabs['reviews']['title'] = __( 'Reviews' );				// Rename the reviews tab

	return $tabs;

}

/**
 * Reorder product data tabs
 */
add_filter( 'woocommerce_product_tabs', 'woo_reorder_tabs', 98 );
function woo_reorder_tabs( $tabs ) {

	$tabs['reviews']['priority'] = 15;			// Reviews first
	$tabs['description']['priority'] = 5;			// Description second
	$tabs['additional_information']['priority'] = 10;	// Additional information third

	return $tabs;
}

/*
* 	Remove Cross-Sells from the shopping cart page
*
*   Copy and paste this code into functions.php to
*   remove cross-sells from the cart in WooCommerce.
*
*   You may need to remove the opening <?php tag
*   before you add it to your functions file.
*/
remove_action( 'woocommerce_cart_collaterals', 'woocommerce_cross_sell_display');

add_filter( 'woocommerce_shipping_package_name', 'custom_shipping_package_name' );
function custom_shipping_package_name( $name ) {
  return 'Estimated Shipping';
}

//add_filter( 'woocommerce_checkout_fields' , 'custom_override_checkout_fields' );
//function custom_override_checkout_fields( $fields ) {
	//unset($fields['billing']['billing_company']);
 	//return $fields;
//}

function comment_support_for_product() {
  add_post_type_support( 'product', 'comments' );
}
add_action( 'init', 'comment_support_for_product' );


add_filter( 'woocommerce_product_subcategories_args', 'remove_uncategorized_category' );
/**
 * Remove uncategorized category from shop page.
 *
 * @param array $args Current arguments.
 * @return array
 **/
function remove_uncategorized_category( $args ) {
  $uncategorized = get_option( 'default_product_cat' );
  $args['exclude'] = $uncategorized;
  return $args;
}
