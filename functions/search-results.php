<?php
//*****
// Ajax Search Results
//*****

// add_action( 'wp_ajax_load_search_results', 'load_search_results' );
// add_action( 'wp_ajax_nopriv_load_search_results', 'load_search_results' );
//
// function load_search_results() {
//   $query = $_POST['query'];
//   $post_type = array('post', 'broadcast', 'sermon', 'lifekey');
//
//   $args = array(
//     'post_type' => $post_type,
//     'post_status' => 'publish',
//     's' => $query
//   );
//   $search = new WP_Query( $args );
//
//   ob_start();
//
//   if ( $search->have_posts() ) :
//
// 		while ( $search->have_posts() ) : $search->the_post();
//       get_template_part('partials/page/tpart-post');
// 		endwhile;
// 		//page nav function goes here ();
// 	else :
// 		get_template_part('partials/page/tpart-404');
// 	endif;
//
// 	$content = ob_get_clean();
//
// 	echo $content;
// 	die();
//
// }
//
// add_filter( 'pre_get_posts', 'utb_search' );
// function utb_search( $query ) {
//   if( is_admin() ) {
//     return $query;
//   }
//   if( isset($_GET['post_type']) && $_GET['post_type']){
//     $type = $_GET['post_type'];
//   }
//   if (!$post_type) {
//     $post_type = 'any';
//   }
//   if ($query->is_search) {
//     $query->set('post_type', $post_type);
//   };
//   return $query;
// }
