<?php
/* == make sure rss info is added to head ============================================ */
add_theme_support('automatic-feed-links');
/* == add WP 3.0 menu support ======================================================== */
add_theme_support('menus');
/* == add thumbnail and featured image support ======================================= */
add_theme_support('post-thumbnails');
/* == remove admin bar =============================================================== */
add_filter('show_admin_bar', '__return_false');
/* == Removes the default link on attachments  ======================================= */
update_option('image_default_link_type', 'none');
/* == Remove the version number of WP  =============================================== */
//remove_action('wp_head', 'wp_generator');

/* == remove funky formatting caused by tinymce advanced ============================= */
function fixtinymceadv($text){
    $text = str_replace('<p><br class="spacer_" /></p>','<br />',$text);
    return $text;
}
add_filter('the_content',  'fixtinymceadv');

//add_filter('wp_feed_cache_transient_lifetime', create_function('', 'return 60;'));

/* == Obscure login screen error messages ============================================ */
function login_obscure(){
    return '<strong>Sorry</strong>: Information that you have entered is incorrect.';
}
add_filter('login_errors', 'login_obscure');

/* == add wp-admin custom styling ==================================================== */
function admin_style() {
  wp_enqueue_style('admin-styles', get_template_directory_uri().'/admin/css/admin.css');
  wp_enqueue_style('line-icons', '//cdnjs.cloudflare.com/ajax/libs/simple-line-icons/2.4.1/css/simple-line-icons.css', false, null);
}
add_action('admin_enqueue_scripts', 'admin_style');

/* == Add blog name to title ========================================================= */
function donedid_alter_title($title, $sep) {
   global $page, $paged;

    // Add the blog name
    $title .= get_bloginfo( 'name', 'display' );

    // Add the blog description for the home/front page.
    $site_description = get_bloginfo( 'description', 'display' );
    if ( $site_description && ( is_home() || is_front_page() ) ) {
        $title .= " $sep $site_description";
    }

    // Add a page number if necessary:
    if ( ( $paged >= 2 || $page >= 2 ) && ! is_404() ) {
        $title .= " $sep " . sprintf( __( 'Page %s', '_s' ), max( $paged, $page ) );
    }
    return $title;
}
add_filter('wp_title', 'donedid_alter_title', 10, 2);

/* == Queue up all css & js files ==================================================== */
function donedid_scripts_styles() {
	if ( !is_admin() ) {
    wp_deregister_script('jquery');
    wp_register_script('jquery', ("//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"), false);
    wp_enqueue_script('jquery');
		wp_enqueue_script('ajaxValidate', '//cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.16.0/jquery.validate.min.js', array('jquery'), '1.16.0');
  }
  if ( !is_admin() ) {
  	wp_enqueue_script('utb_vendors', get_template_directory_uri() . '/assets/js/vendors.min.js',array('jquery'), null, true);
  	wp_enqueue_script('utb_main', get_template_directory_uri() . '/assets/js/main.min.js',array('jquery'), null, true);

    if ( is_search() || is_page_template( 'templates/tpl-stations.php' ) || is_page_template( 'templates/tpl-archive.php' ) || is_woocommerce() || is_product() ) {
      global $wp_query;
    	wp_enqueue_script( 'ajax-query', get_stylesheet_directory_uri() . '/assets/js/ajax.js', array( 'jquery' ), '1.0.0', true );
      wp_localize_script( 'ajax-query', 'ajaxquery', array('ajaxurl' => admin_url( 'admin-ajax.php' ),'query_vars' => json_encode( $wp_query->query )) );
    }

  	wp_enqueue_style('line-icons', '//cdnjs.cloudflare.com/ajax/libs/simple-line-icons/2.4.1/css/simple-line-icons.css', false, null);
  	wp_enqueue_style('utb_style', get_template_directory_uri() . '/assets/css/style.min.css', false, null);

    if (is_product()){
  		wp_enqueue_script('comment-reply');
  	}
  }
}
add_action('wp_enqueue_scripts', 'donedid_scripts_styles');

function fontawesome_dashboard() {
   wp_enqueue_style('fontawesome', '//netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.css', '', '4.0.3', 'all');
}
add_action('admin_init', 'fontawesome_dashboard');

function utb_custom_login() {
  echo '<link rel="stylesheet" type="text/css" href="' . get_bloginfo('stylesheet_directory') . '/admin/css/login.css" />';
}
add_action('login_head', 'utb_custom_login');

/* == adds iOS icons and favicon ===================================================== */
function donedid_header_icons() {
		$global_fields 	= get_field('global', 'option');
		$default_fav		= $global_fields['favicon_default'];
		$default_144		= $global_fields['favicon_apple_144'];
		$default_114		= $global_fields['favicon_apple_114'];
		$default_72			= $global_fields['favicon_apple_72'];
		$default_apple	= $global_fields['favicon_apple_default'];

    $output = '';
    $output .= '<link rel="apple-touch-icon" sizes="144x144" href="' . $default_144 . '" />' . "\n";
    $output .= '<link rel="apple-touch-icon" sizes="114x114" href="' . $default_114 . '" />' . "\n";
    $output .= '<link rel="apple-touch-icon" sizes="72x72" href="' . $default_72 . '" />' . "\n";
    $output .= '<link rel="apple-touch-icon" href="' . $default_apple . '" />' . "\n";
    $output .= '<link id="favicon" rel="shortcut icon" href="' . $default_fav . '" type="image/png">' . "\n";
    echo $output;
}
add_action('wp_head', 'donedid_header_icons');


//Adding the Open Graph in the Language Attributes
function add_opengraph_doctype( $output ) {
        return $output . ' xmlns:og="http://opengraphprotocol.org/schema/" xmlns:fb="http://www.facebook.com/2008/fbml"';
    }
add_filter('language_attributes', 'add_opengraph_doctype');

//Add Open Graph Meta Info from the actual article data, or customize as necessary
function facebook_open_graph() {
  global $post;
  if ( !is_singular()) //if it is not a post or a page
   return;
	if($excerpt = $post->post_excerpt) {
			$excerpt = strip_tags($post->post_excerpt);
		$excerpt = str_replace("", "'", $excerpt);
  } else {
    $excerpt = get_bloginfo('description');
	}
  // 100004048168000 - account ID
  //You'll need to find you Facebook profile Id and add it as the admin
  //echo '<meta property="fb:admins" content="100004048168000"/>';
  echo '<meta property="og:title" content="' . get_the_title() . '"/>';
	echo '<meta property="og:description" content="' . $excerpt . '"/>';
  echo '<meta property="og:type" content="article"/>';
  echo '<meta property="og:url" content="' . get_permalink() . '"/>';
  //Let's also add some Twitter related meta data
  echo '<meta name="twitter:card" content="summary" />';
  //This is the site Twitter @username to be used at the footer of the card
	echo '<meta name="twitter:site" content="@UnlckngtheBible" />';
	//This the Twitter @username which is the creator / author of the article

  // Customize the below with the name of your site
  echo '<meta property="og:site_name" content="Unlocking the Bible"/>';
  if(!has_post_thumbnail( $post->ID )) { //the post does not have featured image, use a default image
    //Create a default image on your server or an image in your media library, and insert it's URL here
    $default_image=''.get_stylesheet_directory_uri().'/assets/img/default.jpg';
    echo '<meta property="og:image" content="' . $default_image . '"/>';
  }
  else{
    $thumbnail_src = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'medium' );
    echo '<meta property="og:image" content="' . esc_attr( $thumbnail_src[0] ) . '"/>';
  }
  echo "";
}
add_action( 'wp_head', 'facebook_open_graph', 5 );


/* == adds wp menus =============================================================== */
function register_my_menus() {
  register_nav_menus(
    array(
			'top-menu' => __( 'Top Menu' ),
      'main-menu' => __( 'Main Menu' ),
			'mobile-top' => __( 'Mobile Top' ),
      'footer-menu' => __( 'Footer Menu' ),
      'full-menu' => __( 'Full Menu' ),
    )
  );
}
add_action( 'init', 'register_my_menus' );

/* == adds svg support =============================================================== */
function cc_mime_types($mimes) {
  $mimes['svg'] = 'image/svg+xml';
  return $mimes;
}
add_filter('upload_mimes', 'cc_mime_types');

/* == adds google fonts ============================================================= */
add_action( 'wp_enqueue_scripts', 'child_load_google_fonts' );
function child_load_google_fonts() {
  // Setup font arguments
	$query_args = array(
		'family' => 'Roboto:400,500,700' // Change this font to whatever font you'd like
	);
 	// A safe way to register a CSS style file for later use
	wp_register_style( 'google-fonts', add_query_arg( $query_args, "//fonts.googleapis.com/css" ), array(), null );
	// A safe way to add/enqueue a CSS style file to a WordPress generated page
	wp_enqueue_style( 'google-fonts' );
}

/* == custom default avatars ============================================================= */
function ryanbenhase_unregister_tags() {
	unregister_taxonomy_for_object_type( 'post_tag', 'post' );
	unregister_taxonomy_for_object_type( 'category', 'post' );
}
add_action( 'init', 'ryanbenhase_unregister_tags' );

function revcon_change_post_label() {
  global $menu;
  global $submenu;
  $menu[5][0] = 'Articles';
  $submenu['edit.php'][5][0] = 'Articles';
  $submenu['edit.php'][10][0] = 'Add Article';
}
function revcon_change_post_object() {
    global $wp_post_types;
    $labels = &$wp_post_types['post']->labels;
    $labels->name = 'Articles';
    $labels->singular_name = 'Articles';
    $labels->add_new = 'Add Article';
    $labels->add_new_item = 'Add Article';
    $labels->edit_item = 'Edit Articles';
    $labels->new_item = 'Articles';
    $labels->view_item = 'View Articles';
    $labels->search_items = 'Search Articles';
    $labels->not_found = 'No Articles found';
    $labels->not_found_in_trash = 'No Articles found in Trash';
    $labels->all_items = 'All Articles';
    $labels->menu_name = 'Articles';
    $labels->name_admin_bar = 'Articles';
}
add_action( 'admin_menu', 'revcon_change_post_label' );
add_action( 'init', 'revcon_change_post_object' );

function is_taxonomy_assigned_to_post_type( $post_type, $taxonomy = null ) {
	if ( is_object( $post_type ) )
		$post_type = $post_type->post_type;
	if ( empty( $post_type ) )
		return false;
	$taxonomies = get_object_taxonomies( $post_type );
	if ( empty( $taxonomy ) )
		$taxonomy = get_query_var( 'taxonomy' );
	return in_array( $taxonomy, $taxonomies );
}

/* Remove Tools admin menu item for everyone other than Administrator */
function hide_menu(){
 global $current_user;
 $user_id = get_current_user_id();
  // echo "user:".$user_id;   // Use this to find your user id quickly
  if($user_id != '56498'){
    remove_menu_page( 'edit.php?post_type=acf-field-group' );
    remove_menu_page( 'admin.php?page=pmxi-admin-import' );
    remove_menu_page( 'admin.php?page=catch-ids' );
    remove_menu_page( 'pods' );
    remove_menu_page( 'gutenberg' );
  }
}
add_action('admin_head', 'hide_menu');

/* == remove archive word from title ============================================================= */
function my_theme_archive_title( $title ) {
    if ( is_category() ) {
        $title = single_cat_title( '', false );
    } elseif ( is_tag() ) {
        $title = single_tag_title( '', false );
    } elseif ( is_author() ) {
        $title = '<span class="vcard">' . get_the_author() . '</span>';
    } elseif ( is_post_type_archive() ) {
        $title = post_type_archive_title( '', false );
    } elseif ( is_tax() ) {
        $title = single_term_title( '', false );
    }

    return $title;
}
add_filter( 'get_the_archive_title', 'my_theme_archive_title' );

/* == singular/plural function ============================================================= */
function plural( $amount, $singular = '', $plural = 's' ) {
  if ( $amount === 1 ) {
    return $singular;
  }
  return $plural;
}

/* == get terms by post type ============================================================= */
function get_terms_by_post_type( $taxonomy, $post_type ) {
  global $wpdb;
  $query = $wpdb->prepare(
    "SELECT t.*, COUNT(*) from $wpdb->terms AS t
    INNER JOIN $wpdb->term_taxonomy AS tt ON t.term_id = tt.term_id
    INNER JOIN $wpdb->term_relationships AS r ON r.term_taxonomy_id = tt.term_taxonomy_id
    INNER JOIN $wpdb->posts AS p ON p.ID = r.object_id
    WHERE p.post_type IN('%s') AND tt.taxonomy IN('%s')
    GROUP BY t.term_id LIMIT 24",
    $post_type,
    $taxonomy
  );
  $results = $wpdb->get_results( $query );
  return $results;
}

/* == post sharing ============================================================= */
function social_sharing_buttons() {
	global $post;
	if(is_singular() || is_home()){

		// Get current page URL
		$crunchifyURL = urlencode(get_permalink());

		// Get current page title
		$crunchifyTitle = str_replace( ' ', '%20', get_the_title());

		// Get Post Thumbnail for pinterest
		$crunchifyThumbnail = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'full' );

		// Construct sharing URL without using any script
    $title = urlencode( get_the_title() );
    $twitter_via = "UnlckngtheBible";

    $twitter_params = 'text=' . $title . '&amp;url=' . $crunchifyURL . '&amp;via=' . $twitter_via;

		$facebookURL  = 'https://www.facebook.com/sharer/sharer.php?u='.$crunchifyURL;
		$googleURL    = 'https://plus.google.com/share?url='.$crunchifyURL;
		$whatsappURL  = 'whatsapp://send?text='.$crunchifyTitle . ' ' . $crunchifyURL;
		$linkedInURL  = 'https://www.linkedin.com/shareArticle?mini=true&url='.$crunchifyURL.'&amp;title='.$crunchifyTitle;
    $emailURL     = 'mailto:?subject='.$crunchifyTitle.'&body='.$crunchifyURL;
    $printURL     = 'mailto:?subject='.$crunchifyTitle.'&body='.$crunchifyURL;
		// Based on popular demand added Pinterest too
		$pinterestURL = 'https:pinterest.com/pin/create/button/?url='.$crunchifyURL.'&amp;media='.$crunchifyThumbnail[0].'&amp;description='.$crunchifyTitle;

		// Add sharing button at the end of page/page content
		$content .= '<ul class="utb--share">';
		$content .= '<li><a href="'.$facebookURL.'" target="_blank" title="Share this on Facebook"><i class="icon-social-facebook"></i></a></li>';
    $content .= '<li><a href="https://twitter.com/intent/tweet?' . $twitter_params . '" target="_blank" title="Share this on Twitter"><i class="icon-social-twitter"></i></a></li>';
    //$content .= '<li><a href="'.$googleURL.'" target="_blank" title="Share on Instagram"><i class="icon-social-google"></i></a></li>';
    $content .= '<li><a href="'.$emailURL.'" target="_blank" title="Share on Email"><i class="icon-envelope"></i></a></li>';
    $content .= '<li><a class="print-doc" href="#" target="_blank" title="Print this Article"><i class="icon-printer"></i></a></li>';
		$content .= '</ul>';

		return $content;
	}else{
		// if not a post/page then don't include sharing button
		return $content;
	}
}

/* == check for gravatar image ============================================================= */
function validate_gravatar($id_or_email) {
  //id or email code borrowed from wp-includes/pluggable.php
    $email = '';
    if ( is_numeric($id_or_email) ) {
        $id = (int) $id_or_email;
        $user = get_userdata($id);
        if ( $user )
            $email = $user->user_email;
    } elseif ( is_object($id_or_email) ) {
        // No avatar for pingbacks or trackbacks
        $allowed_comment_types = apply_filters( 'get_avatar_comment_types', array( 'comment' ) );
        if ( ! empty( $id_or_email->comment_type ) && ! in_array( $id_or_email->comment_type, (array) $allowed_comment_types ) )
            return false;

        if ( !empty($id_or_email->user_id) ) {
            $id = (int) $id_or_email->user_id;
            $user = get_userdata($id);
            if ( $user)
                $email = $user->user_email;
        } elseif ( !empty($id_or_email->comment_author_email) ) {
            $email = $id_or_email->comment_author_email;
        }
    } else {
        $email = $id_or_email;
    }

    $hashkey = md5(strtolower(trim($email)));
    $uri = 'http://www.gravatar.com/avatar/' . $hashkey . '?d=404';

    $data = wp_cache_get($hashkey);
    if (false === $data) {
        $response = wp_remote_head($uri);
        if( is_wp_error($response) ) {
            $data = 'not200';
        } else {
            $data = $response['response']['code'];
        }
        wp_cache_set($hashkey, $data, $group = '', $expire = 60*5);

    }
    if ($data == '200'){
        return true;
    } else {
        return false;
    }
}

/* == check for gravatar image ============================================================= */
add_filter( 'gform_ajax_spinner_url', 'spinner_url', 10, 2 );
function spinner_url( $image_src, $form ) {
  return ''.get_stylesheet_directory_uri().'/assets/img/loader.svg';
}

/* == custom thumb size ============================================================= */
add_action( 'after_setup_theme', 'wpdocs_theme_setup' );
function wpdocs_theme_setup() {
  add_image_size( 'cpt-thumb', 300 );
  add_image_size( 'cpt-cover', 1400 );
  add_image_size( 'product_thumb', 200, 200, true );
}


// Ajax Comments

add_action('comment_post', 'ajaxify_comments',20, 2);
function ajaxify_comments($comment_ID, $comment_status){
  if(!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest'){
    //If AJAX Request Then
    switch($comment_status){
    case '0':
    //notify moderator of unapproved comment
    wp_notify_moderator($comment_ID);
    case '1': //Approved comment
    echo "success";
    $commentdata=&get_comment($comment_ID, ARRAY_A);
    $post=&get_post($commentdata['comment_post_ID']);
    wp_notify_postauthor($comment_ID, $commentdata['comment_type']);
    break;
    default:
    echo "error";
    }
    exit;
  }
}

// Comment Box Functions
function wpb_move_comment_field_to_bottom( $fields ) {
  $comment_field = $fields['comment'];
  unset( $fields['comment'] );
  $fields['comment'] = $comment_field;
  return $fields;
}
add_filter( 'comment_form_fields', 'wpb_move_comment_field_to_bottom' );

remove_filter('pre_user_description', 'wp_filter_kses');

function custom_comments( $comment, $args, $depth ) {
  $GLOBALS['comment'] = $comment;
  switch( $comment->comment_type ) :
      case 'pingback' :
      case 'trackback' : ?>
          <li <?php comment_class(); ?> id="comment<?php comment_ID(); ?>">
          <div class="back-link"><?php comment_author_link(); ?></div>
      <?php break;
      default :

          ?>
          <li <?php comment_class(); ?> id="comment-<?php comment_ID(); ?>">

          <div id="loader"></div>

          <article <?php comment_class(); ?> class="comment">

          <div class="author-pp">
            <div class="author-photo">
            <?php echo get_avatar( $comment, 96 ); ?>
            </div>
            <span class="author-tag">Author</span>
          </div>

          <div class="comment-body">
            <div class="comment-head">
              <span class="author-name"><?php comment_author(); ?></span>
              <?php $comment_date = human_time_diff(get_comment_time('U'), current_time('timestamp'));  ?>
              <span class="comment-date"><?php echo $comment_date; ?></span>
            </div>
            <div class="comment-content"><?php comment_text(); ?></div>
            <div class="reply">
            <?php
              comment_reply_link( array_merge( $args, array(
                'reply_text' => 'Reply',
                'depth' => $depth,
                'max_depth' => $args['max_depth'] ,
                'before'  => '',
                'after'  => '',
              ) ) ); ?>
            </div><!-- .reply -->
          </div><!-- .comment-body -->

          </article><!-- #comment-<?php comment_ID(); ?> -->
      <?php // End the default styling of comment
      break;
  endswitch;
}

function theme_increase_mem_limit($wp_max_mem_limit) {
	return "512M";
}
add_filter('admin_memory_limit', 'theme_increase_mem_limit',10,3);

function get_id_by_slug($page_slug) {
  $page = get_page_by_path($page_slug);
  if ($page) {
    return $page->ID;
  } else {
    return null;
  }
}
