<?php

/* ===============================================================
    Options Menu
=============================================================== */

if ( function_exists( 'acf_add_options_page' ) ) {

  // Main Theme Settings Page
  $parent = acf_add_options_page( array(
    'page_title'  => 'Website Content Options',
    'menu_title'  => 'Site Options',
    'menu_slug'   => 'acf-options-theme-settings',
    'redirect'    => 'Theme Settings',
		'icon_url'    => 'dashicons-admin-settings',
		'position'    => 2
  ) );

  // Station Listing Page
  $parent = acf_add_options_page( array(
    'page_title' => 'Stations Listing',
    'menu_title' => 'Stations',
    'redirect'   => 'Stations Listing',
		'icon_url' => '',
		'position' => 3
  ) );

  // Monthly Offer
  $parent = acf_add_options_page( array(
    'page_title' => 'Monthly Offer',
    'menu_title' => 'Monthly Offer',
    'redirect'   => 'Monthly Offer',
		'icon_url' => '',
		'position' => 3
  ) );

}

add_filter('acf/load_value/name=stations', 'sort_station_column_acf_load_value', 10, 3); //repeater name is timeline
function sort_station_column_acf_load_value( $rows){
  foreach( $rows as $key => $row ) {
    $column_id[ $key ] = $row['field_5b336fd0a480e'];
  }
  array_multisort( $column_id, SORT_ASC, $rows );
  return $rows;
}

// Hide ACF menu from domains
function awesome_acf_hide_acf_admin() {
  // get the current site url
  $site_url = get_bloginfo( 'url' );
  // an array of protected site urls
  $protected_urls = array(
    'https://unlockingthebible.org',
    'https://utbstaging.wpengine.com'
  );
  // check if the current site url is in the protected urls array
  if ( in_array( $site_url, $protected_urls ) ) {
    // hide the acf menu item
    return false;
  } else {
    // show the acf menu item
    return true;
  }
}

add_filter('acf/settings/show_admin', 'awesome_acf_hide_acf_admin');
