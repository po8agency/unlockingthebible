// var delayTimer;
// function doSearch(text) {
//   var $form    = jQuery('#search-container form');
//   var $content = jQuery('#content'),
//       $input   = $form.find('input[name="s"]');
//   var query    = $input.val();
//   clearTimeout(delayTimer);
//   delayTimer = setTimeout(function() {
//     $count   = jQuery('#search-count'),
//     $term    = jQuery('#search-term');
//     jQuery.ajax({
//       type : 'post',
//       url : ajaxquery.ajaxurl,
//       data : {
//         action : 'load_search_results',
//         query : query
//       },
//       beforeSend: function() {
//         $input.prop('disabled', true);
//         $content.addClass('loading');
//       },
//       success : function( response ) {
//         $input.prop('disabled', false);
//         $content.removeClass('loading');
//         $content.html( response );
//         $term.html( query );
//         $count.html( $("#content > article").length );
//       }
//     });
//
//     return false;
//   }, 1000); // Will do the ajax stuff after 1000 ms, or 1 s
// }
//
// $(document).ready(function() {
//   $(".search-form #search-input").keydown(function(event){
//     if(event.keyCode == 13) {
//       doSearch($(this).value);
//       event.preventDefault();
//     }
//   });
// });

// (function($) {
// 	function find_page_number( element ) {
// 		element.find('span').remove();
// 		return parseInt( element.html() );
// 	}
//   $(document).on( 'click', '.pagination a', function( event ) {
//     event.preventDefault();
//
//   	page = find_page_number( $(this).clone() );
//     $.ajax({
//     	url: ajaxquery.ajaxurl,
//     	type: 'post',
//     	data: {
//     		action: 'ajax_pagination',
//     		query_vars: ajaxquery.query_vars,
//     		page: page
//     	},
//     	beforeSend: function() {
//     		$('#content').find( 'article' ).remove();
//     		$('#content nav').remove();
//     		$(document).scrollTop();
//     		$('#content').addClass('loading');
//     	},
//     	success: function( html ) {
//         $('#content').append( html );
//     		$('#content').removeClass('loading');
//     	}
//     })
//   });
//
// })(jQuery);

// Ajax add to cart on the product page
var $warp_fragment_refresh = {
  url: wc_cart_fragments_params.wc_ajax_url.toString().replace( '%%endpoint%%', 'get_refreshed_fragments' ),
  type: 'POST',
  success: function( data ) {
      if ( data && data.fragments ) {

          $.each( data.fragments, function( key, value ) {
              $( key ).replaceWith( value );
          });

          $( document.body ).trigger( 'wc_fragments_refreshed' );
      }
  }
};

$('.entry-summary form.cart').on('submit', function (e) {
    e.preventDefault();

    $('.entry-summary').block({
        message: null,
        overlayCSS: {
            cursor: 'none'
        }
    });

    var product_url = window.location,
        form = $(this);

    $.post(product_url, form.serialize() + '&_wp_http_referer=' + product_url, function (result){
        var cart_dropdown = $('.mini-cart-dropdown', result)

        // update dropdown cart
        $('.mini-cart-dropdown').replaceWith(cart_dropdown);

        // update fragments
        $.ajax($warp_fragment_refresh);

        $('.entry-summary').unblock();

    });
});
