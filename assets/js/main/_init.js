window.lazySizesConfig = window.lazySizesConfig || {};

var lazy = function lazy() {
	document.addEventListener('lazyloaded', function (e)  {
		e.target.parentNode.classList.add('image-loaded');
		e.target.parentNode.classList.remove('loading');
	});
}

lazy();

jQuery(document).ready(function($) {

	if ($('.utb--topics').length) {
		size_li = $(".utb--topic-terms li").size();
    x=12;
    $('.utb--topic-terms li:lt('+x+')').show();
    $('.topics-more a').click(function (e) {
      x= (x+50 <= size_li) ? x+50 : size_li;
      $('.utb--topic-terms li:lt('+x+')').show();
      if(x == size_li){
        $('.topics-more').hide();
      }
			e.preventDefault();
    });
	}

	if ($('.utb--slides').length) {
		$(".utb--slides > div:gt(0)").hide();

		setInterval(function() {
		  $('.utb--slides > div:first')
		    .fadeOut(1000)
		    .next()
		    .fadeIn(1000)
		    .end()
		    .appendTo('.utb--slides');
		}, 5000);
	}

	$('[data-tab]').on('click', function (e) {
	  $('[data-tab]').removeClass('active');
		$(this).addClass('active');
	  $(this).closest('.media-type').find('[data-content=' + $(this).data('tab') + ']').addClass('active').siblings('[data-content]').removeClass('active');
	  e.preventDefault();
	})

	//--------------------
	//  CUSTOM NUMBER INPUT
	//--------------------

	if ($('.utb--product-single').length || $('.utb--cart').length ) {
		jQuery('<div class="quantity-nav"><div class="quantity-button quantity-up">+</div><div class="quantity-button quantity-down">-</div></div>').insertAfter('.quantity input');
		jQuery('.quantity input').attr("max", "100");
    jQuery('.quantity').each(function() {
      var spinner = jQuery(this),
        input 	= spinner.find('input[type="number"]'),
        btnUp 	= spinner.find('.quantity-up'),
        btnDown = spinner.find('.quantity-down'),
        min 		= input.attr('min'),
        max 		= input.attr('max');

      btnUp.click(function() {
        var oldValue = parseFloat(input.val());
        if (oldValue >= max) {
          var newVal = oldValue;
        } else {
          var newVal = oldValue + 1;
        }
        spinner.find("input").val(newVal);
        spinner.find("input").trigger("change");
      });

      btnDown.click(function() {
        var oldValue = parseFloat(input.val());
        if (oldValue <= min) {
          var newVal = oldValue;
        } else {
          var newVal = oldValue - 1;
        }
        spinner.find("input").val(newVal);
        spinner.find("input").trigger("change");
      });

    });
	}

	//--------------------
	//  SMOOTH ANCHOR SCROLLING
	//--------------------
	// Select all links with hashes
	$('a[href*="#"]')
	// Remove links that don't actually link to anything
	.not('[href="#"]')
	.not('[href="#0"]')
	.not('.tabs li a')
	.not('.print-doc')
	.click(function(event) {
		// On-page links
		if (
			location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '')
			&&
			location.hostname == this.hostname
		) {
			// Figure out element to scroll to
			var target = $(this.hash);
			target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
			// Does a scroll target exist?
			if (target.length) {
				// Only prevent default if animation is actually gonna happen
				event.preventDefault();
				$('html, body').animate({
					scrollTop: target.offset().top - 50
				}, 1000, function() {
					// Callback after animation
					// Must change focus!
					var $target = $(target);
					$target.focus();
					if ($target.is(":focus")) { // Checking if the target was focused
						return false;
					} else {
						$target.attr('tabindex','-1'); // Adding tabindex for elements not focusable
						$target.focus(); // Set focus again
					};
				});
			}
		}
	});

	$('.print-doc').on('click', function() {
	  window.print();
	  return false; // why false?
	});

	//--------------------
	//  FOCUS SEARCH INPUT ON PAGE
	//--------------------
	if ($('.utb--search-page').length) {
		setTimeout(function() {
			$('#search-input').focus();
		}, 500);
	}

	//--------------------
	//  VARIABLES
	//--------------------
	var link = $(".menu-toggle"),
			body = $("body"),
			overlayMenu = $(".mobile-overlay"),
			searchBox 	= $("#top-search"),
			searchInput = $("#top-search input"),
			searchLink 	= $(".search-toggle");

	//--------------------
	//  MENU TOGGLE
	//--------------------
	link.on('click touch', function(e) {
		e.stopPropagation();
		$(this).toggleClass("is-active");
		body.toggleClass("is-active");
		overlayMenu.fadeToggle();
		searchBox.slideUp('fast');
		searchLink.removeClass('is-active');
	});

	//--------------------
	//  SEARCH TOGGLE
	//--------------------
	searchLink.on('click touch', function(e) {
		e.stopPropagation();
		searchBox.slideToggle('fast');
		searchLink.toggleClass('is-active');
		setTimeout(function() {
	    searchInput.focus();
	  }, 600);
	});

	//--------------------
	//  CUSTOM SELECT DROPDOWNS
	//--------------------

	if ($('.select-dress').length) {
		$(document).ready(function() {
			$('.select-dress').niceSelect();
		});
	}

	//--------------------
	//  SINGLE POST DOWNLOAD TOGGLE
	//--------------------

	var dlLink = $(".has-dropdown > a");

	dlLink.on('click touch', function(event) {
		event.preventDefault();
		$(this).addClass("is-active");
		$(this).closest('.has-dropdown').children('.dropdown').fadeIn(200);
	});

	if($(".has-dropdowns").length > 0) {
		$("body").mouseup(function(e) {
			var subject = $(".dropdown");
			if(e.target.class != subject.attr('class') && !subject.has(e.target).length) {
				dlLink.removeClass("is-active");
			  dlLink.closest('.has-dropdown').children('.dropdown').fadeOut(100);
			}
		});
	}

	//--------------------
	//  PRODUCT SORTING
	//--------------------

	var sortBtn = $(".product-sort > span");

	sortBtn.on('click touch', function(event) {
		event.preventDefault();
		$(this).parent().toggleClass("open");
		$(this).parent().find('.list').fadeToggle(200);
	});

	if($(".product-sort").length > 0) {
		$("body").mouseup(function(e) {
			var subject = $(".product-sort");
			if(e.target.class != subject.attr('class') && !subject.has(e.target).length) {
				$(".product-sort").removeClass("open");
			  $(".product-sort .list").fadeOut(100);
			}
		});
	}

	$(".product-sort .control").on('click touch', function(event) {
		event.preventDefault();
		var selector = $(this).text();

		sortBtn.text(selector);
	});

});

//--------------------
//  STATIONS TEMPLATE FILTER/SEARCH
//--------------------

if($('#stations-us').length) {
	var containerElArchive = document.querySelector('#stations-us');
			//loadMoreEl = document.querySelector('.load-more'),
		  //currentLimit = 20,
		  //incrementAmount = 5,
		  //canLoadMore = true,
		  //scrollThreshold = 0;

	// function throttle(fn, interval) {
  //   var timeoutId = -1;
  //   var last = -1;
	//
  //   return function() {
  //     var self = this;
  //     var args = arguments;
  //     var now = Date.now();
  //     var difference = last ? now - last : Infinity;
	//
  //     var later = function() {
  //       last = now;
  //       fn.apply(self, args);
  //     };
	//
  //     if (!last || difference >= interval) {
  //       later();
  //     } else {
  //       clearTimeout(timeoutId);
  //       timeoutId = setTimeout(later, interval - difference);
  //     }
  //   };
  // }
	//
  // function handleScroll() {
  //   if (mixerStations.isMixing() || !canLoadMore) return;
	//
  //   var scrollTop = window.scrollY || window.pageYOffset || document.documentElement.scrollTop;
  //   var offset = scrollTop + window.innerHeight;
  //   var containerHeight = containerElArchive.offsetHeight;
	//
  //   if (offset >= containerHeight - scrollThreshold) {
  //     incrementPageLimit();
  //   }
  // }
	//
  // function incrementPageLimit() {
  //     currentLimit += incrementAmount;
	//
  //     mixerStations.paginate({limit: currentLimit});
  // }
	//
  // function handleMixEnd(state) {
  //   // At the end of each operation, we must check whether the current
  //   // matching collection of target elements has additional hidden
  //   // elements, and set the `canLoadMore` flag accordingly.
  //   if (state.activePagination.limit + incrementAmount >= state.totalMatching) {
  //     canLoadMore = false;
  //   } else {
  //     canLoadMore = true;
  //   }
  //   setTimeout(handleScroll, 10);
  // }

	var mixerStations = mixitup(containerElArchive, {
	  multifilter: {
	    enable: true
	  },
		//pagination: {
			//limit: currentLimit,
			//hidePageListIfSinglePage: true,
			//loop: false
		//},
    callbacks: {
      //onMixEnd: handleMixEnd
			onMixStart: function(state, futureState) {
        //console.log('Starting mix');
				$('.station-loader').addClass('loading');
      },
			onMixBusy: function(state) {
        //console.log('Loading stations...');
      },
			onMixEnd: function(state) {
				$('.station-loader').removeClass('loading');
				$('.station-fail').removeClass('has-failed');
				state.sort = 'city:asc';
	      return state;
      },
			onMixFail: function(state) {
				$('.station-fail').addClass('has-failed');
      }
			// onParseFilterGroups: function(command) {
      //   command.sort = 'city:asc';
      //   return command;
      // }
    },
		animation: {
	    duration: 250,
	    nudge: false,
	    reverseOut: true,
	    effects: "fade scale(0.35) translateZ(0px) stagger(30ms)"
    }
	});

	$('#listing-switcher').on('change', function() {
		if (this.value == 'international') {
			$('#stations-int, #stations-head-int').addClass('active-list');
			$('#stations-us, #stations-head-us').removeClass('active-list');
			$('.controls').hide();
		} else if (this.value == 'united-states') {
			$('#stations-us, #stations-head-us').addClass('active-list');
			$('#stations-int, #stations-head-int').removeClass('active-list');
			$('.controls').show();
		}
	})


	// Attach a throttled scroll handler to the scroll event. Will fire
  // up to a maximum of once every 50ms.
  //window.addEventListener('scroll', throttle(handleScroll, 50));

}

//--------------------
//  ARCHIVE TEMPLATE FILTER/SEARCH
//--------------------

if($('.utb--results').length) {

	var containerEl = document.querySelector('.utb--results');
  var targetSelector = '.mix';
  var activeHash = '';

  function deserializeHash() {
    var hash    = window.location.hash.replace(/^#/g, '');
    var obj     = null;
    var groups  = [];

    if (!hash) return obj;

    obj = {};
    groups = hash.split('&');

    groups.forEach(function(group) {
      var pair = group.split('=');
      var groupName = pair[0];

      obj[groupName] = pair[1].split(',');
    });

    return obj;
  }

  function serializeUiState(uiState) {
    var output = '';

    for (var key in uiState) {
      var values = uiState[key];

      if (!values.length) continue;

      output += key + '=';
      output += values.join(',');
      output += '&';
    };

    output = output.replace(/&$/g, '');

    return output;
  }

  function getUiState() {
    // NB: You will need to rename the object keys to match the names of
    // your project's filter groups – these should match those defined
    // in your HTML.

    var uiState = {
      type: mixer.getFilterGroupSelectors('type').map(getValueFromSelector)
    };

    return uiState;
  }

  function setHash(state) {
    var selector = state.activeFilter.selector;
    // Construct an object representing the current state of each
    // filter group
    var uiState = getUiState();
    // Create a URL hash string by serializing the uiState object
    var newHash = '#' + serializeUiState(uiState);
    if (selector === targetSelector && window.location.href.indexOf('#') > -1) {
      // Equivalent to filter "all", and a hash exists, remove the hash
      activeHash = '';
      history.replaceState(null, document.title, window.location.pathname);
    } else if (newHash !== window.location.hash && selector !== targetSelector) {
      // Change the hash
      activeHash = newHash;
      history.replaceState(null, document.title, window.location.pathname + newHash);
    }
  }

  function syncMixerWithPreviousUiState(uiState, animate) {
    var type = (uiState && uiState.type) ? uiState.type : [];

    mixer.setFilterGroupSelectors('type', type.map(getSelectorFromValue));

    // Parse the filter groups (passing `false` will perform no animation)

    return mixer.parseFilterGroups(animate ? true : false);
  }

  function getValueFromSelector(selector) {
    return selector.replace(/^./, '');
  }

  function getSelectorFromValue(value) {
    return '.' + value;
  }

  var uiState = deserializeHash();

  // Instantiate MixItUp

  var mixer = mixitup(containerEl, {
		pagination: {
			limit: 9,
			hidePageListIfSinglePage: true,
			loop: true
		},
	  multifilter: {
	    enable: true
	  },
		animation: {
	    duration: 150,
	    nudge: false,
	    reverseOut: true,
	    effects: "fade scale(0.35) translateZ(0px) stagger(30ms)"
    },
    selectors: {
      target: targetSelector
    },
    callbacks: {
      onMixEnd: setHash // Call the setHash() method at the end of each operation
    }
  });

  if (uiState) {
    // If a valid uiState object is present on page load, filter the mixer
    syncMixerWithPreviousUiState(uiState);
  }

}

if ($('.utb--products').length) {
	var containerElproducts = document.querySelector('.utb--products');
	var mixerPorducts = mixitup(containerElproducts, {
		pagination: {
			limit: 8,
			hidePageListIfSinglePage: true,
			loop: false
		},
		animation: {
			enable: false,
	    nudge: false,
			applyPerspective: false
    }
	});
}

//--------------------
//  CUSTOM HTML5 AUDIO PLAYER
//--------------------

function calculateTotalValue(length) {
  var minutes = Math.floor(length / 60),
    seconds_int = length - minutes * 60,
    seconds_str = seconds_int.toString(),
    seconds = seconds_str.substr(0, 2),
    time = minutes + ':' + seconds

  return time;
}

function calculateCurrentValue(currentTime) {
  var current_hour = parseInt(currentTime / 3600) % 24,
    current_minute = parseInt(currentTime / 60) % 60,
    current_seconds_long = currentTime % 60,
    current_seconds = current_seconds_long.toFixed(),
    current_time = (current_minute < 10 ? "0" + current_minute : current_minute) + ":" + (current_seconds < 10 ? "0" + current_seconds : current_seconds);

  return current_time;
}

function setVolume() {
	var player 	= document.getElementById('player');
	var volume 	= document.getElementById("volume");
	player.volume = volume.value;
}

function setDuration() {
	$("#player").on("canplaythrough", function(e){
    var seconds = e.currentTarget.duration;
    var duration = moment.duration(seconds, "seconds");

    var time = "";
    var hours = duration.hours();
    if (hours > 0) { time = hours + ":" ; }

    time = time + duration.minutes() + ":" + duration.seconds();
    $(".media-type-audio .end-time").text(time);
	});
}

$("input#volume").mousemove(function (e) {
  var val = ($(this).val() - $(this).attr('min')) / ($(this).attr('max') - $(this).attr('min'));
  var percent = val * 100;

  $(this).css('background-image',
      '-webkit-gradient(linear, left top, right top, ' +
      'color-stop(' + percent + '%, #e5b223), ' +
      'color-stop(' + percent + '%, #fbf3dc)' +
      ')');

  $(this).css('background-image',
      '-moz-linear-gradient(left center, #e5b223 0%, #e5b223 ' + percent + '%, #fbf3dc ' + percent + '%, #fbf3dc 100%)');
});

function initProgressBar() {
  var player 	= document.getElementById('player');
  var length 	= player.duration;
  var current_time = player.currentTime;

  // calculate total length of value
  var totalLength = calculateTotalValue(length)
  jQuery(".end-time").html(totalLength);

  // calculate current value time
  var currentTime = calculateCurrentValue(current_time);
  jQuery(".start-time").html(currentTime);

  var progressbar = document.getElementById('seekObj');
  progressbar.value = (player.currentTime / player.duration);
  progressbar.addEventListener("click", seek);

  if (player.currentTime == player.duration) {
    $('#play-btn').removeClass('pause');
		$('#play-btn i').removeClass('icon-control-pause').addClass('icon-control-play');
  }

  function seek(evt) {
    var percent = evt.offsetX / this.offsetWidth;
    player.currentTime = percent * player.duration;
    progressbar.value = percent / 100;
  }
	player.controls = false;
};

function initPlayers(num) {
  // pass num in if there are multiple audio players e.g 'player' + i
  for (var i = 0; i < num; i++) {
    (function() {
			setDuration();
			setVolume();
      // Variables
      // ----------------------------------------------------------
      // audio embed object
      var playerContainer = document.getElementById('player-container'),
        player = document.getElementById('player'),
        isPlaying = false,
        playBtn = document.getElementById('play-btn');

      // Controls Listeners
      // ----------------------------------------------------------
      if (playBtn != null) {
        playBtn.addEventListener('click', function() {
          togglePlay()
        });
      }

      // Controls & Sounds Methods
      // ----------------------------------------------------------
      function togglePlay() {
        if (player.paused === false) {
          player.pause();
          isPlaying = false;
          $('#play-btn').removeClass('pause');
					$('#play-btn i').removeClass('icon-control-pause').addClass('icon-control-play');

        } else {
          player.play();
          $('#play-btn').addClass('pause');
					$('#play-btn i').addClass('icon-control-pause').removeClass('icon-control-play');
          isPlaying = true;
        }
      }
    }());
  }
}
initPlayers(jQuery('.audio-wrapper').length);

$('a').each(function() {
   var a = new RegExp('/' + window.location.host + '/');
   if(!a.test(this.href)) {
       $(this).click(function(event) {
           event.preventDefault();
           event.stopPropagation();
           window.open(this.href, '_blank');
       });
   }
});

$(function() {
	//localStorage.removeItem('btnCount');
	var btnCount = localStorage.getItem('btnCount');
	btnCount = btnCount ? parseInt(btnCount) : 0;
	var $showCount				= $('#counter');
	var $subModal 				= $('#subscribe-modal');
	var $subModalOverlay 	= $('.utb--modal-overlay');
	$showCount.text(btnCount);
  $('#download-mp3').click( function() {
		$showCount.text(++btnCount);
		localStorage.setItem('btnCount', btnCount);
		if(btnCount == 3){
	  	$subModal.addClass('is-active');
			$subModalOverlay.addClass('is-active');
	  }
  });

	$('#close-modal').on('click touch', function(event) {
		$subModal.removeClass('is-active');
		$subModalOverlay.removeClass('is-active');
	});

});
